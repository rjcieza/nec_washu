# About this repository

This repository will be used to document genomic analysis of E. coli strains from WashU
# Glossary

 [I. Environment](#i-environment)
 
  - [a. Miniconda](#a-miniconda)
  - [b. Python2](#b-python2)
  - [c. Python3](#c-python3)
 
 [II. Programs](#ii-programs)

 [A. Software Quality Control](#a-software-quality-control)

 - [A1. FastQC MultiQC Cutadapt](#a1-fastqc-multiqc-cutadapt)
 
 [B. Software Taxonomic Analysis](#b-software-taxonomic-analysis)
 
 - [B1. Kraken2](#b1-kraken2)
 - [B2. ARIBA](#b2-ariba)
 
 [C. Software Read Mapping Core SNPs](#c-software-read-mapping-core-snps)
 
 - [C1. ART](#c1-art)
 - [C2. SPANDx](#c2-spandx)
 - [C3. RAxML-NG](#c3-raxml-ng)
 - [C4. SNP-dists](#c4-snp-dists)
 
 [D. Software Genome Assembly and Pangenome](#d-software-genome-assembly-and-pangenome)

 - [D1. SPAdes](#d1-spades)
 - [D2. Fastagrep](#d2-fastagrep)
 - [D3. Fastq-info](#d3-fastq-info)
 - [D4. QUAST](#d4-quast)
 - [D5. ABACAS](#d5-abacas)
 - [D6. Prokka](#d6-prokka) 
 - [D7. Roary](#d7-roary)

 [E. Analysis](#e-analysis)

 - [E1. SNP distances](#e1-snp-distances)


 [III. Usage](#iii-usage)
- [1. Quality control](#1-quality-control)
- [2. Taxonomy](#2-taxonomy)
- [3. Read mapping to a reference - SNPs](#3-read-mapping-to-a-reference-snps)
- [4. Assembly](#4-assembly)

 [IV. Results](#iv-results)
 
 - [A. taxonomy](#a-taxonomy)
 
 - [1. Quality control](#1-quality-control)
 - [2. Taxonomy](#2-taxonomy)
 - [3. MLST](#3-mlst)

 [V. Results ASSEMBLY](#iv-results-assembly)
 - [1. Assembly and coverage](#1-assembly-and-coverage)

 [VI. Appendix](#v-appendix)
 - [1. PBS Script Writer Setup](#1-pbs-script-writer-setup)


# I. Environment

To install any of the software listed in **Section II**, admin permission
will be needed. This will not be possible in the **UMICH Great Lakes server**.
The best option is creating your own system environment to install any software without being an administrator.
- Software will be installed through **Conda** or directly into **DIR bin**
- **Conda** is an open source package management system and environment management system.

#   ```a. Anaconda```
**Anaconda** latest release will be installed (It contains Conda and Python).
Some software will require **Python version 2** or **Python version 3**. 
To adress this conflict, create separete Python 2 and Python 3 environments,
as indicated in the next section (b).
- Website: [Anaconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)
- Download Anaconda into *HOME* DIR
- During installation when asked:
    - Do you wish the installer to initialize Anaconda3 by running conda init? Answer **YES**

```shell
##################      INSTALLATION INSTRUCTIONS   ############################
# (1) Change DIR to HOME (~)
cd ~
# (2) Download "Miniconda"
wget https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh
# (3) Install
bash Anaconda3-2020.11-Linux-x86_64.sh
# (4) Remove executable after installation is completed
rm Anaconda3-2020.11-Linux-x86_64.sh
# (5) Source ".bashrc" file after installation
source .bashrc
# (6) Confirm version
python -V             
# Output should be: Python 3.8.5
# (7) Confirm PATH
which python          
# Output should be: ~/anaconda3/bin/python
```

#   ```b. Python2```
Create Python 2 environment: 
[Conda environments](https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/environments.html?highlight=environments)
- After **mypython2** environment has been created, install software 
in this environment.

```shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) Create Python 2 environment
conda create -n mypython2 python=2.7

# (2) Activate Python 2 environment
conda activate mypython2

# (3) Confirm version
python -V                   
# Output should be: Python 2.7

# (4) Confirm PATH
which python             
# Output should be: ~/miniconda3/envs/mypython2/bin/python

# (5) Close Python 2 environment
conda activate
```

#   ```c. Python3```
Create Python 3 environment: 
[Conda environments](https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/environments.html?highlight=environments)
- After **mypython3** environment has been created, install software 
in this environment.

```shell
##################      INSTALLATION INSTRUCTIONS   ############################
# (1) Create Python 3 environment
conda create -n QC python=3
conda create -n Aligner python=3
conda create -n Assembly python=3
# (2) Activate Python 3 environment
conda activate QC
conda activate Assembly
# (3) Confirm version
python -V               
# Output should be: any verion of python 3
# (4) Confirm PATH
which python            
# Output should be: ~/Anaconda3/envs/QC/bin/python
# (5) Close Python 3 environment
conda activate
```

# II. Programs

Programs required to install for analysis.

# A. Software Quality Control


#   ```A1. FastQC MultiQC Cutadapt```

A quality control tool for high throughput sequence data.
- Website: [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
- Version: **v 0.11.9**

Modular tool to aggregate results from bioinformatics analyses across 
many samples into a single report.
- Website: [MultiQC](https://github.com/ewels/MultiQC)
- Version: **v 1.9**

Cutadapt will remove adapter sequences, 
N bases at the end of the reads as well as low quality bases.
- Website: [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- Version: **v 2.10**

***
- Required to run **'01_QualityControl_FastQCpre.sh'** 
- Required to run **'02_QualityControl_MultiQCpre.sh'**
- Required to run **'03_QualityControl_Trim.sh'**
- Required to run **'04_QualityControl_FastQCpost.sh'**
- Required to run **'05_QualityControl_MultiQCpost.sh'**
- Required to run **'17_Assembly_MultiQC'**
- Required to run **'20_Assembly_MultiQCannotation'**

```shell
##################      INSTALLATION INSTRUCTIONS   ############################
# (1) Activate Python 3 environment (name = QC)
conda activate QC
# (2) Install FastQC, MultiQC through Conda 
conda install -c bioconda fastqc
conda install -c bioconda multiqc
conda install -c bioconda cutadapt
# (3) Confirm installation
fastqc -v                 # Output should be: FastQC v0.11.9
multiqc --version         # Output should be: multiqc, version 1.9 
cutadapt --version        # Output should be: 2.10
# (4) Close environment
conda activate
```










# B. Software Taxonomic Analysis


#   ```B1. Kraken2``` 

Taxonomic classification system using exact k-mer matches.

- Website: [Kraken2](https://github.com/DerrickWood/kraken2/blob/master/docs/MANUAL.markdown)
- Version: **2.0.7-beta**
- Install in Python 3 environment
- Kraken2 and dependencies are installed through Conda 
- Kraken2 databases are downloaded into DIR [ bin ]
***
- Required to run **06_TaxonomyClassification_Kraken.sh**

``` shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) Activate Python 3 environment
conda activate mypython3

# (2) Install Kraken2
conda install -c bioconda kraken2

# (3) Confirm installation
kraken2 --version             
# Output should be: Kraken version 2.0.7-beta 

# (4) Close Python 3 environment
conda activate

# (5) cd into DIR [ bin ] and download Kraken database
# Kraken database used is:      MiniKraken2_v2_8GB: 
cd bin/
wget ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/minikraken2_v2_8GB_201904_UPDATE.tgz
tar -xvzf minikraken2_v2_8GB_201904_UPDATE.tgz
rm minikraken2_v2_8GB_201904_UPDATE.tgz

# (6) Rename DIR [ minikraken2_v2_8GB_201904_UPDATE ] to [ Kraken2_DB ]
mv minikraken2_v2_8GB_201904_UPDATE Kraken2_DB
```


#   ```B2. ARIBA``` 

Antimicrobial Resistance Identification By Assembly & Sequence typing (MLST).

- Website: [ARIBA](https://github.com/sanger-pathogens/ariba)
- Version: **v 2.12.0**
- Install in Python 3 environment
- ARIBA and dependencies are installed through Conda
***
- Required to run **'07_TaxonomyClassification_MLST.sh'**

``` shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) Activate Python 3 environment
conda activate mypython3

# (2) Install ARIBA
conda install -c bioconda ariba

# (3) Confirm installation
ariba version | head -n 2             
# Output should be: ARIBA version: 2.12.0

# (4) Close Python 3 environment
conda activate
```


# C. Software Read Mapping Core SNPs


#   ```C1. ART``` 

To establish the phylogenetic diversity of our BACTERIAL ISOLATES,
it is important using set of REFERENCE genomes with know phylogeny.
We will download REFERENCE genomes representative of the genetic diversity found 
in *E. coli*.

Reference genomes deposited into NCBI are not in the format required for SPANDx 
pipeline **(FASTQ reads)**. ART outputs artificial FASTQ files (Forward and Reverse) 
from a REFERENCE genome.

It requires to specify a list of REFERENCE genomes to download specified in:
**Reference_List_ART.txt** ( Explained in *Usage* section ).

- Website: [Artificial FastQ Generator](https://www.niehs.nih.gov/research/resources/software/biostatistics/art/index.cfm)
- Version: **v 2.5.8**
- ART is installed into DIR [ bin ]
***
- Required to run **08_Mapping_ReferenceGenomes.sh**

``` shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) cd into DIR [ bin ] and download latest installation
cd bin/
wget https://www.niehs.nih.gov/research/resources/assets/docs/artbinmountrainier2016.06.05linux64.tgz
tar -xvzf artbinmountrainier2016.06.05linux64.tgz
rm artbinmountrainier2016.06.05linux64.tgz

# (2) Rename DIR [ art_bin_MountRainier ] to [ ART ]
mv art_bin_MountRainier/ ART/

# (3) Confirm installation
cd ART/
./art_illumina --help | head -n 7     
# Output should be: Q Version 2.5.8 (June 7, 2016)
```


# ```C2. SPANDx```

SPANDx is a pipeline that identifies genetic variants by read-mapping our samples
(FASTQ reads) to a **REFERENCE GENOME**. SPANDx (Synergised Pipeline for Analysis 
of Next-generation sequencing Data in Linux) identifies single-nucleotide 
polymorphisms [SNPs], insertion/deletions [indels], and large/deletions [> 100 bp].

For this project, the reads were mapped against *Escherichia coli HS*.

SPANDx generates a multi-sequence alignment file that will be used to generate 
a phylogenetic tree with **RAxML**.

Read mapping requires a REFERENCE GENOME. REFERENCE genome is specified in: 
**Reference_List_SPANDx.txt** ( Explained in *Usage* section ).

- Website: [SPANDx](https://github.com/dsarov/SPANDx)
- Version: **v 3.2**
- SPANDx is installed into DIR [ bin ]
***
- Required to run **'09_Mapping_SPANDx.sh'** 
- Required to run **'10_Mapping_SPANDx.sh'**

``` shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) cd into DIR [ bin ] and download the latest installation
cd bin/
git clone https://github.com/dsarov/SPANDx.git

# (2) Change into DIR [ SPANDx ] 
cd SPANDx/

# (3) Remove default CONFIG files and SPANDx.sh
# Replace with the CONFIG files in this repository. DIR [ bin/SPANDx ]
rm SPANDx.config
rm scheduler.config
rm SPANDx.sh
# Modified SPANDx.config file have the PATH for ($SPANDx_LOCATION) modified - line 10
# Modified scheduler.config file have updated resource manager instructions
# Modified SPANDx.sh queues jobs hi a hierarchical order (dependencies)

# (4) Alter file permissions
chmod 755 *

# (5) Confirm installation of GATK
# SPANDx runs with GATK version 3 or less (version 4 is not compatible)
java -jar GenomeAnalysisTK.jar -h | head -n 5
# Output should be: The Genome Analysis Toolkit (GATK) v3.2-2-gec30cee
```


#   ```C3. RAxML-NG```

RAxML-NG is a phylogenetic tree inference tool which uses maximum-likelihood (ML) 
optimality criterion. To determine the phylogenetic relationship among strains.
RAxML-NG uses the matrix containing all the orthologous SNPs generated with SPANDx 
(**Ortho_SNP_matrix_RAxML.nex**).

- Website: [RAxML-NG](https://github.com/amkozlov/raxml-ng)
- Version: **0.6.0 BETA**
- Older versions don't have the full set of options required to run script.
- Install in Python 3 environment
- RAxML-NG and dependencies are installed through Conda
***
- Required to run **'11_Mapping_RAxML.sh'**

``` shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) Activate Python 3 environment
conda activate mypython3

# (2) Install RAxML-NG
conda install -c bioconda raxml-ng

# (3) Confirm installation
raxml-ng --version | head -n 2       
# Output should be: RAxML-NG v. 0.9.0 released on 20.05.2019 by The Exelixis Lab. 

# (4) Close Python 3 environment
conda activate
```

#   ```C4. SNP-dists```

Convert Multi-sequence alignment (MSA) file (in FASTA format) 
into an SNP distance matrix. 

Concatenero will convert NEX alignment (MSA file) to FASTA format and 
afterwards SNP-dists will convert the MSA file to a SNP distance matrix.

- Website: [SNP-dists](https://github.com/tseemann/snp-dists)
- Version: **0.6.3**
- Install in Python 3 environment
- SNP-dists and dependencies are installed through Conda 
***
- Website: [Converter](https://github.com/ODiogoSilva/ElConcatenero)
- Concatenero is installed into DIR [ bin ]
***
- Required to run **'12_Mapping_SNPdistance.sh'**

``` shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) Activate Python 3 environment
conda activate mypython3

# (2) Install SNP-dists
conda install -c bioconda/label/cf201901 snp-dists

# (3) Confirm installation
snp-dists -v
# Output should be: snp-dists 0.6.3

# (4) Close Python 3 environment
conda activate

# (5) cd into DIR [ bin ] and download the latest version
cd bin/
git clone https://github.com/ODiogoSilva/ElConcatenero.git

# (6) Rename DIR [ ElConcatenero ] to [ Converter ]
mv ElConcatenero/ Converter/

# (7) Confirm installation
./Converter/ElConcatenero.py -h | head -n 1 
# Output should be: usage: ElConcatenero.py [-h] [-g GAP] [-m MISSING] [-if {fasta,nexus,phylip}] 
```

# D. Software Genome Assembly and Pangenome


#   ```D1. SPAdes```

Genome assembly with SPAdes.
During assembly it merges different k-mer assemblies (good for varying coverage)

- Website: [SPAdes](http://cab.spbu.ru/files/release3.12.0/manual.html)
- Version: **v 3.14.1**
- Install in environment: Assembly (python = 3)
- SPAdes and dependencies are installed through Conda
***
- Required to run **'13_Assembly_SPAdes.sh'**

```shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) Activate Python 3 environment (name = Assembly)
conda activate Assembly

# (2) Install SPAdes
conda install -c bioconda spades

# (3) Confirm installation
spades.py --version
# Output should be: SPAdes v3.14.1

# (4) Close environment
conda activate
```


#   ```D2. Fastagrep```

Remove small & Low coverage contigs from SPAdes assembly.
The script that uses Fastagrep to remove small & low coverage contigs from an 
SPAdes assembly is a modification of the script described here:
[Remove small & low coverage contigs](https://microsizedmind.wordpress.com/2015/03/05/removing-small-low-coverage-contigs-from-a-spades-assembly/)

- Website: [Fasta_Grep](https://github.com/rec3141/rec-genome-tools/blob/master/bin/fastagrep.pl)
- Fastagrep is installed into DIR [ bin ]
***
- Required to run **'14_Assembly_Filtering.sh'**

```shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) cd into DIR [ bin ] and create DIR [ Fastagrep ]
cd bin/
mkdir Fastagrep

# (2) Change into DIR [ Fastagrep ] and download latest installation
# A copy of the executable file is stored in this repository. DIR [ bin/Fastagrep ]
cd Fastagrep/
wget https://raw.githubusercontent.com/rec3141/rec-genome-tools/master/bin/fastagrep.pl

# (3) Alter file permissions
chmod 755 *

# (4) Confirm installation
./fastagrep.pl
```


#   ```D3. Fastq-info```

Calculate average read length, genome size in bp (fasta) and 
actual sequencing depth/coverage from FASTQ read.
Fastq-info can take in a genome assembly resulting from FASTQ files and generate
the actual sequencing coverage if read length is known.

- Website: [Fastq-info](https://github.com/raymondkiu/fastq-info)
- Fastq-info is installed into DIR [ bin ]
***
- Required to run **'15_Assembly_Coverage.sh'**

```shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) cd into DIR [ bin ] and download latest installation
cd bin/
git clone https://github.com/raymondkiu/fastq-info.git

# (2) Change into DIR [ fastq-info ] 
cd fastq-info/

# (3) Remove default EXECUTABLE files
rm -rf *
# The only EXECUTABLE needed is 'fastq_info_3.sh'
# Replace with the EXECUTABLE file in this repository. DIR [ bin/fastq-info ]
# Modified fastq_info_3.sh file can read FASTQ files that are zipped

# (4) Alter file permissions
chmod 755 *
```


#   ```D4. QUAST```

Evaluate genome assemblies with QUAST. 
Requires a REFERENCE genome to evaluate the assembly.
Evaluating assemblies requires a REFERENCE genome. 
REFERENCE genome is specified in: 
**Reference_List_QUAST.txt** ( Explained in *Usage* section ).

- Website: [QUAST](http://quast.bioinf.spbau.ru/manual.html#sec1)
- Version: **v 5.0.2**
- Install in environment: Assembly (python = 3)
- QUAST and dependencies are installed through Conda
***
- Required to run **'16_Assembly_QUAST.sh'**

```shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) Activate Python 3 environment (name = Assembly)
conda activate Assembly

# (2) Install QUAST
conda install -c bioconda quast

# (3) Confirm installation
quast.py --version
# Output should be: QUAST v5.0.2

# (4) Close environment
conda activate
```


#   ```D5. ABACAS```

ABACAS uses MUMmer to find alignment positions of assembled contigs against a 
REFERENCE genome.
Ordering contigs requires a REFERENCE genome. 
REFERENCE genome is specified in: 
**Reference_List_ABACAS.txt** ( Explained in *Usage* section ).

- Website: [ABACAS](http://abacas.sourceforge.net/)
- Version: **v 1.3.1**
- ABACAS is installed into DIR [ bin ]
***
- Website: [MUMmer](http://mummer.sourceforge.net/)
- Version: **v 3.23**
- Install in Python 3 environment
- Mummer and dependencies are installed through Conda
***
- Required to run **'16_Assembly_QUAST.sh'**

```shell
##################      INSTALLATION INSTRUCTIONS   ############################


# (1) cd into DIR [ bin ] and create DIR [ ABACAS ]
cd bin/
mkdir ABACAS

# (2) Change into DIR [ ABACAS ] and download latest installation
cd ABACAS/
wget https://sourceforge.net/projects/abacas/files/abacas.1.3.1.pl

# (3) Alter file permissions
chmod 755 *

# (4) Confirm installation
perl abacas.1.3.1.pl -h 

# (5) Activate Python 3 environment
conda activate mypython3

# (6) Install MUMmer
conda install -c bioconda mummer

# (7) Confirm installation
mummer -help | head -n 1
# Output should be: Usage: mummer [options] <reference-file> <query-files>

# (8) Close Python 3 environment
conda activate
```


#   ```D6. Prokka```

Whole genome annotation tool. 
Uses *blast* for similarity searches
against protein sequence libraries and *HMMER* for similarity searching
against protein family profiles. Sequence records are prepared for Genbank
submission with *tbl2asn*.

Annotation can use a curated REFERENCE genome to annotate from. 
REFERENCE genome is specified in: 
**Reference_List_PROKKA.txt** ( Explained in *Usage* section ).

- Website: [Prokka](https://github.com/tseemann/prokka)
- Prokka installs the following software:
    - *Aragorn* is used to find transfer RNA features (tRNA)
    - *Prodigal* is used to find protein-coding features (CDS)
    - *Infernal* is used for similarity searches against ncRNA family profiles
    - *barrnap* is used to predict ribosomal RNA features (rRNA).
    - *minced* is used to find CRISPR arrays
- Version: **v 1.14.6**
- Install in: base environment
- Prokka and dependencies are installed through Conda
***
- Required to run **'19_Assembly_Annotation.sh'**

```shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) Activate base environment
conda activate

# (2) Install Prokka
conda install -c conda-forge -c bioconda -c defaults prokka

# (3) Confirm installation
prokka --version
# Output should be: prokka 1.14.6
```


#   ```D7. Roary``` 

Roary takes GFF3 format files (produced by Prokka) and calculates the pan-genome. 
It generates a *gene presence and absence spreadsheet*, 
then it performs a pan-genome wide association analysis.

- Website: [Roary](https://sanger-pathogens.github.io/Roary/)
- Version: **v 3.13.0**
- Install in: base environment
- Roary and dependencies are installed through Conda
***
- Required to run **'21_Assembly_Roary.sh'**

``` shell
##################      INSTALLATION INSTRUCTIONS   ############################

# (1) Activate base environment
conda activate

# (2) Install Roary
conda install roary  

# (3) Check version
roary --version
# Output should be: 
# Use of uninitialized value in require at /home/miniconda3/lib/site_perl/5.26.2/x86_64-linux-thread-multi/Encode.pm line 61.
# 3.13.0
```


# E. Analysis

Most of the analysis of the data generated with software indicated above will 
be done on **R**. Before installing any package make sure you are running the
indicated **version of R**.

#   ```E1. SNP distances```
The following packages should be installed in **R version 3.6.1**.
The following packages are required:

- magrittr **version = 1.5**
- Website: [magrittr](https://cran.r-project.org/web/packages/magrittr/vignettes/magrittr.html)
- openxlsx **version = 4.1.4**
- Website: [openxlsx](https://cran.r-project.org/web/packages/openxlsx/openxlsx.pdf)
- RColorBrewer **version = 1.1-2**
- Website: [RColorBrewer](https://cran.r-project.org/web/packages/RColorBrewer/RColorBrewer.pdf)
- ComplexHeatMap **version = 2.2.0**
- Website: [ComplexHeatMap](https://jokergoo.github.io/ComplexHeatmap-reference/book/)

- Tidyverse **version = 1.3.0**
- Website: [Tidyverse](https://www.tidyverse.org/packages/)
    - dplyr **version = 0.8.3**
    - Website: [dplyr](https://cran.r-project.org/web/packages/dplyr/vignettes/dplyr.html)

``` shell
##################      INSTALLATION INSTRUCTIONS   ############################
# Open R studio:

# (1) Install packages through CRAN
install.packages( "tidyverse" )
# Installed with package
# - dplyr: A Grammar of Data Manipulation (version = 0.8.3)
# - ggplot2: Create elegant data visualizations using the grammar of graphics (version = 3.2.1)

 
# "magrittr" intalled with package          # magrittr - A Forward-Pipe Operator for R
install.packages( "openxlsx" )              # openxlsx - Read, Write and Edit xlsx Files
install.packages( "RColorBrewer")           # RColorBrewer - ColorBrewer Palettes

# (2) Install Bioconductor manager
if ( !requireNamespace( "BiocManager", quietly = TRUE ) )
    install.packages( "BiocManager" )
BiocManager::install( version = "3.10" )

# (3) Install Bioconductor packages
BiocManager::install( "ComplexHeatmap" )    # Make Complex Heatmaps
# "RColorBrewer" installed with package     # ColorBrewer Palettes


```



# III. Usage

= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

**How to run the analysis?**

= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =


# ``` 1. Quality control ```


The scripts in this section are for *quality control* of FASTQ files. FASTQ files are trimmed and adapters removed.
Quality controls report of FASTQ files are also generated through FastQC.
The end goal is generating the following data:
- **Paired Trimmed FASTQ files**
- **MultiQC reports**

Starting **input files** are Illumina reads:
- FASTQ raw reads from a sample *(Forward (R1) and Reverse (R2))*.
- *Sample name* should be written with no underscores or special characters followed by
an *underscore* and wether it is the forward read *(R1)* or the reverse read *(R2)*:
    - SAMPLENAME_R1.fastq.gz
    - SAMPLENAME_R2.fastq.gz

Files required to run **Quality control** scripts:
- **Input files** should be in *DIR data*
- Scripts should be in *DIR src*
- The following scripts are used:
    - QualityControl1_FastQCpre.sh  -- *Generate raw FASTQ quality control report*
    - QualityControl2_MultiQC.sh    -- *Compile FASTQC reports from all samples*
    - QualityControl3_Trim.sh       -- *Trim FASTQ files*
    - QualityControl4_FastQCpost.sh -- *Generate trimmed FASTQ quality control report*
    - QualityControl5_MultiQC.sh    -- *Compile FASTQC reports from all samples*
- DIR structure indicated below.
```shell
##################  Starting DIR Structure   ############################
  .
  ├── bin
  ├── projects
  |   └── ("Your_PROJECT_name")
  |       ├── data
  |       |   ├── SAMPLENAME_R1.fastq.gz
  |       |   └── SAMPLENAME_R2.fastq.gz
  |       ├── results
  |       └── src
  └── reference
```
**Figure A:** *Quality control scripts workflow.* 

*Each script will generate the required input files for the next one when necessary*
![Quality Control Workflow](figures/FigureA_QualityControl.jpg)


# ``` 2. Taxonomy ```


The scripts in this section are for *initial taxonomic identification* of the samples (Species and Sequence Type).
The end goal is generating the following data:
- **Table with the percent of reads belonging to FAMILY - GENUS and SPECIES**
- **Table withe the Sequence Type (ST) classification**

Starting **input files** were generated in the section *Quality control*:
- Trimmed paired FASTQ files from a sample *(Forward (R1) and Reverse (R2))*.
- *Sample name* is written with no underscores or special characters followed by
an *underscore* and wether it is the forward read *(R1)* or the reverse read *(R2)*:
    - SAMPLENAME_R1_paired.fastq.gz
    - SAMPLENAME_R2_paired.fastq.gz

Files required to run **Taxonomy** scripts:
- **Input files** are already in the appropiate DIR after finishing *Quality control* section
- Scripts should be in *DIR src*
- The following scripts are used:
    - Taxonomy1_Kraken.sh           -- *Generate report with family, genus and species classification for each sample*
    - Taxonomy2_MLST.sh             -- *Generate report with ST classification for each sample*
- DIR structure indicated below.
```shell
##################  Starting DIR Structure   ############################
  
  .
  ├── bin
  ├── projects
  |   └── ("Your_PROJECT_name")
  |       ├── data
  |       ├── results
  |       └── src
  └── reference
```
**Figure B:** *Taxonomy scripts workflow.*

*Each script will generate the required input files for the next one when necessary*
![Taxonomy workflow](figures/FigureB_Taxonomy.jpg)


# ``` 3. Read mapping to a reference - SNPs ```


The scripts in this section are for *variant identification* (SNP and indel).
Large structural variations (duplications and large deletions) are not covered.
SPANDX (Synergised Pipeline for Analysis of NGS Data in Linux) is the comparative genomics analysis tools used.
The end goal is generating the following data for further analysis:
- **Indel matrix**
- **Core genome annotated SNP matrix**
- **Nexus file to generate a phylogenetic tree**
- **Phylogenetic tree**

Starting **input files** were generated in the section *Quality control*:
- Trimmed paired FASTQ files from a sample *(Forward (R1) and Reverse (R2))*.

A set of **Reference genomes** [Including an outgroup] are downloaded from NCBI to inform the construction of the phylogenetic tree:
- Reference genomes artificial FASTQ reads will be generated after running script *ReadMapping0_References.sh*
- The list of *reference genomes* to download is present in the text file **Reference_List_1.txt**. 
- *Reference names* should be written with no underscores or special characters.
- Text file *Reference_List_1.txt* is shown below:
```shell
##################  REFERENCE LIST 1   ############################

# Reference_List_1.txt
# Reference genomes list [text file] is TAB separated and contains four columns
# 1st column:   common_name     Is a unique and abbreviated name for the organism (should match NCBI identifier)
# 2nd column:   format          Should indicate the file specification (In this case fna which is a FASTA file)
# 3rd column:   FTP_site        Is the link to where the file is deposited (assembly deposited in NCBI)
# 4th column:   phylogeny       Optional - For internal records describing the phylogeny

common_name format FTP_site phylogeny
ElbertiiKF1  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/512/125/GCF_000512125.1_ASM51212v1/GCF_000512125.1_ASM51212v1_genomic.fna.gz Out
EcoliMG1655  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz A
EcoliREL606  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/017/985/GCF_000017985.1_ASM1798v1/GCF_000017985.1_ASM1798v1_genomic.fna.gz A
EcoliW3110 fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/245/GCF_000010245.2_ASM1024v1/GCF_000010245.2_ASM1024v1_genomic.fna.gz A
EcoliUMNK88  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/212/715/GCF_000212715.2_ASM21271v2/GCF_000212715.2_ASM21271v2_genomic.fna.gz A
EcoliATCC8739  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/019/385/GCF_000019385.1_ASM1938v1/GCF_000019385.1_ASM1938v1_genomic.fna.gz A
Ecoli11368 fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/091/005/GCF_000091005.1_ASM9100v1/GCF_000091005.1_ASM9100v1_genomic.fna.gz B1
EcoliIAI1  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/026/265/GCF_000026265.1_ASM2626v1/GCF_000026265.1_ASM2626v1_genomic.fna.gz B1
Ecoli55989 fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/026/245/GCF_000026245.1_ASM2624v1/GCF_000026245.1_ASM2624v1_genomic.fna.gz B1
EcoliSE11  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/385/GCF_000010385.1_ASM1038v1/GCF_000010385.1_ASM1038v1_genomic.fna.gz B1
EcoliE24377A fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/017/745/GCF_000017745.1_ASM1774v1/GCF_000017745.1_ASM1774v1_genomic.fna.gz B1
EcoliNissle1917  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/714/595/GCF_000714595.1_ASM71459v1/GCF_000714595.1_ASM71459v1_genomic.fna.gz B2
EcoliNRG857c fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/183/345/GCF_000183345.1_ASM18334v1/GCF_000183345.1_ASM18334v1_genomic.fna.gz B2
EcoliSE15  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/485/GCF_000010485.1_ASM1048v1/GCF_000010485.1_ASM1048v1_genomic.fna.gz B2
Ecoli536 fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/013/305/GCF_000013305.1_ASM1330v1/GCF_000013305.1_ASM1330v1_genomic.fna.gz B2
EcoliCFT073  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/007/445/GCF_000007445.1_ASM744v1/GCF_000007445.1_ASM744v1_genomic.fna.gz B2
EcoliUTI89 fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/013/265/GCF_000013265.1_ASM1326v1/GCF_000013265.1_ASM1326v1_genomic.fna.gz B2
EcoliLF82  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/284/495/GCF_000284495.1_ASM28449v1/GCF_000284495.1_ASM28449v1_genomic.fna.gz B2
Ecoli042 fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/027/125/GCF_000027125.1_ASM2712v1/GCF_000027125.1_ASM2712v1_genomic.fna.gz D1
EcoliUMN026  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/026/325/GCF_000026325.1_ASM2632v1/GCF_000026325.1_ASM2632v1_genomic.fna.gz D1
EcoliIAI39 fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/026/345/GCF_000026345.1_ASM2634v1/GCF_000026345.1_ASM2634v1_genomic.fna.gz D2
EcoliSMS35 fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/019/645/GCF_000019645.1_ASM1964v1/GCF_000019645.1_ASM1964v1_genomic.fna.gz D2
EcoliCE10  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/227/625/GCF_000227625.1_ASM22762v1/GCF_000227625.1_ASM22762v1_genomic.fna.gz D2
EcoliTW14359 fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/022/225/GCF_000022225.1_ASM2222v1/GCF_000022225.1_ASM2222v1_genomic.fna.gz E
Ecoli4115  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/021/125/GCF_000021125.1_ASM2112v1/GCF_000021125.1_ASM2112v1_genomic.fna.gz E
EcoliSakai fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/008/865/GCF_000008865.2_ASM886v2/GCF_000008865.2_ASM886v2_genomic.fna.gz E
EcoliEDL933  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/732/965/GCF_000732965.1_ASM73296v1/GCF_000732965.1_ASM73296v1_genomic.fna.gz E
EcoliCB9615  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/025/165/GCF_000025165.1_ASM2516v1/GCF_000025165.1_ASM2516v1_genomic.fna.gz E
```
FASTQ reads will be aligned (read mapped) against a known annotated **Reference genome**.
- The *reference genome* used for read mapping is specified in the text file **Reference_List_2.txt**.
- Text file *Reference_List_2.txt* is shown below:
```shell
##################  REFERENCE LIST 2   ############################

# Reference_List_2.txt
# Reference genomes text file is TAB separated and contains four columns
# 1st column:   common_name     Is a unique and abbreviated name for the organism (should match NCBI identifier)
# 2nd column:   format          Should indicate the file specification (In this case fna which is a FASTA file)
# 3rd column:   FTP_site        Is the link to where the file is deposited (assembly deposited in NCBI)
# 4th column:   phylogeny       Optional - For internal records describing the phylogeny

common_name format FTP_site phylogeny
EcoliHS  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/017/765/GCF_000017765.1_ASM1776v1/GCF_000017765.1_ASM1776v1_genomic.fna.gz  A
```
Files required to run **Read mapping to a reference - SNPs** scripts:
- **Input files** are already in the appropiate DIR after finishing *Quality control* section
- Scripts should be in *DIR src*.
- The following scripts are used:
    - ReadMapping0_References.sh    -- *Generate artificial FASTQ files for reference genomes*
    - ReadMapping1_SPANDx.sh        -- *Generate SNP matrix*
    - ReadMapping2_RAxML.sh         -- *Generate phylogenetic tree*
    - ReadMapping3_SNPdistances.sh   -- *Generate SNP distance matrix*
- Reference genomes lists should be in *DIR reference*.
- The following lists are used:
    - Reference_List_1.txt          -- *Necessary for ART*
    - Reference_List_2.txt          -- *Necessary for SPANDx*
- DIR structure indicated below.
```shell
##################  Starting DIR Structure   ############################

  . 
  ├── bin
  ├── projects
  |   └── ("Your_PROJECT_name")
  |       ├── data
  |       ├── results
  |       └── src
  └── reference
      └── 'Reference_List_1.txt'
      └── 'Reference_List_2.txt'
```

Depending on the bacterial species used, the following sections might need
modification in the following scripts:
- Script **ReadMapping1_SPANDx.sh**: 
    - The reference name matching the snpEff database for variant annotation can be mofidied *[Section (2) - Lines 64, 65]*.
    - Additionally, the Reference and Outgroup names can becan be modified to be removed *[Section (12) - Lines 352, 356]*.
- Script **ReadMapping2_RAxML.sh**: 
    - The outgroup species can be mofidied *[Section (3) - Line 79]*:
```shell
##################  ReadMapping1_SPANDx.sh   ############################


# (2) Annotation Reference required for SPANDx
# ---------------------------------------------
# The name formatting of annotation reference for SPANDx is strict
# Name must exactly match database found in snpEff.config file

# (a) Define genome you are searching for - do not use caps
# Line 64
REF_snpEff_search=escherichia_coli_hs
# Line 65
REF_snpEff_result=Escherichia_coli_HS_uid58393

# (12) Remove Reference Genomes generated in script 'ReadMapping0_References'
#      - Ecoli
#      - Ealbertii
# -----------------------------------------------

# (a) Define input files
# Line 352
REFERENCE_NAME_1=$(find ../data/ -name 'Ecoli*.fastq.gz' | \
# Line 356
REFERENCE_NAME_2=$(find ../data/ -name 'Ealbertii*.fastq.gz' | \


##################  ReadMapping2_RAxML.sh   ############################


# (3) Outgroup rooting
# -----------------------------------------------
# Outgrouping is just a drawing option and does not affect tree inference
# (a) Specify the outgroup strain (Variable)
# Line 79
OutG=EalbertiiKF1
```
**Figure C:** *Read mapping to a reference - SNPs workflow* 

*Each script will generate the required input files for the next one when necessary*
![Variant identification workflow](figures/FigureC_Variant.jpg)


# ``` 4. Assembly ```

The scripts in this section are for *de novo assembly*. 
Contigs are generated, filtered and ordered against a reference genome.
Contigs not present in the reference genome are added to the end of the assembly.
The end goal is generating the following data for further analysis:
- **Annotated draft genome for each isolate**
- **Presence/Absence gene matrix**
- **Pangenome**

Starting **input files** were genereted in the section **Quality control**:
- Trimmed paired FASTQ files from a sample *(Forward (R1) and Reverse (R2))*.

Evaluation of genome assemblies with QUAST and 
ordering of contigs against a reference genome with ABACAS will
require you to specify a **Reference genome**.
- The *reference genome* used for QUAST and ABACAS specified in the text file **Reference_List_3.txt**.
- Text file *Reference_List_3.txt* is shown below:
```shell
##################  REFERENCE LIST 3   ############################

# Reference_List_3.txt
# Reference genomes text file is TAB separated and contains four columns
# 1st column:   common_name     Is a unique and abbreviated name for the organism (should match NCBI identifier)
# 2nd column:   format          Should indicate the file specification (In this case fna which is a FASTA file)
# 3rd column:   FTP_site        Is the link to where the file is deposited (assembly deposited in NCBI)
# 4th column:   phylogeny       Optional - For internal records describing the phylogeny

common_name format FTP_site phylogeny
EcoliHS  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/017/765/GCF_000017765.1_ASM1776v1/GCF_000017765.1_ASM1776v1_genomic.fna.gz  A
```
Annotation with Prokka can use a good quality **Reference genome** (genbank format) to ensure gene naming is consistent. 
- The *reference genome* used for Prokka is specified in the text file **Reference_List_4.txt**.
- Text file *Reference_List_4.txt* is shown below:
```shell
##################  REFERENCE LIST 4   ############################

# Reference_List_4.txt
# Reference genomes text file is TAB separated and contains four columns
# 1st column:   common_name     Is a unique and abbreviated name for the organism (should match NCBI identifier)
# 2nd column:   format          Should indicate the file specification (In this case gbff which is a Genbank flat file format)
# 3rd column:   FTP_site        Is the link to where the file is deposited (assembly deposited in NCBI)
# 4th column:   phylogeny       Optional - For internal records describing the phylogeny

common_name format FTP_site phylogeny
EcoliHS gbff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/017/765/GCF_000017765.1_ASM1776v1/GCF_000017765.1_ASM1776v1_genomic.gbff.gz  A
```
Roary pipeline creates a pangenome from a set of isolates.
A set of **Reference genomes** are downloaded from NCBI (GFF format) to inform the construction of the pangenome:
- The list of *reference genomes* to download is present in the text file **Reference_List_5.txt**. 
- *Reference names* should be written with no underscores or special characters.
- Text file *Reference_List_5.txt* is shown below:
```shell
##################  REFERENCE LIST 5   ############################

# Reference_List_5.txt
# Reference genomes text file is TAB separated and contains four columns
# 1st column:   common_name     Is a unique and abbreviated name for the organism (should match NCBI identifier)
# 2nd column:   format          Should indicate the file specification (In this case gff which is a  Generic Feature Format Version 3 (GFF3))
# 3rd column:   FTP_site        Is the link to where the file is deposited (assembly deposited in NCBI)
# 4th column:   phylogeny       Optional - For internal records describing the phylogeny

common_name format FTP_site phylogeny
EcoliMG1655 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.gff.gz A
EcoliREL606 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/017/985/GCF_000017985.1_ASM1798v1/GCF_000017985.1_ASM1798v1_genomic.gff.gz A
EcoliW3110  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/245/GCF_000010245.2_ASM1024v1/GCF_000010245.2_ASM1024v1_genomic.gff.gz A
EcoliUMNK88 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/212/715/GCF_000212715.2_ASM21271v2/GCF_000212715.2_ASM21271v2_genomic.gff.gz A
EcoliATCC8739 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/019/385/GCF_000019385.1_ASM1938v1/GCF_000019385.1_ASM1938v1_genomic.gff.gz A
Ecoli11368  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/091/005/GCF_000091005.1_ASM9100v1/GCF_000091005.1_ASM9100v1_genomic.gff.gz B1
EcoliIAI1 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/026/265/GCF_000026265.1_ASM2626v1/GCF_000026265.1_ASM2626v1_genomic.gff.gz B1
Ecoli55989  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/026/245/GCF_000026245.1_ASM2624v1/GCF_000026245.1_ASM2624v1_genomic.gff.gz B1
EcoliSE11 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/385/GCF_000010385.1_ASM1038v1/GCF_000010385.1_ASM1038v1_genomic.gff.gz B1
EcoliE24377A  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/017/745/GCF_000017745.1_ASM1774v1/GCF_000017745.1_ASM1774v1_genomic.gff.gz B1
EcoliNissle1917 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/714/595/GCF_000714595.1_ASM71459v1/GCF_000714595.1_ASM71459v1_genomic.gff.gz B2
EcoliNRG857c  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/183/345/GCF_000183345.1_ASM18334v1/GCF_000183345.1_ASM18334v1_genomic.gff.gz B2
EcoliSE15 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/010/485/GCF_000010485.1_ASM1048v1/GCF_000010485.1_ASM1048v1_genomic.gff.gz B2
Ecoli536  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/013/305/GCF_000013305.1_ASM1330v1/GCF_000013305.1_ASM1330v1_genomic.gff.gz B2
EcoliCFT073 gff ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/007/445/GCF_000007445.1_ASM744v1/GCF_000007445.1_ASM744v1_genomic.gff.gz B2
EcoliUTI89  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/013/265/GCF_000013265.1_ASM1326v1/GCF_000013265.1_ASM1326v1_genomic.gff.gz B2
EcoliLF82 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/284/495/GCF_000284495.1_ASM28449v1/GCF_000284495.1_ASM28449v1_genomic.gff.gz B2
Ecoli042  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/027/125/GCF_000027125.1_ASM2712v1/GCF_000027125.1_ASM2712v1_genomic.gff.gz D1
EcoliUMN026 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/026/325/GCF_000026325.1_ASM2632v1/GCF_000026325.1_ASM2632v1_genomic.gff.gz D1
EcoliIAI39  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/026/345/GCF_000026345.1_ASM2634v1/GCF_000026345.1_ASM2634v1_genomic.gff.gz D2
EcoliSMS35  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/019/645/GCF_000019645.1_ASM1964v1/GCF_000019645.1_ASM1964v1_genomic.gff.gz D2
EcoliCE10 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/227/625/GCF_000227625.1_ASM22762v1/GCF_000227625.1_ASM22762v1_genomic.gff.gz D2
EcoliTW14359  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/022/225/GCF_000022225.1_ASM2222v1/GCF_000022225.1_ASM2222v1_genomic.gff.gz E
Ecoli4115 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/021/125/GCF_000021125.1_ASM2112v1/GCF_000021125.1_ASM2112v1_genomic.gff.gz E
EcoliSakai  gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/008/865/GCF_000008865.2_ASM886v2/GCF_000008865.2_ASM886v2_genomic.gff.gz E
EcoliEDL933 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/732/965/GCF_000732965.1_ASM73296v1/GCF_000732965.1_ASM73296v1_genomic.gff.gz E
EcoliCB9615 gff  ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/025/165/GCF_000025165.1_ASM2516v1/GCF_000025165.1_ASM2516v1_genomic.gff.gz E
```
Files required to run **Assembly** scripts:
- **Input files** are already in the appropiate DIR after finishing *Quality control* section 
- Scripts should be in *DIR src*.
- - The following scripts are used:
    - Assembly1_SPAdes.sh           -- *Generate raw FASTQ quality control report*
    - Assembly2_ContigFiltering.sh  -- *Filter contigs with low coverage or less than 500 bp*
    - Assembly3_Coverage.sh         -- *Calculate read coverage based on the estimated assembly size*
    - Assembly4_QUAST.sh            -- *Evaluate genome assemblies with QUAST. Generate QUAST report*
    - Assembly5_MultiQC.sh          -- *Compile QUAST reports from all samples*
    - Assembly6_ABACAS.sh           -- *Order contigs against a reference genome*
    - Assembly7_Annotation.sh       -- *Annotate genome with Prokka*
    - Assembly8_MultiQC.sh          -- *Compile summary results from the Prokka annotation pipeline*
    - Assembly9_Roary.sh            -- *Calculates the pan genome*
- Reference genomes lists should be in *DIR reference*.
- The following lists are used:
    - Reference_List_3.txt          -- *Necessary for QUAST and ABACAS*
    - Reference_List_4.txt          -- *Necessary for Annotation with Prokka*
    - Reference_List_5.txt          -- *Necessary for Roary*
- DIR structure indicated below.
```shell
##################  Starting DIR Structure   ############################

  .
  ├── bin
  ├── projects
  |   └── ("Your_PROJECT_name")
  |       ├── data
  |       ├── results
  |       └── src
  └── reference
      ├── 'Reference_List_3.txt'
      ├── 'Reference_List_4.txt'
      └── 'Reference_List_5.txt'
```
**Figure D:** *Assembly scripts workflow.* 

*Each script will generate the required input files for the next one when necessary*
![Assembly workflow](figures/FigureD_Assembly.jpg)


# IV. Results


# ``` 1. Quality control ```

The first step of quality control was running the sequences through FastQC.
- *First:* Run **QualityControl1_FastQCpre.sh** to generate FastQC reports
- *Second:* Run **QualityControl2_MultiQC.sh** to generate a MultiQC report. The report is deposited in */results/MultiQC/*

Results:
- All samples have a sequence length of 151 bp as expected
- 48 out 84 samples had 10% of the library with duplication levels >10
- Overall the mean quality phred scores were above 30 for all reads

**Figure 1:** *FastQC results before running the samples through Trimmomatic.*
*Mean Quality Scores, Per Sequence Quality Scores and Sequence Duplication levels are shown.*

![FastQC results](figures/Figure1_FastQC.jpg)

The next step of quality control was running the sequences through Trimmomatic
to remove sequencing adapters.

- *Third:* Run **QualityControl3_Trim.sh** to trim FastQ reads

The trimming parameters used were the following:

```shell
# Define the variable of the adapter
# TruSeq3 adapters are used by HiSeq and MiSeq machines
# Adapter used below: Line 76
Adapter=../../../bin/Trimmomatic-0.38/adapters/TruSeq3-PE-2-NexteraPE-PE_combined.fa

trimmomatic_options=(
    ILLUMINACLIP:$Adapter:2:30:10:8:true \
# ILLUMINACLIP
# Cut adapter and illumina-specific sequences from the read
#   * Remove Illumina adapters provided in TruSeq3-PE-2-NexteraPE-PE_combined.fa file
#   * Looks for seeds matches (16 bases) allowing maximally 2 mismatches
#   * Seeds are extended and clipped if PE reads score 30 (around 50 bp)
#   * Seeds are extended and clipped if SE reads score 10 (around 17 bp)
#   * Detect minimum length of adapter (default = 8 bases)
#   * "true" retains the reverse read
	SLIDINGWINDOW:4:15 \
# SLIDINGWINDOW:
# Performs a sliding window trimming approach
#   * Scan the read with a 4-base sliding window
#   * Cuts the read when the average quality per base drops below 15
	MINLEN:36 \
# MINLEN:
# Drop the read if it is below a specified length
#   * Less than 36 bases
)
```

The last step of quality control was running the trimmed sequences through FastQC.
- *Fourth:* Run **QualityControl4_FastQCpost.sh** to generate FastQC reports post-trimming
- *Fifth:* Run **QualityControl5_MultiQC.sh** to generate a MultiQC report. The report is deposited in */results/MultiQC/*

Results:
- After trimming most of the sequences have a length between 147 - 151 bp
- 47 out 84 samples had 10% of the library with duplication levels >10
- Overall the mean quality phred scores were above 30 for all reads

**Figure 2:** *FastQC results after running the samples through Trimmomatic.*
*Mean Quality Scores, Per Sequence Quality Scores, Sequence Length and Sequence Duplication levels are shown.*
![FastQC results](figures/Figure2_FastQC.jpg)

# ``` A. Taxonomy ```

Running the script **06_TaxonomyClassification_Kraken.sh** will generate a CSV
file (*Output from Kraken2*) containing the percentage of reads assigned to a 
Domain, Family, Genus and Species as well as the percentage of Unclassified reads 
per sample.

From the total number of samples analyzed **(n = 48)**, the number of samples
classied as *E. coli* at the Species level were **(n = 41)**. Samples not 
classified as *E. coli* will not be used for further analysis (phylogenetic tree 
generation).

**Results Figure 02:** *CSV file containing taxonomic classification (output from*
*Kraken2). CSV file is deposited in DIR [ Results ]. Only the first six rows of*
*the file are shown.*
![Results - Taxonomy](figures/Results_Figure02_Taxonomy.jpg)


# ``` 3. MLST ```

# V. Results ASSEMBLY

# ``` 1. Assembly and coverage ```

The first step of assembly is generating the de novo assemblies with **SPAdes**. 
Several reports show that SPAdes outperforms other popular assemblers such as **Velvet**.
SPAdes produce higher N50 length and larger contigs. 
Compared to other assemblers SPAdes conducts assembly multiple times using different k-mers.
- *First:* Run **Assembly1_SPAdes.sh** to generate a **contigs.fasta** file containing all assemblies (contigs)
- *Second:* Run **Assembly2_ContigFiltering.sh** to filter high coverage contigs and generate **HCov.contigs.fasta**
- *Third:* Run **Assembly3_Coverage.sh** to generate a **CSV** file with sequencing coverage

Results:
- After assembly only high coverage contigs **> 2.0** were kept
- Additionally a second filtering parameter was discarding any contig **< 500 bp**
- Below is a section of **Assembly2_ContigFiltering.sh** showing the filtering parameters
```shell
# (e) Contig filtering
echo ['FILTERING CONTIGS in'] $Input1
# Line 100: 	Extract all contigs
# Line 101:		Remove underscore from contig name
# Line 102:		Numeric, reverse and key sort (Column 6 = coverage value)
# Line 103:		Coverage more than 2.0 and length more than 500 bp
# Line 104:		Restore underscore from contig
# Line 105:		Remove '>' from contig name
grep -F ">" $Input1 | \
sed -e 's/_/ /g' | \
sort -nrk 6 | \
awk '$6 >= 2.0 && $4 >= 500 {print $0}' | \
sed -e 's/ /_/g' | \
sed -e 's/>//g' > $Output1
```
- After filtering the contig, the coverage was calculated using the predicted genome size after assembly
- **Coverage** refers to the average number of reads per locus.
- When using short-read technologies, high total read coverage (> 100x) is needed
- Coverage was calculated by:
```shell
#To calculate actual sequencing depth/coverage
#sequencing depth= LN/G =average read length * average reads / genome size
```
- All samples had a coverage larger than **150x**

**Figure 10:** *Highlighted in orange is the sequencing coverage*
*Hihglighted in yellow are the reference isolates where atificial FASTQ files were generated*

![Coverage results](figures/Figure10_Coverage.jpg)

# VI. Appendix

# ``` 1. PBS Script Writer setup ```

To run any of the scripts indicated above we used the high-performance (HPC)
cluster from the University of Michigan, **Flux**. [Flux](https://arc-ts.umich.edu/flux/)

To submit any jobs to the FLux cluster you need to use **Torque PBS** (Portable Batch System).
It controls the running jobs in the Flux cluster. [PBS](https://arc-ts.umich.edu/flux-user-guide/)

This sections contains the instructions to use **PBS Script Writer (pbsw)**. 
PBSw is a function to assist with generating PBS scripts *(.pbs)* and submiting them *(qsub)*.
Use of this feature is entirely optional but may simplify and expedite the process.

- Navigate to **DIR src** where scripts are run from. *All scripts are run from DIR src*
``` shell
# (1) Navigate DIR src of your project
.
+-- bin
+-- projects
¦   +-- Project_name
¦       +-- data
¦       +-- results
¦       +-- src
+-- reference
```

- Executable **pbsw** is deposited into DIR bin
- Once you are in DIR src from the project you are working on:
``` shell 
# (2) Copy pbsw temporarily to DIR
cp ../../../bin/pbsw .
```

- Make sure the script you will be running **is also present in DIR src**
- Basic structure of **pbsw** is as follows:
``` shell
# Structuture of pbsw executable
./pbsw --email [PBS arguments] my_script.sh "script_arguments"

./pbsw              # Execute pbsw
--email             # Username and e-mail where notifications will be sent
[PBS arguments]     # Number of nodes, memory and walltime
my_script.sh        # Name of the script that will be submitted
"script_arguments"  # Modify arguments within the script such as threads to use
```

- Example:
    - To run the first script in the assembly section (Section E)
    - Script name: **Assembly1_SPAdes.sh**
    - Username: The username will be extracted from my Umich e-mail. In this case **me**
    - PBS arguments: Here I enter the information regarding number of processors (ppn) to use the memory per processor (pmem) and the time (walltime)
    - script arguments: The number processors (threads) entered here will automatically modify the script **Assembly1_SPAdes.sh**
 - Important:
    - The number of processors requested **ppn** should match the number of **threads** indicated (Look at the script below)
    - If not indicated the default number of **threads** will be 8 if the script permits it. Not all the scripts have the option to specify threads

``` shell
# Example of submussion to Flux of script Assembly1_SPAdes.sh
# Job is requiring 8 processors (ppn) all in 1 node
# We are requiring each processor with a memory of 10gb
# A total time of 3 days
# Make sure the ppn number matches the number of threads
./pbsw --email=me@med.umich.edu --pbs_l="nodes=1:ppn=8,pmem=10gb,walltime=72:00:00" Assembly1_SPAdes.sh "--threads=8"
# After executing ./pbsw it will show you a draft of your PBS script
# Look over the draft and answer YES to submit to the scheduler or NO to start over
```
- For most jobs the following specifications are sufficient. I try not to change them:
    - **nodes=1** - Try to request all processors in 1 node
    - **ppn=8** - Eight processros
    - **pmem=10gb** - 10gb of RAM per processors. This equals 80gb of RAM which is plenty for most jobs
    - **walltime** - I always require time in excess to avoid the job being interrupted. Time will depend of the number of samples analyzed

See [here](https://github.com/hilldr/pbsw) for details on how to set up a local config file.
**pbsw** was written by David R. Hill.

