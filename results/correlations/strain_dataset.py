import pandas as pd
## CREATE STRAIN DATABASE
## read in Tarr strain data
tarr_data = pd.read_csv('../../data/Copy of V.Young_MI_Sendout.for.PT scrubbed.csv',
                        index_col=['TL number or ID#'])
## reformat strain number
tarr_data.index = tarr_data.index.str.replace('-', '')

## read in species, phylotype, ST type designations
strain_data = pd.read_excel('../../data/Species_ST_Phylogroup.xlsx',
                            index_col=None)
strain_data = strain_data.set_index('Sample_ID')

strain_data = strain_data.join(tarr_data)
strain_data.to_csv(path_or_buf = "strain_clinical_phylogroup_dataset.csv", index=True)
