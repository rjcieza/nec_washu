import pandas as pd
import numpy as np

## CREATE HIO DATABASE

## read in excel
hio_data = pd.read_excel('../../data/Metadata_experimental_NEC.xlsx',
                     index_col=None)

## remove outdated permeability data
hio_data = hio_data.filter(regex='^((?!kDa).)*$', axis=1)

## read in barrier data
barrier = pd.read_csv('../barrier/data_auc.csv')
## trim to three key columns
barrier = barrier.filter(items=['channel', 'unique_id', 'thalf.alt'])
## rename columns
barrier = barrier.rename(index=str, columns={"thalf.alt": "thalf",
                                            "unique_id": "HIO_number"})
## remove rows containing channel=ratio
barrier = barrier.drop(barrier[barrier.channel == 'ratio'].index)
## reshape the data
barrier = pd.pivot(barrier, index="HIO_number", columns="channel",values="thalf")
## rename columns
barrier = barrier.rename(index=str, columns={"C0": "thalf_4kd",
                                             "C1": "thalf_70kd"})

## join the data
hio_data = hio_data.set_index('HIO_number')
hio_data.index = hio_data.index.astype(str, copy = False)
hio_data = hio_data.join(barrier)

## stratify growth data
hio_data['delta_CFU'] = hio_data.Output_per_HIO - hio_data.Input_per_HIO

## growth_status
cut_off = 10e4
growth_condition = [
    (hio_data['Output_per_HIO'] > cut_off),
    (hio_data['Output_per_HIO'] < cut_off) & (hio_data['Output_per_HIO'] > 0),
    (hio_data['Output_per_HIO'] == 0)]

stratify_growth = [
    'Robust Growth',
    'Weak Growth',
    'No Growth'
]

hio_data['growth_status'] = np.select(growth_condition, stratify_growth)

## colonization status [none, persistence, expansion]

colonization_condition = [
    (hio_data['Output_per_HIO'] > hio_data['Input_per_HIO']),
    (hio_data['Output_per_HIO'] < hio_data['Input_per_HIO']) & (hio_data['Output_per_HIO'] > 0),
    (hio_data['Output_per_HIO'] == 0)
]

stratify_colonization = [
    'Expansion',
    'Persistence',
    'Extinction'
]
hio_data['colonization_status'] = np.select(colonization_condition, stratify_colonization)

## write out to csv
hio_data.to_csv(path_or_buf = "ELISA_barrier_CFU_HIO_dataset.csv", index=True)
