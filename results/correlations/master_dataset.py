import pandas as pd
## CREATE MASTER DATABASE
strain_data = pd.read_csv('strain_clinical_phylogroup_dataset.csv',
                        index_col=['Sample_ID'])

hio_data = pd.read_csv("ELISA_barrier_CFU_HIO_dataset.csv", index_col=['Strain'])

all_data = hio_data.join(strain_data)
all_data.to_csv(path_or_buf = "master_dataset.csv", index=True)
