#!/bin/bash
## script for automating conversion of VSI images into data files
## David R. Hill
cd ../../bin/permeability/
for dir in $(find /media/david/Elements/live_imaging/NEC/ \
		  -maxdepth 1 \
		  -mindepth 1 \
		  -type d \! -name \*_snap*)
do
    ## first test T0 with a range of min threshold values
    xvfb-run -a ./src/estimate_threshold_value.sh $dir/ \
	     /home/david/nec_washu/results/barrier/ \
    	     --min_thresh="$(seq 0 500 20000)"
done

