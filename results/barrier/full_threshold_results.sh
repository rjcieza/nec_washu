#!/bin/bash
## script for automating conversion of VSI images into data files
## David R. Hill
cd ../../bin/permeability/
## automatically start full analysis using threshhold of 60000
for dir in $(find /media/david/Elements/live_imaging/NEC/ \
		  -maxdepth 1 \
		  -mindepth 1 \
		  -type d \! -name \*_snap*)
do
    ## should prompt for user input of min thresh
    echo "STARTING ANALYSIS OF $dir"    
    xvfb-run -a ./src/barrier-pipeline.sh $dir/ \
	     /home/david/nec_washu/results/barrier/ \
	     --min_thresh="3000"
done
