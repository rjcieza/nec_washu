#!/bin/bash

# -----------------------------------------------------------------------------
# SCRIPT:       13_Assembly_SPAdes.sh
# Written by:   Cieza RJ
# PURPOSE:      Genome assembly with SPAdes
# -----------------------------------------------------------------------------

# SPAdes (v 3.12.0)
  # Genome assembly algorithm / toolkit containing various assembly pipelines
  # http://cab.spbu.ru/files/release3.12.0/manual.html

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  |   └── SPAdes-3.12.0-Linux
#  |        └── bin
#  └── projects
#      └── ("Your_PROJECT_name")
#          ├── data
#          ├── results
#          |   └── ("SAMPLE_name")
#          |        ├── SPAdes
#          |        └── Trimm
#          └── src


###############################################################################
# (00) Supply ARGUMENTS to SCRIPT from the command line
###############################################################################

# (00.1)
# -----------------------------------------------------------------------------
# Determine the number of processors used in the SCRIPT.

  # (a) Default VALUE
  # If no value is provided, 4 processors will be used
    PROCESSORS=4

# (00.2)
# -----------------------------------------------------------------------------
# User-supplied values (will overwrite default)

# ARGUMENTS will provided through the command Line
# [-p]
  while [[ "$1" == -* ]];
  do
    case $1 in
      # (a) PROCESSORS option
      # - - - - - - - - - - - - - - - -
      # The argument to the OPTION is placed in the variable $1
      -p|--processsors)
        shift
        PROCESSORS="$1"
        ;;
      # (b) END of arguments
      # - - - - - - - - - - - - - - - -
      --)
        shift
        break
        ;;
      # FINISHING the loop
      esac
      shift
    done

# (00.3)
# -----------------------------------------------------------------------------
# Log message

  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "
  echo " [ PROCESSORS used ] [ - $PROCESSORS - ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# Activate Python 3 ENVIRONMENT

  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate mypython3


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ SOFTWARE verification] "
  echo " ###############################################################"
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE version

# SPAdes should have been installed through Conda (view README)
# Check SPAdes version
  spades.py --version


###############################################################################
# (03) Run RAxML-NG
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ Running assembly with SPAdes ] "
  echo " ###############################################################"
  echo " "

# (03.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR "data_PROCESSED"

# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of unique names in the DIR (Keep only the sample name)
# Each sample has:
  	# Forward (*_R1.fastq.gz)
  	# Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
		  					sed 's/\_R[1-2].fastq.gz//' | \
		  					sort -u)

# (03.3)
# -----------------------------------------------------------------------------
# Run SPAdes & create a DIR to deposit the results for EACH sample

# For Loop
  for sample in $SAMPLE_NAME
  do

  # (a) Define DIRNAME
  # Contains only the SAMPLE name with no PATH
    DIRNAME=${sample##*/}

  # (b) Define INPUT
  # Use FASTQ reads after processing with cutadapt
	# Forward FASTQ read (R1)
		R1=$sample\_R1.fastq.gz
	# Reverse FASTQ read (R2)
		R2=$sample\_R2.fastq.gz

  # (c) Define OUTPUT
  # OUTPUT PATH
    OUTDIR_Temp=../results/$DIRNAME/SPAdes/Temp
    OUTDIR_Assembly=../results/$DIRNAME/SPAdes
  # Make DIR
    mkdir -p $OUTDIR_Temp
    mkdir -p $OUTDIR_Assembly

  # (d) Run SOFTWARE
  # SOFTWARE: SPAdes
  # USAGE:
  # -k:           k-mers to use:
  #               For multicell data sets:
  #               K values are automatically selected using maximum read length
  #               About effect of k-mer size:
  #               https://github.com/rrwick/Bandage/wiki/Effect-of-kmer-size
  # --threads:    Adjust accordingly - DEFAULT = 4
  # --careful:    Reduce the number of mismatches and short indels
  # --pe-1:       Paired-end file with forward reads (R1)
  # --pe-2:       Paired-end file with reverse reads (R2)
  # ----------
    spades.py --threads $PROCESSORS \
              --careful \
              --pe1-1 $R1 \
              --pe1-2 $R2 \
              -o $OUTDIR_Temp

  # (e) Append SAMPLE NAME to assembly OUTPUT files
  # Rename 'contigs.fasta'(Contains assembled OUTPUT contigs)
    mv $OUTDIR_Temp/contigs.fasta \
       $OUTDIR_Temp/$DIRNAME\_contigs.fasta
  # Rename 'scaffolds.fasta' file (contains assembled scaffolds)
    mv $OUTDIR_Temp/scaffolds.fasta \
       $OUTDIR_Temp/$DIRNAME\_scaffolds.fasta
  # Rename 'assembly_graph.fastg' (Contains SPAdes assembly graph in FASTG)
    mv $OUTDIR_Temp/assembly_graph.fastg \
       $OUTDIR_Temp/$DIRNAME\_assembly_graph.fastg

  # (f) Copy OUTPUT files to final DIR [ SPAdes ]
  # contigs.fasta
    cp $OUTDIR_Temp/$DIRNAME\_contigs.fasta \
       $OUTDIR_Assembly
  # scaffolds.fasta file
    cp $OUTDIR_Temp/$DIRNAME\_scaffolds.fasta \
       $OUTDIR_Assembly
  # assembly_graph.fastg
    cp $OUTDIR_Temp/$DIRNAME\_assembly_graph.fastg \
       $OUTDIR_Assembly

  # (g) Remove accesory files generated
    rm -r $OUTDIR_Temp

  # (h) COMPLETION message
    echo " "
	  echo " [ Assembly COMPLETE ] [ SAMPLE name: $DIRNAME ] "
	  echo " "
	  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	  echo " "

  done


#############################################################################
# (04) Return to base environment
#############################################################################

# (04.1)
# Return to the conda base ENVIRONMENT
  conda activate


###############################################################################
# END:
# SCRIPT: 13_Assembly_SPAdes.sh
###############################################################################
