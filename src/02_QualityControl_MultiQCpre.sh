#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       02_QualityControl_MultiQCpre.sh
# Written by:   Cieza RJ
# PURPOSE:      Summary statistics from FastQC (Pre-trimming)
# ---------------------------------------------------------------------------

# MultiQC (v 1.8)
	# Reporting tool that parses summary statistics of various tools
	# http://multiqc.info/docs/#using-multiqc

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  └── projects
#      └── ("PROJECT_name")
#          ├── data
#          ├── results
#          |   ├── QC_Pre								-- INPUT --
#   			 |	 ├── Reports_MultiQC			-- DIR removed upon completion --
#          |   └── MultiQC              -- OUTPUT --
#          ├── reference
#          └── src


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# Activate Python 3 ENVIRONMENT

  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh

  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate env_multiqc


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ SOFTWARE verification] "
  echo " ############################################################### "
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE

  # (a) MultiQC should have been installed through Conda (view README)
  # Check MultiQC version
    multiqc --version

  # (b) Exit if SOFTWARE is not detected
    if [ $? -eq 0 ]
    then
      echo " "
      echo " --- MultiQC is INSTALLED --- "
      echo " "
    else
      echo " "
      echo " --- MultiQC is NOT DETECTED --- "
      echo " --- EXIT script --- "
      echo " "
      exit 1
    fi


#############################################################################
# (03) Transfer FastQC reports (ZIP) to DIR [FastQC]
#############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message
	echo " "
	echo " ############################################################### "
	echo " [ Copying FastQC reports ] "
	echo " ############################################################### "
	echo " "

# (03.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR [ data ]

  # DIR [ data ] contains FASTQ paired end (PE) reads
  # Make a list of unique names in the DIR
  # Each SAMPLE has:    Forward (*_R1.fastq.gz)
  #                     Reverse (*_R2.fastq.gz)
  # find:   Find SAMPLES in DIR [ data]
  # -name:  List unique SAMPLE names
  # sed:    Remove the ending of file names only keeping the unique name
  # sort:   Sort by unique name
    SAMPLE_NAME=$(find ../data/ -name '*.fastq.gz' | \
          				sed 's/\_R[1-2].fastq.gz//' | \
          				sort -u)

# (03.3)
# -----------------------------------------------------------------------------
# Make DIR [ Reports_MultiQC ]

  # (a) PATH & Make DIR
    DIR_Reports=../results/Reports_MultiQC
    mkdir -p $DIR_Reports

# (03.4)
# -----------------------------------------------------------------------------
# Find FastQC reports and move to DIR [ Reports_MultiQC ]

# For Loop
for sample in $SAMPLE_NAME
do

  # (a) DIR name
  # Contains only the SAMPLE name with no PATH
    DIRNAME=${sample##*/}

  # (b) INPUT
    INP_report=$(find ../results/QC_Pre/ -name '*.zip')

  # (c) Determine if FastQC reports are present
    if [ -z "$INP_report" ];
    then
    # ERROR message
      echo " [ ERROR - No FastQC report found for: ] [ $DIRNAME ] "
      exit 1
    else
    # PASS message
      echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
      echo " [ FastQC report found for: ] [ $DIRNAME ] "
    fi

  # (d) Copy INPUT report (ZIP) into DIR [ Reports_MultiQC ]
    cp $INP_report \
    	 $DIR_Reports

  # (e) COMPLETION message
    echo " [ Copying FastQC report ] [ $DIRNAME ] [ COMPLETE ] "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "

  done


###############################################################################
# (04) Run MultiQC on DIR [ Reports_MultiQC ]
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# USER message
	echo " "
	echo " ############################################################### "
	echo " [ Running MultiQC on DIR [ Reports_MultiQC ] ] "
	echo " ############################################################### "
	echo " "

# (04.2)
# -----------------------------------------------------------------------------
# INPUT

  # (a) DIR PATH [ Reports_MultiQC ]
  # DIR was created in step 03
    DIR_Reports=../results/Reports_MultiQC

# (04.3)
# -----------------------------------------------------------------------------
# OUTPUT

  # (a) PATH & Make DIR
    OUT_MultiQC=../results/MultiQC
    mkdir -p $OUT_MultiQC
  # (b) MultiQC OUTPUT report
    OUT_Report_name=MultiQC_FastQC_Pre

# (04.4)
# -----------------------------------------------------------------------------
# Run MultiQC

	# Run SOFTWARE
  # SOFTWARE: MultiQC
  # USAGE:
	# --module:     Use only this module
	# --outdir:     Create report in the specified output directory
	# --filename:   Report filename
		multiqc --module fastqc \
            --outdir $OUT_MultiQC \
            --filename $OUT_Report_name \
            $DIR_Reports

# (04.5)
# -----------------------------------------------------------------------------
# COMPLETION message
  echo " "
	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	echo " [ Running MultiQC ] "
  echo " [ DIR = $DIR_Reports ] [ COMPLETE ] "
	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	echo " "


###############################################################################
# (05) Cleaning work ENVIRONMENT
###############################################################################

# (05.1)
# -----------------------------------------------------------------------------
# USER message
	echo " "
	echo " ############################################################### "
	echo " [ Cleaning Temporary files ] "
	echo " ############################################################### "
	echo " "

# (05.2)
# -----------------------------------------------------------------------------
# Remove DIR [ Reports_MultiQC ] if MultiQC is successful

  if [ -f "$OUT_MultiQC/$OUT_Report_name.html" ];
  then
  # PASS message
    echo " [ MultiQC report FOUND ] "
    echo " [ Removing temporary DIR and files ]"
    echo " "
  # Remove:
  # DIR containing ZIP reports [ Reports_MultiQC ]
    rm -r $DIR_Reports
  # Remove DIR containing individual HTML reports [ QC_Pre ]
    rm -r ../results/QC_Pre
  # Remove accesory files created when running MultiQC
    rm -r $OUT_MultiQC/$OUT_Report_name\_data
  else
  # FAIL message
    echo " "
    echo " [ MultiQC report NOT FOUND ] "
    echo " [ FAIL - Run MultiQC again ] "
    echo " "
    exit 1
  fi


###############################################################################
# (06) Return to base ENVIRONMENT
###############################################################################

# (06.1)
# -----------------------------------------------------------------------------
# Return to conda base ENVIRONMENT
  conda activate


#############################################################################
# END:
# SCRIPT: 02_QualityControl_MultiQCpre.sh
#############################################################################
