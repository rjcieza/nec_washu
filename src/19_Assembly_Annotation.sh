#!/bin/bash

# -----------------------------------------------------------------------------
# SCRIPT:       19_Assembly_Annotation.sh
# Written by:   Cieza RJ
# PURPOSE:      Genome annotation with Prokka
# -----------------------------------------------------------------------------

# Prokka (v 1.14.6)
  # Rapid annotation of prokaryotic genomes
  # https://github.com/tseemann/prokka

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  ├── projects
#      └── ("PROJECT_name")
#          ├── data
#          ├── data_PROCESSED
#          ├── results
#          |   └── Contigs_Ordered                 -- INPUT --
#          |        ├── Prokka_working             -- DIR removed when done --
#          |        └── Prokka                     -- OUTPUT --
#          |            ├── GBK
#          |            ├── Statistics
#          |            └── ProteinFASTA
#          ├── reference
#          |   └── Reference_List_PROKKA.txt       -- FILE removed when done --
#          └── src


###############################################################################
# (00) Supply ARGUMENTS to SCRIPT from the command line
###############################################################################

# (00.1)
# -----------------------------------------------------------------------------
# Determine the number of processors used in the SCRIPT.

  # (a) Default VALUE
  # If no value is provided, 4 processors will be used
    PROCESSORS=4

# (00.2)
# -----------------------------------------------------------------------------
# User-supplied values (will overwrite default)

# ARGUMENTS will provided through the command Line
# [-p]
  while [[ "$1" == -* ]];
  do
    case $1 in
      # (a) PROCESSORS option
      # - - - - - - - - - - - - - - - -
      # The argument to the OPTION is placed in the variable $1
      -p|--processsors)
        shift
        PROCESSORS="$1"
        ;;
      # (b) END of arguments
      # - - - - - - - - - - - - - - - -
      --)
        shift
        break
        ;;
      # FINISHING the loop
      esac
      shift
    done

# (00.3)
# -----------------------------------------------------------------------------
# Log message

  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "
  echo " [ PROCESSORS used ] [ - $PROCESSORS - ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# Activate base ENVIRONMENT

  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ############################################################### "
  echo " [ SOFTWARE verification] "
  echo " ############################################################### "
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE

  # (a) Prokka should have been installed through Conda (view README)
  # Check Prokka version
    prokka --version


###############################################################################
# (03) Download REFERENCE GENOME - Genbank File
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ STARTING REFERENCE GENOME download ] "
  echo " ############################################################### "
  echo " "

# (03.2)
# -----------------------------------------------------------------------------
# REFERENCE List TXT file:
# - Reference_List_PROKKA.txt
# - Reference_List_PROKKA.txt should be in DIR [ reference ]

  # (a) REFERENCE list VARIABLE (TXT file)
    List=../reference/Reference_List_PROKKA.txt

  # (b) Detecting the presence or absence of REFERENCE list
    if [ -e "$List" ];
    then
      echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
      echo " "
      echo " [ REFERENCE LIST exists - PROCEED ] "
      echo " "
    else
      echo " "
      echo " [ REFERENCE LIST does NOT exist - ABORT ] "
      echo " "
    fi

  # (c) Read TXT file and skip lines starting with #:
    grep -o '^[^#]*' $List > REFERENCE_list.txt

# (03.3)
# -----------------------------------------------------------------------------
# Download & process REFERENCE GENOME

# Read TXT file
# 1st column:   common_name
# 2nd column:   format
# 3rd column:   FTP_site
  while read common_name format FTP_site
  do

  # (a) Skip the header
    [ "$common_name" == "common_name" ] && continue

  # (b) Download REFERENCE from NCBI
  # Begin task message
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE download BEGINS ] [ REFERENCE name: $common_name ] "
    echo " "
  # Download genome
	  wget $FTP_site
  # End task message
	  echo " [ REFERENCE download COMPLETE ] [ REFERENCE name: $common_name ] "

  # (c) DIR where downloaded REFERENCE will be deposited
  # OUTPUT PATH
    OUT_REF_PATH=../reference/$common_name
  # Make DIR
    mkdir -p $OUT_REF_PATH

  # (d) Unzip GZ file
  # INPUT file
    REF_01=$(echo *.gz)
  # Unzip
    gunzip $REF_01

  # (e) Move unzipped file to DIR [ reference ]
  # INPUT file
    REF_02=$(echo *.$format)
  # Move file
    mv $REF_02 \
       $OUT_REF_PATH

  # (f) REFERENCE variable at DIR [reference]
    REF_03=$(echo $OUT_REF_PATH/*.$format)

  # (g) Copy REFERENCE to workind DIR [src]
    cp $REF_03 .

  # (h) Rename REFERENCE using VARIABLE '$common_name'
  # GBFF ending required
    mv ./$REF_02 \
       $common_name\.gbff

  # (i) Remove accesory files created
    rm $REF_03

  # (j) Remove accesory DIR created
  # REFERENCE PATH
    REF_DIR=$(echo $OUT_REF_PATH)
    rm -R $REF_DIR

  # (k) Loop completion message
   	echo " [ REFERENCE processing COMPLETE ] [ REFERENCE name: $common_name ]"
   	echo " "
   	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "

  done < REFERENCE_list.txt

# (03.4)
# -----------------------------------------------------------------------------
# Remove modified List generated

  # Temporary TXT file generated
    rm REFERENCE_list.txt


###############################################################################
# (04) Load Prokka DATABASES
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ############################################################### "
  echo " [ Load Prokka DATABASES ] "
  echo " ############################################################### "
  echo " "

  # (a) Loading Prokka DATABASES
  # Databases: Kingdoms, Genera, HMMs, CMs
    prokka --setupdb

  # (b) Confirm DATABASES are loaded
  # Output should be:
  # Looking for databases in: /home/$USER/miniconda3/db
  # Kingdoms: Archaea Bacteria Bacteria Bacteria Mitochondria Viruses
  # Genera: Enterococcus Escherichia Staphylococcus
  # HMMs: HAMAP
  # CMs: Bacteria Viruses
    echo " "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    prokka --listdb
    echo " "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "


###############################################################################
# (05) Run Prokka
###############################################################################

# (05.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ############################################################### "
  echo " [ Annotating genomes with Prokka ] "
  echo " ############################################################### "
  echo " "

# (05.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR [ data_PROCESSED ]

	# DIR [ data_PROCESSED ] contains FASTQ paired end (PE) reads
	# Make a list of unique names in the DIR
	# Each SAMPLE has:
	  	# Forward (*_R1.fastq.gz)
	  	# Reverse (*_R2.fastq.gz)
	# find:   Find SAMPLES in DIR [ data_PROCESSED ]
	# -name:  List unique SAMPLE names
	# sed:    Remove the ending of file names only keeping the unique name
	# sort:   Sort by unique name
		SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
			  					sed 's/\_R[1-2].fastq.gz//' | \
			  					sort -u)

# (05.3)
# -----------------------------------------------------------------------------
# REFERENCE GENOME to input it under Flag --proteins in Prokka

  # PATH to REFERENCE GENOME
    REF_PROKKA=$(echo *.gbff)

# (05.4)
# -----------------------------------------------------------------------------
# OUTPUT

  # (a) Working DIR to deposit temporary results
  # PATH & Make DIR
    OUT_DIR_work=../results/Prokka_working
    mkdir -p $OUT_DIR_work

	# (b) DIR to deposit ordered contigs
	# PATH & Make DIR
		OUT_DIR=../results/Prokka
    mkdir -p $OUT_DIR
    mkdir -p $OUT_DIR/GBK
    mkdir -p $OUT_DIR/Statistics
    mkdir -p $OUT_DIR/ProteinFASTA

# (05.5)
# -----------------------------------------------------------------------------
# Running Prokka

# For Loop
  for sample in $SAMPLE_NAME
  do

	# (a) DIR name
	# Contains only the SAMPLE name with no PATH
	  DIRNAME=${sample##*/}

  # (b) INPUT file
  # Contigs - Assembly file (Filtered & Ordered)
  	INP_contigs=../results/Contigs_Ordered/$DIRNAME\_contigs_ordered.fasta

  # (c) Define Sequencing centre
    Seq_centre=UMICH

  # (d) Run SOFTWARE
  # SOFTWARE: Prokka
  # USAGE:
  # --outdir:      Output DIR
  # --force:       Force overwriting existing output folder
  # --prefix:      Filename output prefix
  # --addgenes:    Add 'gene' feature for each 'CDS' feature
  # --centre:      Sequence centre ID
  # --compliant:   Force Genbank/ENA/DDBJ compliance
  # --strain:      SAMPLE name
  # --plasmid:     Plasmid name or identifier
  # --kingdom:     Annotation mode: Archaea|Bacteria|Viruses
  # --proteins:    If REFERENCE is available - ensure gene naming is consistent
  # --rfam:        Enable searching for ncRNAs with Infernal+Rfam (SLOW!)
  # --cpus:        Number of CPUs to use
    prokka --outdir $OUT_DIR_work \
           --force \
           --prefix $DIRNAME \
           --addgenes \
           --centre $Seq_centre \
           --compliant \
           --strain $DIRNAME \
           --plasmid p$DIRNAME \
           --kingdom Bacteria \
           --proteins $REF_PROKKA \
           --rfam \
           --cpus $PROCESSORS \
           $INP_contigs

  # (e) Move OUTPUT files to final DIR [ Prokka ]
  # Master annotation in GFF3 format, containing both sequences and annotations
    mv $OUT_DIR_work/$DIRNAME.gff \
       $OUT_DIR
  # Standard Genbank file derived from the master .gff
    mv $OUT_DIR_work/$DIRNAME.gbk \
       $OUT_DIR/GBK
  # Protein FASTA file of the translated CDS sequences
    mv $OUT_DIR_work/$DIRNAME.faa \
       $OUT_DIR/ProteinFASTA
  # Statistics relating to the annotated features found
    mv $OUT_DIR_work/$DIRNAME.txt \
       $OUT_DIR/Statistics

  # (f) Remove accesory files generated
    rm -r $OUT_DIR_work/*

  # (g) COMPLETION message
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ Annotating genome with Prokka complete ] "
    echo " [ Contigs PATH: ] [ $INP_contigs ] "
    echo " [ SAMPLE name: $DIRNAME ] "
    echo " "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "

  done

# (05.6)
# -----------------------------------------------------------------------------
# Remove accesory files generated

  # (a) REFERENCE FNA
    rm $REF_PROKKA
  # (b) REFERENCE list VARIABLE (TXT file)
    rm $List
  # (c) Working DIR [ Prokka_working ]
    rm -r $OUT_DIR_work


###############################################################################
# (06) Return to base environment
###############################################################################

# (06.1)
# -----------------------------------------------------------------------------
# Return to the conda base ENVIRONMENT

  # Prokka is installed in base ENVIRONMENT
    conda activate


###############################################################################
# END:
# SCRIPT: 19_Assembly_Annotation.sh
###############################################################################
