#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       06_TaxonomyClassification_Kraken.sh
# Written by:   Cieza RJ
# PURPOSE:			Taxonomic sequence classification (Kraken2)
# ---------------------------------------------------------------------------

# Kraken2 (v 2.0.7-beta)
	# Taxonomic sequence classifier
	# https://ccb.jhu.edu/software/kraken2/index.shtml?t=manual

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  |   └── Kraken2_DB
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       |   ├── Taxonomy
#  |       |   └── ("SAMPLE_name")
#  |       |   			└── Kraken					-- DIR removed upon completion --
#  |       └── src
#  └── reference


#############################################################################
# (01) Set up ENVIRONMENT
#############################################################################

# (01.1)
# Activate Python 3 ENVIRONMENT
  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate mypython3


#############################################################################
# (02) Verify SOFTWARE
#############################################################################

# (02.1)
# Section message
	echo " "
	echo " ###############################################################"
	echo " [ SOFTWARE verification] "
	echo " ###############################################################"
	echo " "

# (02.2)
# Kraken2 should have been installed through Conda (view README)
# Check kraken2 version
	echo "kraken2 version is:"
	kraken2 --version


#############################################################################
# (03) Kraken2 database
#############################################################################

# (03.1)
# Kraken2 database PATH
	DB=../../../bin/Kraken2_DB/


################################################################################
# (04) Run Kraken2
###############################################################################

# (04.1)
# Section message
# ---------------------
  echo " "
  echo " ###############################################################"
  echo " [ Running Kraken2 to assign taxonomic labels ] "
  echo " ###############################################################"
  echo " "

# (04.2)
# Generate a list of the samples present in DIR "data_PROCESSED"
# ---------------------
# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of unique names in the DIR (Keep only the sample name)
# Each sample has:
  	# Forward (*_R1.fastq.gz)
  	# Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
		  					sed 's/\_R[1-2].fastq.gz//' | \
		  					sort -u)

# (04.3)
# For Loop
# ---------------------
	for sample in $SAMPLE_NAME
	do

	# (a) Define DIRNAME
	# ---------------------
	# Contains only the SAMPLE name with no PATH
		DIRNAME=${sample##*/}

	# (b) Define INPUT
	# ---------------------
  # Use FASTQ reads after processing them with cutadapt
	# Forward FASTQ read (R1)
		R1=$sample\_R1.fastq.gz
	# Reverse FASTQ read (R2)
		R2=$sample\_R2.fastq.gz

	# (c) Define the OUTPUT DIR and files for the loop
	# ---------------------
	# Define DIR PATH to deposit results per sample
		OUT_sample=../results/$DIRNAME/Kraken/
	# Make DIR
  	mkdir -p $OUT_sample
	# OUTPUT reports
		OUT_Class=../results/$DIRNAME/Kraken/Kraken_classified_$DIRNAME
		OUT_Report=../results/$DIRNAME/Kraken/Kraken_report_$DIRNAME

  # (d) Run software:
	# SOFTWARE: Kraken2
	# -------------------------------------------------------------------------
  # USAGE:
  # --db:									Name for Kraken DB
	# --threads:
	# --use-names:					Print scientific names instead of just taxids
	# --gzip-compressed:		Input files are compressed
	# --paired:							The filenames provided have paired-end reads
	# --classified-out:			Print classified sequences to filename
	# --report:							Print a report with aggregrate counts/clade to file
  # -------------------------------------------------------------------------
	  kraken2 --db $DB \
						--threads 8 \
						--use-names \
						--gzip-compressed \
						--paired $R1 $R2 \
						--report $OUT_Report

	# (e) Loop completion message
	# ---------------------
    echo " "
    echo " [ Finished TAXONOMIC classification ] [ $DIRNAME ] "
    echo " "
    echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
    echo " "

	done


#############################################################################
# (05) Extracting DOMAIN, FAMILY, GENUS, SPECIES and UNCLASSIFIED
#			 information from Kraken report
#############################################################################

# (05.1)
# Section message
# ---------------------
  echo " "
  echo " ###############################################################"
  echo " [ Extracting TAXONOMIC information from Kraken2 report ] "
  echo " ###############################################################"
  echo " "

# (05.2)
# For Loop
# ---------------------
	for sample in $SAMPLE_NAME
	do

	# (a) Define DIRNAME
	# ---------------------
	# Contains only the SAMPLE name with no PATH
		DIRNAME=${sample##*/}

  # (b) Define INPUT (Kraken report generated per SAMPLE)
	# ---------------------
  	TOTAL_report=../results/$DIRNAME/Kraken/Kraken_report_$DIRNAME

	# (c) Define OUTPUT (Taxonomy report per sample)
	# ---------------------
	# TAXONOMIC reports
		UN_report=unclass_tmp
		DO_report=class_tmp
		FAM_report=family_tmp
		GN_report=genus_tmp
		SP_report=species_tmp
	# TAXONOMIC reports including SAMPLE name
		UN_report_name=unclass
		DO_report_name=class
		FAM_report_name=family
		GN_report_name=genus
		SP_report_name=species

	# (d) Extract TAXONOMIC information from REPORT
	# DOMAIN, FAMILY, GENUS, SPECIES and UNCLASSIFIED
	# ---------------------
		cat $TOTAL_report | head -1 | tail -1| cut -f 1,6 > $UN_report
		cat $TOTAL_report | head -4 | tail -1| cut -f 1,6 > $DO_report
		cat $TOTAL_report | head -8 | tail -1| cut -f 1,6 > $FAM_report
		cat $TOTAL_report | head -9 | tail -1| cut -f 1,6 > $GN_report
		cat $TOTAL_report | head -10 | tail -1| cut -f 1,6 > $SP_report

	# (e) Add SAMPLE name to the TAXONOMY report into the first COLUMN
	# ---------------------
		awk -v var="$DIRNAME" '{FS = "\t"} ; {print var FS $0}' $UN_report >> $UN_report_name
		awk -v var="$DIRNAME" '{FS = "\t"} ; {print var FS $0}' $DO_report >> $DO_report_name
		awk -v var="$DIRNAME" '{FS = "\t"} ; {print var FS $0}' $FAM_report >> $FAM_report_name
		awk -v var="$DIRNAME" '{FS = "\t"} ; {print var FS $0}' $GN_report >> $GN_report_name
		awk -v var="$DIRNAME" '{FS = "\t"} ; {print var FS $0}' $SP_report >> $SP_report_name

	# (f) Loop completion message
	# ---------------------
    echo " "
    echo " [ Finished extracting TAXONOMIC information from report ] [ $DIRNAME ] "
    echo " "
    echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
    echo " "

	done


#############################################################################
# (06) Generate final CSV report
#############################################################################

# (06.1)
# Section message
# ---------------------
  echo " "
  echo " ###############################################################"
  echo " [ Generating TAXONOMY CSV report ] "
  echo " ###############################################################"
  echo " "

# (06.2)
# Define OUTPUT
# ---------------------
# (a) TAXONOMIC reports with COLUMN headers
	TMP1_report=../results/Taxonomy/Out1.tsv
	TMP2_report=../results/Taxonomy/Out2.tsv
	TMP3_report=../results/Taxonomy/Out3.tsv
	TMP4_report=../results/Taxonomy/Out4.tsv
	TMP5_report=../results/Taxonomy/Out5.tsv

# (b) OUTPUT PATH
  OUT_Taxo=../results/Taxonomy/

# (c) Make DIR
  mkdir -p $OUT_Taxo

# (d) Final Report TSV and CSV versions\
# TSV
	UN_report_TSV=$OUT_Taxo\Taxonomy_unclass.tsv
  DO_report_TSV=$OUT_Taxo\Taxonomy_domain.tsv
  FAM_report_TSV=$OUT_Taxo\Taxonomy_family.tsv
  GN_report_TSV=$OUT_Taxo\Taxonomy_genus.tsv
  SP_report_TSV=$OUT_Taxo\Taxonomy_species.tsv
# CSV
	UN_report_CSV=$OUT_Taxo\Taxonomy_unclass.csv
	DO_report_CSV=$OUT_Taxo\Taxonomy_domain.csv
	FAM_report_CSV=$OUT_Taxo\Taxonomy_family.csv
	GN_report_CSV=$OUT_Taxo\Taxonomy_genus.csv
	SP_report_CSV=$OUT_Taxo\Taxonomy_species.csv

# (e) Combined report with all TAXONOMIC information of all samples
  TAXO_report=../results/Taxonomy/Out6.csv
  TAXO_report_final=$OUT_Taxo\Taxonomy_report.csv

# (06.3)
# Prints the respective COLUMN headers to all taxonomic REPORTS
# ---------------------
	awk 'BEGIN {print "SAMPLE\tPercentReads\tDomain"}; {print}' \
      $DO_report_name > $TMP1_report
	awk 'BEGIN {print "SAMPLE\tPercentReads\tFamily"}; {print}' \
      $FAM_report_name > $TMP2_report
	awk 'BEGIN {print "SAMPLE\tPercentReads\tGenus"}; {print}' \
      $GN_report_name > $TMP3_report
	awk 'BEGIN {print "SAMPLE\tPercentReads\tSpecies"}; {print}' \
      $SP_report_name > $TMP4_report
	awk 'BEGIN {print "SAMPLE\tPercentReads\tSpecies"}; {print}' \
      $UN_report_name > $TMP5_report

# (06.4)
# Format file to TAB delimited & create a CSV report
# ---------------------
# Domain
	cat $TMP1_report > $DO_report_TSV
	cat $DO_report_TSV | tr "\t" "," > $DO_report_CSV
# Family
	cat $TMP2_report > $FAM_report_TSV
	cat $FAM_report_TSV | tr "\t" "," > $FAM_report_CSV
# Genus
	cat $TMP3_report > $GN_report_TSV
	cat $GN_report_TSV | tr "\t" "," > $GN_report_CSV
# Species
	cat $TMP4_report > $SP_report_TSV
	cat $SP_report_TSV | tr "\t" "," > $SP_report_CSV
# Unclassified
	cat $TMP5_report > $UN_report_TSV
	cat $UN_report_TSV | tr "\t" "," > $UN_report_CSV

# (06.5)
# Merge results into a single CSV file
# ---------------------
# (a) Merge COLUMNS of interest from all CSV reports
  :| paste -d ',' $DO_report_CSV \
                  $FAM_report_CSV \
                  $GN_report_CSV \
                  $SP_report_CSV \
                  $UN_report_CSV \
                  > \
                  $TAXO_report
# (b) Extract COLUMNS of interest
  cut -d ',' \
      -f 4,7,10,13 \
      --complement \
      $TAXO_report\
      > \
      $TAXO_report_final

# (06.6)
# Clean temporary files
# ---------------------
# TAXONOMIC reports with COLUMN headers
	rm $TMP1_report \
     $TMP2_report \
     $TMP3_report \
     $TMP4_report \
     $TMP5_report
# Remove TAXONOMIC reports
	rm $DO_report \
		 $FAM_report \
		 $GN_report \
		 $SP_report \
		 $UN_report
# Remove TAXONOMIC reports including SAMPLE name
	rm $DO_report_name \
		 $FAM_report_name \
		 $GN_report_name \
		 $SP_report_name \
		 $UN_report_name
# Final Report TSV version
  rm $DO_report_TSV \
     $FAM_report_TSV \
     $GN_report_TSV \
     $SP_report_TSV \
     $UN_report_TSV
 # Final Report CSV version
   rm $DO_report_CSV \
      $FAM_report_CSV \
      $GN_report_CSV \
      $SP_report_CSV \
      $UN_report_CSV \
      $TAXO_report
# DIR Kraken contains individual Kraken2 reports
# DIR Kraken is not needed anymore after compiling results into a single file
# For Loop
# ---------------------
  for sample in $SAMPLE_NAME
  do

	# (a) Define DIRNAME
	# ---------------------
	# Contains only the SAMPLE name with no PATH
		DIRNAME=${sample##*/}

  # (b) Remove DIR Kraken
  # ---------------------
    rm -R ../results/$DIRNAME/Kraken/

  # (c) Loop completion message
	# ---------------------
    echo " "
    echo " [ Delete complete - DIR Kraken ] [ $DIRNAME ] "
    echo " "
    echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
    echo " "

  done


#############################################################################
# (07) Return to base environment
#############################################################################

# (07.1)
# Return to the base environment
	conda activate


#############################################################################
# END:
# SCRIPT: 06_TaxonomyClassification_Kraken.sh
#############################################################################
