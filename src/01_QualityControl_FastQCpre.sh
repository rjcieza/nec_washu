#!/bin/bash

# -----------------------------------------------------------------------------
# SCRIPT:         01_QualityControl_FastQCpre.sh
# Written by:     Cieza RJ
# PURPOSE:        Quality control (QC) with FastQC before Trimmomatic
# -----------------------------------------------------------------------------

# FastQC (v 0.11.8)
  # QC checks on raw sequence data from high throughput sequencing pipelines
  # https://www.bioinformatics.babraham.ac.uk/projects/fastqc/

# DIR tree
  # Script DIR structure
  # Script should be run from the DIR src within a PROJECT
  # DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  └── projects
#      └── ("PROJECT_name")
#          ├── data             -- INPUT --
#          ├── results
#          |   └── QC_Pre       -- OUTPUT --
#          ├── reference
#          └── src

###############################################################################
###############################################################################

###############################################################################
# (00) Supply ARGUMENTS to SCRIPT from the command line
###############################################################################

# (00.1)
# -----------------------------------------------------------------------------
# Determine the number of processors used in the SCRIPT.

  # (a) Default VALUE
  # If no value is provided, 4 processors will be used
    PROCESSORS=4

# (00.2)
# -----------------------------------------------------------------------------
# User-supplied values (will overwrite default)

# ARGUMENTS will provided through the command Line
# [-p]
  while [[ "$1" == -* ]];
  do
    case $1 in
      # (a) PROCESSORS option
      # - - - - - - - - - - - - - - - -
      # The argument to the OPTION is placed in the variable $1
      -p|--processsors)
        shift
        PROCESSORS="$1"
        ;;
      # (b) END of arguments
      # - - - - - - - - - - - - - - - -
      --)
        shift
        break
        ;;
      # FINISHING the loop
      esac
      shift
    done

# (00.3)
# -----------------------------------------------------------------------------
# Log message
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "
  echo " [ PROCESSORS used ] [ - $PROCESSORS - ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# Activate Python 3 ENVIRONMENT

  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh

  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate mypython3


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ SOFTWARE verification] "
  echo " ############################################################### "
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE

  # (a) FastQC should have been installed through Conda (view README)
  # Check FastQC version
  fastqc --version

  # (b) Exit if SOFTWARE is not detected
  if [ $? -eq 0 ]
  then
    echo " "
    echo " --- FastQC is INSTALLED --- "
    echo " "
  else
    echo " "
    echo " --- FastQC is NOT DETECTED --- "
    echo " --- EXIT script --- "
    echo " "
    exit 1
  fi


#############################################################################
# (03) Run FastQC & create a DIR [ QC_Pre ] to deposit results
#############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ Running FastQC ] "
  echo " ############################################################### "
  echo " "

# (03.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR [ data ]

  # DIR [ data ] contains FASTQ paired end (PE) reads
  # Make a list of unique names in the DIR
  # Each SAMPLE has:    Forward (*_R1.fastq.gz)
  #                     Reverse (*_R2.fastq.gz)
  # find:   Find SAMPLES in DIR [ data]
  # -name:  List unique SAMPLE names
  # sed:    Remove the ending of file names only keeping the unique name
  # sort:   Sort by unique name
    SAMPLE_NAME=$(find ../data/ -name '*.fastq.gz' | \
          				sed 's/\_R[1-2].fastq.gz//' | \
          				sort -u)

# (03.3)
# -----------------------------------------------------------------------------
# Find FASTQ files and run FastQC

# For Loop
  for sample in $SAMPLE_NAME
  do

  # (a) DIR name
  # Contains only the SAMPLE name with no PATH
    DIRNAME=${sample##*/}

  # (b) INPUT
  # Use FASTQ reads
  # Forward FASTQ read (R1)
  # Reverse FASTQ read (R2)
    R1=$sample\_R1.fastq.gz
    R2=$sample\_R2.fastq.gz

  # (c) OUTPUT
  # PATH & Make DIR
    OUTDIR_pre=../results/QC_Pre
    mkdir -p $OUTDIR_pre

  # (d) Run SOFTWARE
  # SOFTWARE: FastQC
  # USAGE:
  # -o:            Create all output files in the specified output directory
  # --threads:     Each thread will be allocated 250MB of memory
  # Forward FASTQ read (R1)
    fastqc --threads $PROCESSORS \
           -o $OUTDIR_pre \
           $R1
  # Reverse FASTQ read (R2)
    fastqc --threads $PROCESSORS \
           -o $OUTDIR_pre \
           $R2

  # (e) COMPLETION message
    echo " "
    echo " [ FastQC analysis ] "
    echo " [ SAMPLE name: $DIRNAME ] [ COMPLETE ] "
    echo " "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "

  done


###############################################################################
# (04) Return to base ENVIRONMENT
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# Return to conda base ENVIRONMENT
  conda activate


#############################################################################
# END
# SCRIPT: 01_QualityControl_FastQCpre.sh
#############################################################################
