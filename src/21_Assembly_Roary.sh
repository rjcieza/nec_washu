#!/bin/bash

# -----------------------------------------------------------------------------
# SCRIPT:       21_Assembly_Roary.sh
# Written by:   Cieza RJ
# PURPOSE:      Create a pan-genome with Roary
# -----------------------------------------------------------------------------

# Roary (v 3.12.0)
  # Calculates the pan-genomeakes from annotated assemblies in GFF format
  # Pan-genome: Full complement of genes in a set of genomes
  # https://sanger-pathogens.github.io/Roary/

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |			 |	 ├── Prokka			          -- INPUT --
#  |			 |	 ├── Roary_working  			-- DIR removed upon completion --
#  |       |   └── Roary						    -- OUTPUT --
#  |       └── src
#  |			  	 └── OUT_DIR        			-- DIR removed upon completion --
#  └── reference


###############################################################################
# (00) Supply ARGUMENTS to SCRIPT from the command line
###############################################################################

# (00.1)
# -----------------------------------------------------------------------------
# Determine the number of processors used in the SCRIPT.

  # (a) Default VALUE
  # If no value is provided, 4 processors will be used
    PROCESSORS=4

# (00.2)
# -----------------------------------------------------------------------------
# User-supplied values (will overwrite default)

# ARGUMENTS will provided through the command Line
# [-p]
  while [[ "$1" == -* ]];
  do
    case $1 in
      # (a) PROCESSORS option
      # - - - - - - - - - - - - - - - -
      # The argument to the OPTION is placed in the variable $1
      -p|--processsors)
        shift
        PROCESSORS="$1"
        ;;
      # (b) END of arguments
      # - - - - - - - - - - - - - - - -
      --)
        shift
        break
        ;;
      # FINISHING the loop
      esac
      shift
    done

# (00.3)
# -----------------------------------------------------------------------------
# Log message

  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "
  echo " [ PROCESSORS used ] [ - $PROCESSORS - ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# Activate base ENVIRONMENT

  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ############################################################### "
  echo " [ SOFTWARE verification] "
  echo " ############################################################### "
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE

  # (a) Check Roary version
    roary --version

  # (b) Check all Roary dependencies are available
    roary -a


###############################################################################
# (03) Transfer GFF files to working DIR
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ############################################################### "
  echo " [ Copying GFF files to DIR [ Roary_working ] ] "
  echo " ############################################################### "
  echo " "

# (03.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR [ data_PROCESSED ]

	# DIR [ data_PROCESSED ] contains FASTQ paired end (PE) reads
	# Make a list of unique names in the DIR
	# Each SAMPLE has:
	  	# Forward (*_R1.fastq.gz)
	  	# Reverse (*_R2.fastq.gz)
	# find:   Find SAMPLES in DIR [ data_PROCESSED ]
	# -name:  List unique SAMPLE names
	# sed:    Remove the ending of file names only keeping the unique name
	# sort:   Sort by unique name
		SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
			  					sed 's/\_R[1-2].fastq.gz//' | \
			  					sort -u)

# (03.3)
# -----------------------------------------------------------------------------
# OUTPUT

  # (a) Working DIR to deposit INPUT GFF files
  # PATH & Make DIR
    INP_DIR_work=../results/Roary_working
    mkdir -p $INP_DIR_work

# (03.4)
# -----------------------------------------------------------------------------
# Find GFF files and move to DIR [ Roary_working ]

	# For Loop
		for sample in $SAMPLE_NAME
		do

		# (a) Define DIRNAME
		# Contains only the SAMPLE name with no PATH
			DIRNAME=${sample##*/}

	  # (b) INPUT
	  	INP_GFF=../results/Prokka/$DIRNAME.gff

    # (c) Copy INPUT (GFF) into DIR [ Roary_working ]
		  cp $INP_GFF \
		  	 $INP_DIR_work

    # (d) COMPLETION message
			echo " [ Copying GFF ] [ $DIRNAME ] [ COMPLETE ] "
			echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
			echo " "

    done

# (03.5)
# -----------------------------------------------------------------------------
# List GFF files present in DIR [ Roary_working ] to be used by Roary

  # (a) GFF files to be used with Roary
    echo " List of GFF files to construct pan-genome "
    echo " = = = = = = = = = = = = = = = = = = = = = = "
    ls -l $INP_DIR_work
    echo " END of list "

  # (b) Save into a TXT file
    ls -l $INP_DIR_work > Pangenome_SAMPLE_List.txt


###############################################################################
# (04) Run Roary on DIR [ Roary_working ]
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# USER message

	echo " "
	echo " ############################################################### "
	echo " [ Running Roary on DIR [ Roary_working ] ] "
	echo " ############################################################### "
	echo " "

# (04.2)
# -----------------------------------------------------------------------------
# INPUT

	# (a) List INPUT GFF files
	# DIR was created in step 03
		INP_List=../results/Roary_working/*.gff
    echo $INP_List

# (04.3)
# -----------------------------------------------------------------------------
# OUTPUT

	# (a) PATH & Make DIR
		OUT_Roary=../results/Roary
		mkdir -p $OUT_Roary

# (04.4)
# -----------------------------------------------------------------------------
# Run Roary

  # Run SOFTWARE
  # SOFTWARE: Roary
  # USAGE:
  # -p:     Number of threads
  # -f:     Output DIR
  # -i:     Minimum percentage identity for blastp [Default = 95]
  # -g:     Maximum number of clusters [Default = 50000]
  # -e:     Create a multiFASTA alignment of core genes using PRANK
  # -r:     Create R plots, requires R and ggplot2
  # -v:     Verbose output to STDOUT
    roary -p $PROCESSORS \
          -f OUT_DIR \
          -i 85 \
          -g 60000 \
          -e \
          -r \
          -v \
          $INP_List

# (04.5)
# -----------------------------------------------------------------------------
# COMPLETION message

	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	echo " [ Running Roary ] [ DIR = $INP_DIR_work ] [ COMPLETE ] "
	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	echo " "

# (04.6)
# -----------------------------------------------------------------------------
# Remove accesory files generated

  # (a) Gene Presence Absence CSV file
    mv OUT_DIR/gene_presence_absence.csv \
       $OUT_Roary
  # (b) Gene Presence Absence R tab delimited file
    mv OUT_DIR/gene_presence_absence.Rtab \
       $OUT_Roary
  # (c) Gene alignment across SAMPLES
    mv OUT_DIR/core_gene_alignment.aln \
       $OUT_Roary
  # (d) R plots - Pangenome build
    mv OUT_DIR/Rplots.pdf \
      $OUT_Roary
  # (e) Core genome summary statistics
    mv OUT_DIR/summary_statistics.txt \
       $OUT_Roary

# (04.7)
# -----------------------------------------------------------------------------
# Remove accesory files generated

  # (a) Working DIR [ Roary_working ]
    rm -r $INP_DIR_work
  # (b) Resuts DIR
    rm -r OUT_DIR
  # (c) Move SAMPLE list to results DIR [ Roary ]
    mv Pangenome_SAMPLE_List.txt \
       $OUT_Roary


###############################################################################
# (05) Return to base environment
###############################################################################

# (05.1)
# -----------------------------------------------------------------------------
# Return to the conda base ENVIRONMENT

  # Prokka is installed in base ENVIRONMENT
    conda activate

###############################################################################
# END:
# SCRIPT: 21_Assembly_Roary.sh
###############################################################################
