#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       05_QualityControl_MultiQCpost
# Written by:   Cieza RJ
# PURPOSE:      Summary statistics from FastQC (Post-trimming)
# ---------------------------------------------------------------------------

# MultiQC (v 1.8)
	# Reporting tool that parses summary statistics of various tools
	# http://multiqc.info/docs/#using-multiqc

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       |   ├── FastQC								-- DIR removed upon completion --
#  |       |   ├── MultiQC
#  |       |   └── ("SAMPLE_name")
#  |       |        └── QC							-- DIR removed upon completion --
#  |       |            └── Post				-- DIR removed upon completion --
#  |       └── src
#  └── reference


#############################################################################
# (01) Set up ENVIRONMENT
#############################################################################

# (01.1)
# Activate Python 3 ENVIRONMENT
  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate env_multiqc


#############################################################################
# (02) Verify SOFTWARE
#############################################################################

# (02.1)
# Section message
	echo " "
	echo " ###############################################################"
	echo " [ SOFTWARE verification] "
	echo " ###############################################################"
	echo " "

# (02.2)
# MultiQC should have been installed through Conda (view README)
# Check MultiQC version
	multiqc --version


#############################################################################
# (03) Transfer FastQC reports (ZIP) to DIR [FastQC]
#############################################################################

# (03.1)
# Section message
	echo " "
	echo " ###############################################################"
	echo " [ Copying FastQC reports ] "
	echo " ###############################################################"
	echo " "

# (03.2)
# Generate a list of the samples present in DIR "data_PROCESSED"
# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of unique names in the DIR (Keep only the sample name)
# Each sample has:
  	# Forward (*_R1.fastq.gz)
  	# Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
		  					sed 's/\_R[1-2].fastq.gz//' | \
		  					sort -u)

# (03.3)
# Define OUTPUT
# ---------------------
# (a) OUTPUT PATH
	OUT_PostQC=../results/FastQC/
# (b) Make DIR
	mkdir -p $OUT_PostQC

# (03.4)
# For Loop
	for sample in $SAMPLE_NAME
	do

	# (a) Define DIRNAME
	# ---------------------
  # Contains only the SAMPLE name with no PATH
  	DIRNAME=${sample##*/}

  # (b) Define INPUT
	# ---------------------
  	INP_PostQC=$(find ../results/$DIRNAME/QC/Post/ -name '*.zip')

	# (c) Determine if FastQC reports are present
	# ---------------------
		if [ -z "$INP_PostQC" ];
		then
    	echo "ERROR: No FastQC report found for $DIRNAME"
		else
    	echo "[FastQC report found] $DIRNAME"
		fi

	# (d) Copy reports to DIR [FastQC]
	# ---------------------
		cp $INP_PostQC \
			 $OUT_PostQC

	# (e) Loop Completion message
	# ---------------------
		echo " "
		echo " [ Copying FastQC report ] [ $DIRNAME ] [ COMPLETE ] "
		echo " "
		echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
		echo " "

	done


#############################################################################
# (04) Run MultiQC on DIR [FastQC]
#############################################################################

# (04.1)
# Section message
# ---------------------
	echo " "
	echo " ###############################################################"
	echo " [ Running MultiQC ] "
	echo " ###############################################################"
	echo " "

# (04.2)
# Define INPUT DIR - DIR was created in step 03
# ---------------------
	INP_MultiQC=../results/FastQC/

# (04.3)
# Define OUTPUT
# ---------------------
# (a) Define OUTPUT PATH
	OUT_MultiQC=../results/MultiQC/
# (b) Make DIR
	mkdir -p $OUT_MultiQC
# (c) MultiQC OUTPUT report
	OUT_Report_name=MultiQC_FastQC_Post

# (04.4)
# Run software:
# SOFTWARE:	MultiQC
# -------------------------------------------------------------------------
# USAGE:
	# --module:     Use only this module
	# --outdir:     Create report in the specified output directory
	# --filename:   Report filename
# -------------------------------------------------------------------------
	multiqc --module fastqc \
  				--outdir $OUT_MultiQC \
  				--filename $OUT_Report_name \
  				$INP_MultiQC

# (04.5)
# Section END message
# ---------------------
	echo " "
	echo " [ Running MultiQC ] [ DIR = $INP_MultiQC ] [ COMPLETE ] "
	echo " "
	echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
	echo " "


#############################################################################
# (05) Cleaning work ENVIRONMENT
#############################################################################

# (05.1)
# Section message
# ---------------------
	echo " "
	echo " ###############################################################"
	echo " [ Cleaning Temporary files ] "
	echo " ###############################################################"
	echo " "

# (05.2)
# Remove DIR [FastQC] if MultiQC is successful
# ---------------------
	if [ -d $OUT_MultiQC$OUT_Report_name\_data ];
	then

	# (a) Found MultiqQC report message
	# ---------------------
		echo " "
		echo "MultiQC report present"
		echo " "

	# (b) For Loop
	# ---------------------
		for sample in $SAMPLE_NAME
    do

		# Define DIRNAME
		# Contains only the SAMPLE name with no PATH
			DIRNAME=${sample##*/}

		# Remove DIR [QC] and [Post] for sample
	   	rm -R ../results/$DIRNAME/QC/Post/
			rm -R ../results/$DIRNAME/QC/
      rm -R ../results/$DIRNAME/

	  done

	# (C) Remove DIR [FastQC]
	# ---------------------
  	rm -R $INP_MultiQC

	# (d) Remove accesory files created when running MultiQC
	# ---------------------
  	rm -R $OUT_MultiQC$OUT_Report_name\_data

	fi


#############################################################################
# (06) Return to base environment
#############################################################################

# (06.1)
# Return to the base environment
	conda activate


#############################################################################
# END:
# SCRIPT: 05_QualityControl_MultiQCpost
#############################################################################
