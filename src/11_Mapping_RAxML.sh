#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       11_Mapping_RAxML.sh
# Written by:   Cieza RJ
# PURPOSE:      Phylogeny inference with RAxML-NG
# ---------------------------------------------------------------------------

# RAxML-NG (v 0.5.0 BETA):
  # Phylogenetic tree inference tool
  # Uses maximum-likelihood (ML) optimality criterion
  # https://github.com/amkozlov/raxml-ng

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       |   ├── Phylogenetics
#  |       |   └── SPANDx
#  |       |       └── Outputs
#  |       |           └── Comparative
#  |       └── src
#  └── reference


###############################################################################
# (00) Supply ARGUMENTS to SCRIPT from the command line
###############################################################################

# (00.1)
# -----------------------------------------------------------------------------
# Determine the number of processors used in the SCRIPT.

  # (a) Default VALUE
  # If no value is provided, 4 processors will be used
    PROCESSORS=4

# (00.2)
# -----------------------------------------------------------------------------
# User-supplied values (will overwrite default)

# ARGUMENTS will provided through the command Line
# [-p]
  while [[ "$1" == -* ]];
  do
    case $1 in
      # (a) PROCESSORS option
      # - - - - - - - - - - - - - - - -
      # The argument to the OPTION is placed in the variable $1
      -p|--processsors)
        shift
        PROCESSORS="$1"
        ;;
      # (b) END of arguments
      # - - - - - - - - - - - - - - - -
      --)
        shift
        break
        ;;
      # FINISHING the loop
      esac
      shift
    done

# (00.3)
# -----------------------------------------------------------------------------
# Log message

  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "
  echo " [ PROCESSORS used ] [ - $PROCESSORS - ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# Activate Python 3 ENVIRONMENT

  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate mypython3


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ SOFTWARE verification] "
  echo " ###############################################################"
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE version

# RAxML-ng should have been installed through Conda (view README)
# Check RAxML-ng version
  raxml-ng --version | head -n 2


###############################################################################
# (04) Run RAxML-NG
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ Running RAxML-NG to generate phylogenetic tree ] "
  echo " ###############################################################"
  echo " "

# (04.2)
# -----------------------------------------------------------------------------
# Define INPUT

  # (a) INPUT multi-sequence alignment file PATH
    INPUT_MSA=../results/SPANDx/Outputs/Comparative/Ortho_SNP_matrix_RAxML.nex
  # (b) Define PREFIX (used with flag --prefix)
    Prefix=MLtree
  # (c) Specify the OUTGROUP Reference
  # This parameter should be modified according to set of samples
    OutG=EalbertiiKF1

# (04.3)
# -----------------------------------------------------------------------------
# Define OUTPUT

  # (a) OUTPUT PATH
    OUT_RAxML=../results/Phylogenetics
  # (b) Make DIR
    mkdir -p $OUT_RAxML

# (04.4)
# -----------------------------------------------------------------------------
# Run SOFTWARE

# SOFTWARE: RAxML-NG
# USAGE:
# --all:              Combined tree search and boostrapping analysis
#                     [Number of trees = 10 ramdom + 10 parsimony]
# --msa:							Multiple Sequence Alignment file
# --model:            Substitution matrix model + Rate heterogenity
#                     + Empirical base frequencies
# --prefix:           Prefix for the OUTPUT files
# --threads:          Number of processors to use
# --bs-trees:         The --all command by default infers 100 trees
  raxml-ng --all \
           --msa $INPUT_MSA \
           --model GTR+G+FC \
           --prefix $Prefix \
           --threads $PROCESSORS \
           --outgroup $OutG \
           --bs-trees 200

 # (04.5)
 # ----------------------------------------------------------------------------
 # Move results to DIR [ Phylogenetics ]

  # (a) Move files to output DIR Phylogenetics:
  # Best Tree
    mv $Prefix.raxml.bestTree \
       $OUT_RAxML
  # (b) Best Tree + bootstrap values in the tree
    mv $Prefix.raxml.support \
       $OUT_RAxML
  # (c) Best Model (Optimitized parameters for the best scoring tree)
    mv $Prefix.raxml.bestModel \
       $OUT_RAxML
  # (d) Starting tree used for ML inference
    mv $Prefix.raxml.startTree \
       $OUT_RAxML
  # (e) Compressed multi-sequence alignment (MSA) in binary format
    mv $Prefix.raxml.rba \
       $OUT_RAxML
  # (f) Trees inferred for each bootstrap replicate
     mv $Prefix.raxml.bootstraps \
        $OUT_RAxML

# (04.6)
# -----------------------------------------------------------------------------
# Remove accesory files generated

  # (a) Screen log
    rm $Prefix.raxml.log
  # (b) ML trees for each starting tree
    rm $Prefix.raxml.mlTrees


###############################################################################
# END:
# SCRIPT: 11_Mapping_RAxML.sh
###############################################################################
