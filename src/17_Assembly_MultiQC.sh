#!/bin/bash

# -----------------------------------------------------------------------------
# SCRIPT:       17_Assembly_MultiQC.sh
# Written by:   Cieza RJ
# PURPOSE:      Summary statistics from Assembly obtained with QUAST
# -----------------------------------------------------------------------------

# MultiQC (v 1.8)
	# Reporting tool that parses summary statistics of various tools
	# http://multiqc.info/docs/#using-multiqc

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |			 |	 ├── Reports_MultiQC			-- DIR removed upon completion --
#  |       |   ├── QUAST_reports				-- INPUT --
#  |       |   └── MultiQC							-- OUTPUT --
#  |       └── src
#  └── reference


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# Activate Python 3 ENVIRONMENT

  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate env_multiqc


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message

	echo " "
	echo " ###############################################################"
	echo " [ SOFTWARE verification] "
	echo " ###############################################################"
	echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE

  # MultiQC should have been installed through Conda (view README)
  # Check MultiQC version
		multiqc --version


###############################################################################
# (03) Transfer QUAST reports to DIR [ Reports_MultiQC ]
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message

	echo " "
	echo " ############################################################### "
	echo " [ Copying QUAST reports to DIR [ Reports_MultiQC ] ] "
	echo " ############################################################### "
	echo " "

# (03.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR "data_PROCESSED"

	# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
	# Make a list of unique names in the DIR (Keep only the sample name)
	# Each sample has:
	  	# Forward (*_R1.fastq.gz)
	  	# Reverse (*_R2.fastq.gz)
	# find:   Find samples in DIR "data_PROCESSED"
	# -name:  List unique names
	# sed:    Remove the ending of file names only keeping the unique name
	# sort:   Sort by unique name
		SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
			  					sed 's/\_R[1-2].fastq.gz//' | \
			  					sort -u)

# (03.3)
# -----------------------------------------------------------------------------
# Make DIR [ Reports_MultiQC ]

	# (a) PATH & Make DIR
		DIR_Reports=../results/Reports_MultiQC
		mkdir -p $DIR_Reports

# (03.4)
# -----------------------------------------------------------------------------
# Find QUAST reports and move to DIR [ Reports_MultiQC ]

	# For Loop
		for sample in $SAMPLE_NAME
		do

		# (a) Define DIRNAME
		# Contains only the SAMPLE name with no PATH
			DIRNAME=${sample##*/}

	  # (b) INPUT
	  	INP_report=../results/QUAST_reports/$DIRNAME\_report.tsv

		# (c) Determine if QUAST reports are present
			if [ -z "$INP_report" ];
			then
			# ERROR message
				echo " [ ERROR - No QUAST report found for: ] [ $DIRNAME ] "
			else
			# PASS message
				echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
				echo " [ QUAST report found for: ] [ $DIRNAME ] "
			fi

	  # (d) Create a DIR for each SAMPLE into DIR [ Reports_MultiQC ]
		# PATH & Make DIR
			DIR_SAMPLE=$DIR_Reports/$DIRNAME
			mkdir -p $DIR_SAMPLE

	  # (e) Copy INPUT report (TSV) into DIR [ Reports_MultiQC ]
		  cp $INP_report \
		  	 $DIR_SAMPLE

	  # (f) Rename file
	  # MultiQC only recognizes files named 'report.tsv'
	  # Remove SAMPLE name from 'report.tsv' file
		  mv $DIR_SAMPLE/$DIRNAME\_report.tsv \
		     $DIR_SAMPLE/report.tsv \

		# (g) COMPLETION message
			echo " [ Copying QUAST report ] [ $DIRNAME ] [ COMPLETE ] "
			echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
			echo " "

		done


###############################################################################
# (04) Run MultiQC on DIR [ Reports_MultiQC ]
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# USER message

	echo " "
	echo " ############################################################### "
	echo " [ Running MultiQC on DIR [ Reports_MultiQC ] ] "
	echo " ############################################################### "
	echo " "

# (04.2)
# -----------------------------------------------------------------------------
# INPUT

	# (a) DIR PATH [ Reports_MultiQC ]
	# DIR was created in step 03
		DIR_Reports=../results/Reports_MultiQC

# (04.3)
# -----------------------------------------------------------------------------
# OUTPUT

	# (a) PATH & Make DIR
		OUT_MultiQC=../results/MultiQC/
		mkdir -p $OUT_MultiQC
	# (b) MultiQC OUTPUT report
		OUT_Report_name=MultiQC_Assembly

# (04.4)
# -----------------------------------------------------------------------------
# Run MultiQC

	# Run SOFTWARE
  # SOFTWARE: MultiQC
  # USAGE:
	# --module:     Use only this module
	# --outdir:     Create report in the specified output directory
	# --filename:   Report filename
		multiqc --module quast \
				    --outdir $OUT_MultiQC \
				    --filename $OUT_Report_name \
				    $DIR_Reports

# (04.5)
# -----------------------------------------------------------------------------
# COMPLETION message

	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	echo " [ Running MultiQC ] [ DIR = $DIR_Reports ] [ COMPLETE ] "
	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	echo " "


###############################################################################
# (05) Cleaning work ENVIRONMENT
###############################################################################

# (05.1)
# -----------------------------------------------------------------------------
# USER message

	echo " "
	echo " ###############################################################"
	echo " [ Cleaning Temporary files ] "
	echo " ###############################################################"
	echo " "

# (05.2)
# -----------------------------------------------------------------------------
# Remove DIR [ Reports_MultiQC ] if MultiQC is successful

	if [ -d $OUT_MultiQC$OUT_Report_name\_data ];
	then

	# (a) Found MultiqQC report message
		echo " [ MultiQC report present ] [ Assembly ]"
    echo " "

	# (b) Remove DIR containing reports [ Reports_MultiQC ]
		rm -r $DIR_Reports

	# (c) Remove accesory files created when running MultiQC
		rm -r $OUT_MultiQC$OUT_Report_name\_data

	fi


#############################################################################
# (06) Return to base environment
#############################################################################

# (06.1)
# -----------------------------------------------------------------------------
# Return to the conda base ENVIRONMENT

  conda activate


###############################################################################
# END:
# SCRIPT: 17_Assembly_MultiQC.sh
###############################################################################
