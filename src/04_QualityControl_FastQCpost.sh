#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:         04_QualityControl_FastQCpost.sh
# Written by:     Cieza RJ
# PURPOSE:        Quality control (QC) with FastQC after trimming
# ---------------------------------------------------------------------------

# FastQC (v 0.11.8)
  # QC checks on raw sequence data from high throughput sequencing pipelines
  # https://www.bioinformatics.babraham.ac.uk/projects/fastqc/

# DIR tree
  # Script DIR structure
  # Script should be run from the DIR src within a PROJECT
  # DIR structure shown includes temporary DIR generated with the script

  #  .
  #  ├── bin
  #  ├── projects
  #  |   └── ("PROJECT_name")
  #  |       ├── data
  #  |       ├── data_PROCESSED
  #  |       ├── results
  #  |       |   └── ("SAMPLE_name")
  #  |       |       └── QC
  #  |       |           └── Post
  #  |       └── src
  #  └── reference


#############################################################################
# (00) Supply ARGUMENTS to SCRIPT from the command line
#############################################################################

# (a) Default VALUE
  PROCESSORS=4

# (b) User-supplied values (will overwrite default)
# ARGUMENTS will provided through the command Line
  # [-t]
  while [[ "$1" == -* ]];
  do
    case $1 in

      # PROCESSORS option
      # The argument to the OPTION is placed in the variable $1
      -p|--processsors)
        shift
        PROCESSORS="$1"
        ;;

      # END of arguments
      --)
        shift
        break
        ;;

      # FINISHING the loop
      esac
      shift
    done

# (c) Log message
  echo " ###############################################################"
  echo " "
  echo " [ PROCESSORS used ] [ - $PROCESSORS - ] "
  echo " "
  echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
  echo " "


#############################################################################
# (01) Set up ENVIRONMENT
#############################################################################

# (01.1)
# Activate Python 3 ENVIRONMENT
  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate mypython3


#############################################################################
# (02) Verify SOFTWARE
#############################################################################

# (02.1)
# Section message
  echo " "
  echo " ###############################################################"
  echo " [ SOFTWARE verification] "
  echo " ###############################################################"
  echo " "

# (02.2)
# FastQC should have been installed through Conda (view README)
# Check Fastqc version
  fastqc --version


#############################################################################
# (03) Run FastQC & create a DIR to deposit the results for EACH sample
#############################################################################

# (03.1)
# Section message
  echo " "
  echo " ###############################################################"
  echo " [ Running FastQC ] "
  echo " ###############################################################"
  echo " "

# (03.2)
# Generate a list of the samples present in DIR "data_PROCESSED"
# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of unique names in the DIR (Keep only the sample name)
# Each sample has:
    # Forward (*_R1.fastq.gz)
    # Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
  SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
                sed 's/\_R[1-2].fastq.gz//' | \
                sort -u)

# (03.3)
# For Loop
  for sample in $SAMPLE_NAME
  do

    # (a) Define DIRNAME
    # ---------------------
    # Contains only the SAMPLE name with no PATH
      DIRNAME=${sample##*/}

    # (b) Define INPUT
    # ---------------------
    # Forward FASTQ read (R1)
      R1=$sample\_R1.fastq.gz
    # Reverse FASTQ read (R2)
      R2=$sample\_R2.fastq.gz

    # (c) Define OUTPUT
    # ---------------------
    # OUTPUT PATH
      QCpost=../results/$DIRNAME/QC/Post/
    # Make DIR
      mkdir -p $QCpost

    # (d) Run FastQC on INPUT
    # ---------------------
    # Forward FASTQ read (R1)
      fastqc --threads $PROCESSORS \
             -o $QCpost \
             $R1
    # Reverse FASTQ read (R2)
      fastqc --threads $PROCESSORS \
             -o $QCpost \
             $R2

    # (e) Loop Completion message
    # ---------------------
      echo " "
      echo " [ FastQC analysis ] [ $DIRNAME ] [ COMPLETE ] "
      echo " "
      echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
      echo " "

  done


#############################################################################
# (04) Return to base environment
#############################################################################

# (04.1)
# Return to the conda base ENVIRONMENT
  conda activate


#############################################################################
# END:
# SCRIPT: 04_QualityControl_FastQCpost.sh
#############################################################################
