# The following list contains the REFERENCE GENOME used in evaluating assemblies
# This REFERENCE GENOME will be used by QUAST
#
# TXT file HEADER
common_name format FTP_site
#
# Phylogenetic GROUP A
EcoliHS  fna ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/017/765/GCF_000017765.1_ASM1776v1/GCF_000017765.1_ASM1776v1_genomic.fna.gz
