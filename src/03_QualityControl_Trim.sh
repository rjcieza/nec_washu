#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       03_QualityControl_Trim.sh
# Written by:   Cieza RJ
# PURPOSE:      Remove Illumina adapters and reads shorter than 36 bp
                # Remove N at the end of reads and bases with quality < 10
# ---------------------------------------------------------------------------

# Cutadapt (v 1.18)
  # Cutadapt finds and removes adapter sequences, primers, poly-A tails and other types of unwanted sequences
  # https://cutadapt.readthedocs.io/en/stable/

# DIR tree
  # Script DIR structure
  # Script should be run from the DIR src within a PROJECT
  # DIR structure shown includes temporary DIR generated with the script

  #  .
  #  ├── bin
  #  └── projects
  #  |   └── ("PROJECT_name")
  #  |       ├── data
  #  |       ├── data_PROCESSED
  #  |       ├── results
  #  |       |   └── Trim_report
  #  |       └── src
  #  └── reference


#############################################################################
# (00) Supply ARGUMENTS to SCRIPT from the command line
#############################################################################

# (a) Default VALUE
  PROCESSORS=4

# (b) User-supplied values (will overwrite default)
# ARGUMENTS will provided through the command Line
  # [-t]
  while [[ "$1" == -* ]];
  do
    case $1 in

      # PROCESSORS option
      # The argument to the OPTION is placed in the variable $1
      -p|--processsors)
        shift
        PROCESSORS="$1"
        ;;

      # END of arguments
      --)
        shift
        break
        ;;

      # FINISHING the loop
      esac
      shift
    done

# (c) Log message
  echo " "
  echo " [ THREADS used ] [ $PROCESSORS ] "
  echo " "
  echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
  echo " "


#############################################################################
# (01) Set up ENVIRONMENT
#############################################################################

# (01.1)
# Activate Python 3 ENVIRONMENT
  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate mypython3


#############################################################################
# (02) Verify SOFTWARE
#############################################################################

# (02.1)
# Section message
  echo " "
  echo " ###############################################################"
  echo " [ SOFTWARE verification] "
  echo " ###############################################################"
  echo " "

# (02.2)
# Cutadapt should have been installed through Conda (view README)
# Check cutadapt version
  echo "cutadapt version is:"
  cutadapt --version


################################################################################
# (03) Run Cutadapt & create a DIR [data_PROCESSED] to deposit the results
###############################################################################

# (03.1)
# Section message
  echo " "
  echo " ###############################################################"
  echo " [ Running Cutadapt ] "
  echo " ###############################################################"
  echo " "

# (03.2)
# Define the ADAPTER sequence (indicated in Illumina website)
# Nextera / Nextera XT / Nextera DNA Flex
# Sequences corresponding to library adapters can be present in:
  # FASTQ files at the 3’ end of the reads
# https://support.illumina.com/bulletins/2016/12/what-sequences-do-i-use-for-adapter-trimming.html
  ADAPT=CTGTCTCTTATACACATCT

# (03.3)
# Generate a list of the samples present in DIR "data"
# DIR "data" contains FASTQ paired end (PE) reads
# Make a list of unique names in the DIR (Keep only the sample name)
# Each sample has:
    # Forward (*_R1.fastq.gz)
    # Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
  SAMPLE_NAME=$(find ../data/ -name '*.fastq.gz' | \
		            sed 's/\_R[1-2].fastq.gz//' | \
		            sort -u)

# (03.4)
# Define OUTPUT - not looped

	# (a) OUTPUT PATH
		OUT_Trim=../data_PROCESSED/
    OUT_Trim_report=../results/Trim_report/

  # (b) Make DIR
		mkdir -p $OUT_Trim
    mkdir -p $OUT_Trim_report

  # (c) Trimming report TSV
    OUT_Report_TSV=$OUT_Trim_report\Trim_report.txt

  # (d) Trimming report CSV
    OUT_Report_CSV=$OUT_Trim_report\Trim_report.csv

# (03.5)
# For Loop
  for sample in $SAMPLE_NAME
  do

    # (a) Define DIRNAME
    # Contains only the SAMPLE name with no PATH
      DIRNAME=${sample##*/}

    # (b) Begin loop message
      echo " "
      echo " -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
      echo " "
      echo " [ Trimming & cutting adapters ] [ $DIRNAME ] [ STARTED ] "
      echo " "

    # (c) Define INPUT files
    # (c.1) Forward FASTQ read (R1)
      INP_R1=$sample\_R1.fastq.gz
    # (c.2) Reverse FASTQ read (R2)
      INP_R2=$sample\_R2.fastq.gz

    # (d) Define OUTPUT files
    # (d.1) Forward FASTQ read processed (R1)
      OUT_R1=$OUT_Trim$DIRNAME\_R1.fastq.gz
    # (d.2) Reverse FASTQ read processed (R2)
      OUT_R2=$OUT_Trim$DIRNAME\_R2.fastq.gz

    # (e) Run software:
    # SOFTWARE: cutadapt
    # -------------------------------------------------------------------------
    # USAGE:
    # -j:					          Number of CPU cores to use
    # --report=minimal:     Report a one-line summary
    # --trim-n		          Trim N at the end of reads
    # -q:                   Trim low-quality ends from reads
                            # Specify 1 value, the 3’ end of each read is trimmed
    # -m:LENGTH1:LENGTH2    Minimum length
                            Ln=36
    # -a:                   Adapter assumed to be 3’ end of your sequence
                            # Adapter and sequence following are trimmed
    # -A:                   Corresponding option for R2
    # -o:                   Output
    # -p:                   Paired output file
    # -------------------------------------------------------------------------

    # Run on forward FASTQ read (R1) & reverse FASTQ read (R2)
    # Store REPORT in a TXT file
      cutadapt -j $PROCESSORS \
               --report=minimal \
               --trim-n \
               -q 10 \
               -m $Ln:$Ln \
               -a $ADAPT \
               -A $ADAPT \
               -o $OUT_R1 \
               -p $OUT_R2 \
               $INP_R1 \
               $INP_R2 \
               > \
               TEMP01_Report

    # (f) Add SAMPLE name to REPORT into the FIRST column
      awk -v var=$DIRNAME '{FS = "\t"} ; {if (NR==2) print var FS $0}' \
          TEMP01_Report \
          >> \
          TEMP02_Report

    # (g) Loop completion message
      echo " "
      echo " [ Trimming & cutting adapters ] [ $DIRNAME ] [ COMPLETE ] "
      echo " "
      echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - "
      echo " "

  done

# (03.6)
# Define COLUMN header variables to print to REPORT
# COLUMN headers are the sames used on cutadapt REPORT
# A new header in the first column has been added to identify SAMPLE name
  COL_Header=$( echo SAMPLE'\t'\
                     STATUS'\t'\
                     INPUT_READS'\t'\
                     INPUT_bp'\t'\
                     too_SHORT'\t'\
                     too_LONG'\t'\
                     too_many_N'\t'\
                     OUTPUT_READS'\t'\
                     w/adapters'\t'\
                     qualtrim_bp'\t'\
                     OUTPUT_bp'\t'\
                     w/adapters2'\t'\
                     qualtrim2_bp'\t'\
                     out2_bp )

# (03.7)
# Prints the respective COLUMN headers to REPORT
  awk -v VAR="$COL_Header" 'BEGIN { print VAR }; { print }' \
               TEMP02_Report \
               > \
               $OUT_Report_TSV

# (03.8)
# Generate a CSV delimited report
  cat $OUT_Report_TSV | tr "\t" "," > $OUT_Report_CSV
 # cat TEMP02_Report | tr "\t" "," > TEMP03_Report

# (03.9)
# Remove accesory files created
  rm TEMP01_Report
  rm TEMP02_Report


#############################################################################
# (04) Return to base environment
#############################################################################

# (04.1)
# Return to the base environment
	conda activate


#############################################################################
# END:
# SCRIPT: 03_QualityControl_Trim.sh
#############################################################################
