#!/bin/bash

### -------------------------------------------------------------------------
### SCRIPT:       Scheduler_PBS.sh
### Written by:   Roberto J. Cieza
### PURPOSE:      Submit PBS script with arguments from the command line
## -------------------------------------------------------------------------


###############################################################################
# (01) ARGUMENTS to be passed to PBS
###############################################################################

# ----------------- Command line arguments -------------------------------------

# Determine the ARGUMENTS that will be provided to PBS
# [-n]  JOBNAME or SCRIPT NAME
# [-u]  UNIQNAME
# [-p]  PROCESSORS
# [-t]  TIME in HOURS
  while [[ "$1" == -* ]];
  do

    case $1 in
    # JOBNAME option
    # The argument to the OPTION is placed in the variable $1
      -n|--jobname)
        shift
        JOBNAME="$1"
        ;;

    # UNIQNAME option
    # The argument to the OPTION is placed in the variable $1
      -u|--uniqname)
        shift
        UNIQNAME="$1"
        ;;

    # PROCESSORS option
    # The argument to the OPTION is placed in the variable $1
      -p|--processors)
        shift
        PROCESSORS="$1"
        ;;

    # TIME option
    # The argument to the OPTION is placed in the variable $1
      -t|--time)
        shift
        TIME="$1"
        ;;

    # END of arguments
      --)
        shift
        break
        ;;
    esac

    # FINISHING the loop
    shift
  done

# ----------------- Detect PATH  --------------------------------------------

# Detect present working DIR
  WORKDIR_01=$(pwd)
  WORKDIR_02=$(echo $WORKDIR_01)


###############################################################################
# (02) Verify that ALL OPTIONS received an ARGUMENT
###############################################################################

# Message summarizing parameters to be used to submit job
  echo " "
  echo " ** Below are shown the PARAMETERS used to submit the JOB ** "

# (02.1)
# Exit if JOBNAME is not specified
  if [[ -z "$JOBNAME" ]];
  then
    echo " "
    echo " ##################   ERROR   ############################# "
    echo " JOBNAME not specified. Provide [-n] " >&2
    echo " JOBNAME MUST be the SCRIPT name "
    echo " "
    exit 1
  else
    echo " "
    echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
    echo "1) JOBNAME:                     [ $JOBNAME ]"
  fi

# (02.2)
# Exit if UNIQNAME is not specified
  if [[ -z "$UNIQNAME" ]];
  then
    echo " "
    echo " ##################   ERROR   ############################# "
    echo " UNIQNAME not specified. Provide [-u] " >&2
    echo " UNIQNAME will be appended to @med.umich.edu "
    echo " "
    exit 1
  else
    echo "2) UNIQNAME:                    [ $UNIQNAME ]"
    echo "3) UNIQNAME mail:               [ $UNIQNAME@med.umich.edu ]"
  fi

# (02.3)
# Exit if PROCESSORS is not a number
  if [[ "$PROCESSORS" -le 0 ]];
  then
    echo " "
    echo " ##################   ERROR   ############################# "
    echo " PROCESSORS must be a positive integer. Provide [-p] " >&2
    echo " "
    exit 1
  else
    echo "4) PROCESSORS used in 1 node:   [ $PROCESSORS ]"
  fi

# (02.4)
# Exit if TIME is not a number
  if [[ "$TIME" -le 0 ]];
  then
    echo " "
    echo " ##################   ERROR   ############################# "
    echo " TIME must be a positive integer. Provide [-t] " >&2
    echo " Provide TIME in HOURS "
    echo " "
    exit 1
  else
    echo "5) TIME in hours:               [ $TIME ]"
  fi


# (02.4)
# Print full PATH to SCRIPT and SCRIPT name:
  echo "6) PATH to SCRIPT:              [ $WORKDIR_02 ]"
  echo "7) SCRIPT name:                 [ $JOBNAME.sh ]"
  echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
  echo " "


###############################################################################
## (03) START of #PBS directives
###############################################################################

# Begin message
  echo " "
  echo " ** Below is shown your PBS script ** "
  echo " ** This is how the PBS script will be submitted: ** "
  echo " ############################################################# "
  echo " "

# Here Document
# Redirect input into an interactive shell script or program (qsub)
cat << EOF > PBSjob_$JOBNAME.pbs
### The interpreter used to execute the script (bash)

### ------------------------------------------------------------------------
### PART 1
### START of #PBS directives
### Output and error file by default will be copied -o

#PBS -S /bin/sh
#PBS -N $JOBNAME
#PBS -M $UNIQNAME@med.umich.edu
#PBS -m abe
#PBS -A youngvi_fluxm
#PBS -l qos=flux
#PBS -q fluxm
#PBS -l nodes=1:ppn=$PROCESSORS,pmem=10gb
#PBS -l walltime=$TIME:00:00
#PBS -j oe
#PBS -V

### Switch to the working DIR
### By default TORQUE launches processes from your HOME directory
cd \$PBS_O_WORKDIR


### ------------------------------------------------------------------------
### PART 2
### Display job information

echo " "
echo " Job identifiers are: "
echo " -------------------- "
echo " "
echo " Running from: "
echo " \$PBS_O_WORKDIR "
echo " "
echo " PATH to script:        $WORKDIR_02 "
echo " SCRIPT:                $JOBNAME.sh "
echo " PBS Job ID:            \$PBS_JOBID"
echo " PBS Job name:          \$PBS_JOBNAME "
echo " Running from DIR:      \$PBS_O_WORKDIR "
echo " PROCESSORS:            $PROCESSORS "
echo " PROCESSORS used:"
echo " ---------- "
echo " "
cat \$PBS_NODEFILE
echo " "
echo " ---------- "
echo " "


### ------------------------------------------------------------------------
### PART 3
### TASK to be submitted to PBS [ SCRIPT ]

sh $(pwd)/$JOBNAME.sh \
-p $PROCESSORS

### ------------------------------------------------------------------------
### PART 4
### Clear workspace

rm PBSjob_$JOBNAME.pbs


EOF

# Print PBS script to STDOUT
  cat PBSjob_$JOBNAME.pbs

# End message
  echo " "
  echo " ** END ** "
  echo " ############################################################# "
  echo " "

###############################################################################
## (04) Commit message
###############################################################################

# (04.1)
# Prompt user INPUT to proceed to QSUB submission
  while true;
  do

  # YES - NO prompt message
    echo " "
    echo " ############################################################# "
    echo " "
    echo " Please review the PBS directives above "
    read -p " Is this JOB ready to submit to PBS [y/n]? " yn

    case $yn in
    [Yy]* )

    # Submit script with QSUB command
      qsub PBSjob_$JOBNAME.pbs
      echo " "
      echo " [ JOB was submitted ] "
      echo " "
      echo " ############################################################# "
      echo " "

    # Otherwise exit
  	  break;;

    [Nn]* ) exit;;
    esac
  done


#############################################################################
# END:
# SCRIPT: Scheduler_PBS.sh
#############################################################################
