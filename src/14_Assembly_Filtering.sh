#!/bin/bash

# -----------------------------------------------------------------------------
# SCRIPT:				14_Assembly_Filtering.sh
# Written by:   Cieza RJ
# PURPOSE:      Remove small & low coverage contigs
# -----------------------------------------------------------------------------

# Fastagrep.pl
	# Remove small & low coverage contigs from SPAdes assembly
	# https://github.com/rec3141/rec-genome-tools/blob/master/bin/fastagrep.pl

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  |   └── Fastagrep
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       |   └── Contigs_Filtered
#  |       |   └── ("SAMPLE_name")
#  |       |       └── SPAdes
#  |       └── src
#  └── reference


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# EXECUTABLES

	# (a) Define PATH for Fastagrep executable
	  run_Fastagrep=../../../bin/Fastagrep/./fastagrep.pl


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ SOFTWARE verification] "
  echo " ###############################################################"
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE

# Fastagrep should have been installed into DIR [ bin ] (view README)
  $run_Fastagrep


###############################################################################
# (03) Run Fastagrep
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ Filtering small & low coverage contigs from assembly ] "
  echo " ###############################################################"
  echo " "

# (03.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR "data_PROCESSED"

# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of unique names in the DIR (Keep only the sample name)
# Each sample has:
  	# Forward (*_R1.fastq.gz)
  	# Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
		  					sed 's/\_R[1-2].fastq.gz//' | \
		  					sort -u)

# (03.3)
# -----------------------------------------------------------------------------
# Remove small & low coverage contigs

# For Loop
  for sample in $SAMPLE_NAME
  do

	# (a) Define DIRNAME
	# Contains only the SAMPLE name with no PATH
	  DIRNAME=${sample##*/}

	# (b) Define INPUT
	# INPUT PATH
		INP_DIR=../results/$DIRNAME/SPAdes
	# Contigs.fasta
		INP_contigs=$INP_DIR/$DIRNAME\_contigs.fasta

	# (c) Define OUTPUT
  # OUTPUT PATH
    OUT_DIR=../results/Contigs_Filtered
  # Make DIR
    mkdir -p $OUT_DIR
  # OUTPUT File 1:
	# Contains a list of the high coverage contigs
  	OUT_list=$OUT_DIR/$DIRNAME\_contigs.txt
	# OUTPUT File 2:
	# Contains high coverage contigs
		OUT_contigs=$OUT_DIR/$DIRNAME\_HCov.contigs.fasta

	# (d) START message
		echo " "
		echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
		echo " "
		echo " [ FILTERING CONTIGS ] [ SAMPLE name: $INP_contigs ] "

	# (e) Filter contigs
	# grep: 	Extract all contigs
	# sed:		Remove underscore from contig name
	# sort:		Numeric, reverse and key sort (Column 6 = coverage value)
	# awk:		Coverage more than 2.0 and length more than 500 bp
	# sed:		Restore underscore from contig
	# sed:		Remove '>' from contig name
		grep -F ">" $INP_contigs | \
		sed -e 's/_/ /g' | \
		sort -nrk 6 | \
		awk '$6 >= 2.0 && $4 >= 500 {print $0}' | \
		sed -e 's/ /_/g' | \
		sed -e 's/>//g' > $OUT_list

	# (f) Run SOFTWARE
  # SOFTWARE: Fastagrep
  # USAGE:
	# -f:				Take patterns from file, one per line
		$run_Fastagrep -f $OUT_list \
									 $INP_contigs \
									 > \
									 $OUT_contigs

 	# (g) Remove accesory files generated
    rm $OUT_list

	# (h) Calculate contigs kept
		grep -c ">" $OUT_contigs > Number
		NUM_contigs=$(cat Number)
		rm Number

	# (i) COMPLETION message
	  echo " [ NUMBER OF CONTIGS TO KEEP ] [ SAMPLE name: $DIRNAME ] "
		echo " [ $NUM_contigs ] "
	  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	  echo " "

	done


###############################################################################
# END:
# SCRIPT: 14_Assembly_Filtering.sh
###############################################################################
