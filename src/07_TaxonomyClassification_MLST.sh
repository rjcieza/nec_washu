#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       07_TaxonomyClassification_MLST.sh
# Written by:   Cieza RJ
# PURPOSE:      Determine Multilocus Sequence (MLST) Type with ARIBA
# ---------------------------------------------------------------------------

# ARIBA (v 2.12.0)
  # Antimicrobial Resistance Identification By Assembly (ARIBA) tool
  # ARIBA be used for MLST calling
  # Warwick Medical School scheme (Escherichia coli)
  # https://github.com/sanger-pathogens/ariba/wiki/MLST-calling-with-ARIBA

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  ├── projects
#  |   └── ("Your_PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       |   ├── MLST
#  |       |   └── ("SAMPLE_name")
#  |       |   			└── MLST          -- DIR removed upon completion -
#  |       └── src
#  |           └── DATABASE           -- DIR removed upon completion --
#  └── reference


###############################################################################
# (00) Supply ARGUMENTS to SCRIPT from the command line
###############################################################################

# (00.1)
# Determine the number of processors used in the SCRIPT.
# If no value is provided, 4 processors will be used
# -----------------------------------------------------------------------------
# (a) Default VALUE
  PROCESSORS=4

# (00.2)
# User-supplied values (will overwrite default)
# -----------------------------------------------------------------------------
# ARGUMENTS will provided through the command Line
# [-p]
  while [[ "$1" == -* ]];
  do
    case $1 in
      # (a) PROCESSORS option
      # - - - - - - - - - - - - - - - -
      # The argument to the OPTION is placed in the variable $1
      -p|--processsors)
        shift
        PROCESSORS="$1"
        ;;
      # (b) END of arguments
      # - - - - - - - - - - - - - - - -
      --)
        shift
        break
        ;;
      # FINISHING the loop
      esac
      shift
    done

# (00.3)
# Log message
# -----------------------------------------------------------------------------
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "
  echo " [ PROCESSORS used ] [ - $PROCESSORS - ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# Activate Python 3 ENVIRONMENT
# -----------------------------------------------------------------------------
# (a) PATH to DIR where "conda.sh" is located
  . ~/miniconda3/etc/profile.d/conda.sh
# (b) conda activate command followed by your ENVIRONMENT name
  conda activate mypython3


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# Section message
# -----------------------------------------------------------------------------
	echo " "
	echo " ###############################################################"
	echo " [ SOFTWARE verification] "
	echo " ###############################################################"
	echo " "

# (02.2)
# Check ARIBA version
# -----------------------------------------------------------------------------
# ARIBA should have been installed through Conda (view README)
	ariba version


###############################################################################
# (03) Select MLST database
###############################################################################

# (03.1)
# Section message
# -----------------------------------------------------------------------------
	echo " "
	echo " ###############################################################"
	echo " [ Downloading DATABASE ] "
	echo " ###############################################################"
	echo " "

# (03.2)
# Define SPECIES database to be imported
# -----------------------------------------------------------------------------
# PubMLST:
# Public databases for molecular typing and microbial genome diversity
# https://pubmlst.org/

# List of DATABASES can be obtained with the following command:
# - ariba pubmlstspecies

# Database USED: Warwick Medical School
  echo " "
  echo " [ DATABASE used ] "
  echo ' [ "Escherichia coli#1" ] '
  echo " "

# (03.3)
# Retrive database from Pubmlst.org
# -----------------------------------------------------------------------------
# (a) Download DATABASE
  ariba pubmlstget \
        "Escherichia coli#1" \
        DATABASE
# (b) PATH to DATABASE
  MLST_DB=DATABASE/ref_db


###############################################################################
# (04) Run ARIBA
###############################################################################

# (04.1)
# Section message
# -----------------------------------------------------------------------------
  echo " "
  echo " ###############################################################"
  echo " [ Running ARIBA to determine MLST ] "
  echo " ###############################################################"
  echo " "

# (04.2)
# Generate a list of the samples present in DIR "data_PROCESSED"
# -----------------------------------------------------------------------------
# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of unique names in the DIR (Keep only the sample name)
# Each sample has:
  	# Forward (*_R1.fastq.gz)
  	# Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
		  					sed 's/\_R[1-2].fastq.gz//' | \
		  					sort -u)

# (04.3)
# Define OUTPUT
# -----------------------------------------------------------------------------
# (a) OUTPUT PATH
  OUT_MLST=../results/MLST/
# (b) Make DIR
  mkdir -p $OUT_MLST
# (c) OUTPUT files (CSV and TSV)
  MLST_REPORT_TSV=$OUT_MLST/MLST_Report_Warwick.tsv
  MLST_REPORT_CSV=$OUT_MLST/MLST_Report_Warwick.csv

# (04.4)
# MLST calling with ARIBA
# -----------------------------------------------------------------------------
# For Loop
  for sample in $SAMPLE_NAME
  do
  # (a) Define DIRNAME
  # - - - - - - - - - - - - - - - -
  # Contains only the SAMPLE name with no PATH
    DIRNAME=${sample##*/}

  # (b) Define INPUT
  # - - - - - - - - - - - - - - - -
  # Use FASTQ reads after processing them with cutadapt
	# Forward FASTQ read (R1)
		R1=$sample\_R1.fastq.gz
	# Reverse FASTQ read (R2)
		R2=$sample\_R2.fastq.gz

  # (c) Define OUTPUT
  # - - - - - - - - - - - - - - - -
  # Create a DIR to deposit results for each individual SAMPLE
    mkdir -p ../results/$DIRNAME
  # ARIBA generates OUTPUT DIR
  # PATH
    OUT_DIR=../results/$DIRNAME/MLST
  # TSV report
    OUT_SAMPLE_TSV=$OUT_DIR/mlst_report.tsv

  # (d) Run software
  # - - - - - - - - - - - - - - - -
  # SOFTWARE: ARIBA
  # USAGE:
  # ariba run [options] <prepareref_dir> <reads1.fq> <reads2.fq> <outdir>
    ariba run \
          --threads $PROCESSORS \
          $MLST_DB \
          $R1 \
          $R2 \
          $OUT_DIR

  # (e) Add SAMPLE name to MLST report into the first COLUMN
  # - - - - - - - - - - - - - - - -
    awk -v var="$DIRNAME" '{FS = "\t"} ; {print var FS $0}' \
           $OUT_SAMPLE_TSV >> $MLST_REPORT_TSV

  # (j) Task Completion message
  # - - - - - - - - - - - - - - - -
    echo " "
    echo " [ MLST analysis complete ] [ $DIRNAME ] "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
  done


###############################################################################
# (05) Format final MLST report ( contains SAMPLE names and COLUMN headers )
###############################################################################

# (05.1)
# Define OUTPUT
# -----------------------------------------------------------------------------
# (a) MLST reports - temporary files
  TMP1_report=../results/MLST/Out1.tsv
  TMP2_report=../results/MLST/Out2.tsv
# (b) OUTPUT files (CSV and TSV)
  MLST_REPORT_TSV=$OUT_MLST/MLST_Report_Warwick.tsv
  MLST_REPORT_CSV=$OUT_MLST/MLST_Report_Warwick.csv

# (05.2)
# Define COLUMN header variables to print to REPORT
# -----------------------------------------------------------------------------
# COLUMN headers are the sames used on cutadapt REPORT
# A new header in the first column has been added to identify SAMPLE name
  COL_Header=$( echo SAMPLE'\t'\
                     ST'\t'\
                     adk'\t'\
                     fumC'\t'\
                     gyrB'\t'\
                     ticd'\t'\
                     tmdh'\t'\
                     purA'\t'\
                     recA )

# (05.3)
# Prints the respective COLUMN headers to REPORT
# -----------------------------------------------------------------------------
# (a) Remove headers from TSV reports generated for individual samples
  awk 'NR%2==0' $MLST_REPORT_TSV >> $TMP1_report
# (b) Print column headers to TSV report (FIRST row)
  awk -v VAR="$COL_Header" 'BEGIN { print VAR }; { print }' \
              $TMP1_report > $TMP2_report
# (c) Copy formatted TSV report into final location
  cat $TMP2_report > $MLST_REPORT_TSV
# (d) Generate a CSV delimited report
  cat $MLST_REPORT_TSV | tr "\t" "," > $MLST_REPORT_CSV

# (05.4)
# Remove accesory files generated
# -----------------------------------------------------------------------------
# (a) Temporary reports
  rm $TMP1_report
  rm $TMP2_report
# (b) Database DIR [ DATABASE ]
  rm -R DATABASE/
# (c) DIR [ MLST ] generated for each SAMPLE
# DIR not needed after compiling results into a single file (TSV and CSV)
# For Loop
  for sample in $SAMPLE_NAME
  do
  # (a) Define DIRNAME
  # - - - - - - - - - - - - - - - -
  # Contains only the SAMPLE name with no PATH
    DIRNAME=${sample##*/}

  # (b) Remove DIR [ MLST ] for each SAMPLE
  # - - - - - - - - - - - - - - - -
    rm -R ../results/$DIRNAME/MLST
    rm -R ../results/$DIRNAME

  # (c) Task Completion message
  # - - - - - - - - - - - - - - - -
	  echo " "
    echo " [ DELETE accesory files COMPLETE ] [ $DIRNAME ] "
    echo " "
  done


###############################################################################
# (06) Return to base environment
###############################################################################

# (06.1)
# Return to the base environment
# -----------------------------------------------------------------------------
	conda activate
#  awk 'BEGIN {print "SAMPLE\tST\tadk\tfumC\tgyrB\ticd\tmdh\tpurA\trecA"}; {print}' $TMP1 > $TMP2


###############################################################################
# END:
# SCRIPT: 07_TaxonomyClassification_MLST.sh
###############################################################################
