#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       09_Mapping_SPANDx.sh
# Written by:   Cieza RJ
# PURPOSE:      Comparative genomics analysis with SPANDx
# ---------------------------------------------------------------------------

# SPANDx:
  # Comparative genomics pipeline for microbes
  # Identify SNP and indel variants in haploid genomes
  # SPANDx combines: [BWA, SAMtools, BEDtools, Picard, GATK, VCFtools and SnpEff]
  # https://github.com/dsarov/SPANDx

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  |   └── SPANDx
#  |       └── snpEff
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       └── src
#  └── reference
#      └── Reference_List_SPANDx.txt


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# EXECUTABLES

# (a) Define PATH for SPANx executable
  run_SPANDx=../../../bin/SPANDx/./SPANDx.sh
# (b) Define PATH for snpEff executable
  snpEff=../../../bin/SPANDx/snpEff/snpEff.jar

# (01.2)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ SOFTWARE verification] "
  echo " ############################################################### "
  echo " "

# (01.3)
# -----------------------------------------------------------------------------
# Check SPANDx can be executed
  $run_SPANDx -h | head -n 3
  echo " "


###############################################################################
# (02) ANNOTATION REFERENCE required for SPANDx
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# ANNOTATION REFERENCE
# The name formatting of ANNOTATION REFERENCE for SPANDx is strict
# Name must exactly match database found in snpEff.config file

# (a) Define REFERENCE GENOME you are searching for
  REF_snpEff_search=Escherichia_coli_HS_uid58393

# (b) Search for your REFERENCE GENOME in snpEff DATABASE
  echo " "
  echo " ############################################################### "
  echo " [ Search for REFERENCE GENOME in snpEff DATABASE ] "
  echo " ############################################################### "
  echo " "
  echo " List of DATABASES found: "
  echo " "
  java -jar $snpEff databases | grep -i $REF_snpEff_search
  echo " "
  echo " [ Search COMPLETED ] "
  echo " "

# (c) Obtain CHROMOSOME NAME from the line "Chromosome names [sizes]"
  # java -jar $snpEff -v $REF_snpEff_search


###############################################################################
# (03) Download REFERENCE GENOME
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ STARTING REFERENCE GENOME download ] "
  echo " ############################################################### "
  echo " "

# (03.2)
# -----------------------------------------------------------------------------
# REFERENCE List TXT file:
# - Reference_List_SPANDx.txt
# - Reference_List_SPANDx.txt should be in DIR [ reference ]

# (a) REFERENCE list VARIABLE (TXT file)
  List=../../../reference/Reference_List_SPANDx.txt

# (b) Detecting the presence or absence of REFERENCE list
  if [ -e "$List" ]; then
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE LIST exists - PROCEED ] "
    echo " "
  else
    echo " "
    echo " [ REFERENCE LIST does NOT exist - ABORT ] "
    echo " "
  fi

# (c) Read TXT file and skip lines starting with #:
  grep -o '^[^#]*' $List > REFERENCE_list.txt

# (03.3)
# -----------------------------------------------------------------------------
# Download & process REFERENCE GENOME

# Read TXT file
# 1st column:   common_name
# 2nd column:   format
# 3rd column:   FTP_site
  while read common_name format FTP_site
  do
  # (a) Skip the header
    [ "$common_name" == "common_name" ] && continue

  # (b) Download REFERENCE from NCBI
  # Begin task message
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE download BEGINS ] [ REFERENCE name: $common_name ] "
    echo " "
  # Download genome
	  wget $FTP_site
  # End task message
	  echo " [ REFERENCE download COMPLETE ] [ REFERENCE name: $common_name ] "

  # (c) DIR where downloaded REFERENCE will be deposited
  # OUTPUT PATH
    OUT_REF_PATH=../../../reference/$common_name
  # Make DIR
    mkdir -p $OUT_REF_PATH

  # (d) Unzip GZ file
  # INPUT file
    REF_01=$(echo *.gz)
  # Unzip
    gunzip $REF_01

  # (e) Move unzipped file to DIR [ reference ]
  # INPUT file
    REF_02=$(echo *.$format)
  # Move file
    mv $REF_02 \
       $OUT_REF_PATH

  # (f) REFERENCE variable at DIR [reference]
    REF_03=$(echo $OUT_REF_PATH/*.$format)

  # (g) Copy REFERENCE to workind DIR [src]
    cp $REF_03 .

  # (h) Rename REFERENCE using VARIABLE '$common_name'
  # Change FNA extension for FASTA
  # FASTA ending required, otherwise SPANDx won't read it
    mv ./$REF_02 \
       $common_name\.fasta

  # (i) Remove accesory files created
    rm $REF_03

  # (j) Remove accesory DIR created
  # REFERENCE PATH
    REF_DIR=$(echo $OUT_REF_PATH)
    rm -R $REF_DIR

  # (j) Loop completion message
  	echo " [ REFERENCE processing COMPLETE ] [ REFERENCE name: $common_name ]"
  	echo " "
  	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  done < REFERENCE_list.txt
  # Remove modified List generated
  rm REFERENCE_list.txt


###############################################################################
# (04) Preparing samples to run SPANDx
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ Preparing samples to run SPANDx ] "
  echo " ############################################################### "
  echo " "

# (04.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR "data_PROCESSED"

# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of unique names in the DIR (Keep only the sample name)
# Each sample has:
  	# Forward (*_R1.fastq.gz)
  	# Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
		  					sed 's/\_R[1-2].fastq.gz//' | \
		  					sort -u)

# (04.3)
# -----------------------------------------------------------------------------
# Copy processed FASTQ reads into SPANDx working DIR [ src ]

# For Loop
  for sample in $SAMPLE_NAME
  do
  # (a) Define DIRNAME
  # Contains only the SAMPLE name with no PATH
    DIRNAME=${sample##*/}

  # (b) Define INPUT
  # Use FASTQ reads after processing them with cutadapt
	# Forward FASTQ read (R1)
		R1=$sample\_R1.fastq.gz
	# Reverse FASTQ read (R2)
		R2=$sample\_R2.fastq.gz

  # (c) Copy FASTQ reads into DIR [ src ]
    cp $R1 $R2 .

  # (d) Loop completion message
    echo " [ FASTQ read TRANSFER COMPLETE ] [ $DIRNAME ] "
    echo " "
  done

# (04.4)
# -----------------------------------------------------------------------------
# Rename FASTQ files to SPANDx required format

# (a) Define INPUT list
# Forward FASTQ read (R1)
  R1_list=$(ls *_R1.fastq.gz | sort)
# Reverse FASTQ read (R2)
  R2_list=$(ls *_R2.fastq.gz | sort)

# (b) Rename FORWARD FASTQ reads to SPANDx required format
# For Loop
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  for file in $R1_list
  do
  # Substitute (_R1) with the required ending (_1_sequence)
    mv $file $(echo $file | sed 's/\_R1/\_1_sequence/g')
  # Loop Completion message
    echo " "
    echo " [ Renaming R1 COMPLETE ] [ $file ] "
  done

# (c) USER message
  echo " "
  echo " [ Renaming FORWARD R1 reads COMPLETE ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "

# (d) Rename REVERSE FASTQ reads to SPANDx required format
# For Loop
  for file in $R2_list
  do
  # Substitute (_R2) with the required ending (_2_sequence)
    mv $file $(echo $file | sed 's/\_R2/\_2_sequence/g')
  # Loop Completion message
    echo " "
    echo " [ Renaming R2 COMPLETE ] [ $file ] "
  done

# (e) USER message
  echo " "
  echo " [ Renaming REVERSE R2 reads COMPLETE ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "


###############################################################################
# (05) Running SPANDx pipeline
###############################################################################

# (05.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ Running SPANDx pipeline ] "
  echo " ############################################################### "
  echo " "

# (05.2)
# -----------------------------------------------------------------------------
# Renaming REFERENCE GENOME to input it under Flag -r in SPANDx

# PATH to REFERENCE GENOME
# REFERENCE GENOME was downloaded in Section (03)
# SPANDx requires you to exclude ( .fasta ) extension
# Substitute ( .fasta ) ending with ( )
  REF_SPANDx=$(echo *.fasta | sed 's/\.fasta//g')
# USER message
  echo " "
  echo " [ REFERENCE GENOME renaming COMPLETE ] [ $REF_SPANDx ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
# Match REFERENCE GENOME chromosome name to SnpEff ANNOTATION DATABASE
# The chromosome name of REFERENCE GENOME must match those used by SnpEff
  sed -i 's/\>.*//' $REF_SPANDx.fasta

# (05.3)
# -----------------------------------------------------------------------------
# Run SOFTWARE

# SOFTWARE: SPANDx
# USAGE:
# -r:     Reference excluding the '.fasta' extension
# -m:     Create a matrix with all orthologous SNP variants (core genome)
# -i: 		Create a matrix with all orthologous indel variants
# -a:     Annotation of variants with snpEff
# -v:     Reference genome for annotation
#         - Name must match variable in
# -t:     Technology:   Illumina, Illumina_old, 454, PGM
# -p:     Pairing of reads (SE or PE)
  $run_SPANDx -r $REF_SPANDx \
              -m yes \
              -i yes \
              -a yes \
              -v $REF_snpEff_search \
              -t Illumina \
              -p PE


# #############################################################################
# # END:
# # SCRIPT: 09_Mapping_SPANDx.sh
# #############################################################################
