#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       08_Mapping_ReferenceGenomes.sh
# Written by:   Cieza RJ
# PURPOSE:      Generate FASTQ reads from REFERENCE GENOMES to be used in the analysis
# -----------------------------------------------------------------------------

# ART ( Q Version 2.5.8 (June 7, 2016) )
  # Simulates sequencing reads by mimicking real sequencing process
  # Takes REFERENCE GENOME (FASTA format) as input and outputs artificial FASTQ files
  # https://www.niehs.nih.gov/research/resources/software/biostatistics/art/index.cfm

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  |   └── ART
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       |   └── ART_FASTQ          -- DIR removed upon completion --
#  |       └── src
#  └── reference
#      └── Reference_List_ART.txt     -- FILE removed upon completion --


#############################################################################
# (01) Set up ENVIRONMENT
#############################################################################


# (01.1)
# ART executable
# -----------------------------------------------------------------------------
  # (a) Export PATH for ART executable
    export PATH=$PATH:../../../bin/ART/
  # (b) ART executable VARIABLE
    ART=../../../bin/ART/art_illumina


# (01.2)
# Section message
# -----------------------------------------------------------------------------
  echo " "
  echo " ###############################################################"
  echo " [ SOFTWARE verification] "
  echo " ###############################################################"
  echo " "


# (01.3)
# Check ART illumina can be executed
# -----------------------------------------------------------------------------
  ./$ART --help | head -n 7


###############################################################################
# (02) Download REFERENCE Genome
###############################################################################


# (02.1)
# Section message
# -----------------------------------------------------------------------------
  echo " "
  echo " ###############################################################"
  echo " [ STARTING REFERENCE GENOMES download ] "
  echo " ###############################################################"
  echo " "


# (02.2)
# Create a DIR to deposit all downloaded REFERENCE genomes to input into ART
# Reference_List_ART.txt should be in DIR [reference]
# -----------------------------------------------------------------------------
  # (a) OUTPUT PATH
    ART_FASTQ=../results/ART_FASTQ
  # (b) Make DIR
    mkdir -p $ART_FASTQ


# (02.3)
# TXT file:
# - Reference_List_ART.txt
# - Reference_List_ART.txt should be in DIR [reference]
# 1st column:   common_name
# 2nd column:   format
# 3rd column:   FTP_site
# -----------------------------------------------------------------------------
  # (a) REFERENCE list VARIABLE
  # TXT file
    List=../../../reference/Reference_List_ART.txt

  # (b) Detecting the presence or absence of REFERENCE list
    if [ -e "$List" ]; then
      echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
      echo " "
      echo " [ REFERENCE LIST exists - PROCEED ] "
      echo " "
    else
      echo " "
      echo " [ REFERENCE LIST does NOT exist - ABORT ] "
      echo " "
    fi

  # (c) Read TXT file and skip lines starting with #:
    grep -o '^[^#]*' $List > REFERENCE_list.txt


# (02.4)
# Download & process REFERENCE GENOMES
# -----------------------------------------------------------------------------
# Read TXT file
  while read common_name format FTP_site
  do
  # (a) Skip the header
    [ "$common_name" == "common_name" ] && continue

  # (b) Download REFERENCE from NCBI
  # (b.1) Begin task message
    echo " "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE download BEGINS ] [ REFERENCE name: $common_name ] "
    echo " "
  # (b.2) Download genome
	  wget $FTP_site
  # (b.3) End task message
	  echo " [ REFERENCE download COMPLETE ] [ REFERENCE name: $common_name ] "

  # (c) DIR where downloaded REFERENCE will be deposited
  # (c.1) OUTPUT PATH
    OUT_REF_PATH=../../../reference/$common_name
  # (c.2) Make DIR
    mkdir -p $OUT_REF_PATH

  # (d) Unzip GZ file
  # (d.1) INPUT file
    REF_01=$(echo *.gz)
  # (d.2) Unzip
    gunzip $REF_01

  # (e) Move unzipped file to DIR [reference]
  # (e.1) INPUT file
    REF_02=$(echo *.$format)
  # (e.2) Move file
    mv $REF_02 \
       $OUT_REF_PATH

  # (f) REFERENCE variable at DIR [reference]
    REF_03=$(echo $OUT_REF_PATH/*.$format)

  # (g) Copy REFERENCE to workind DIR [src]
    cp $REF_03 \
       .

  # (h) Rename REFERENCE using VARIABLE '$common_name'
  # Keep FNA extension
    mv ./$REF_02 \
       $common_name\.fna

  # (i) Remove accesory files created
    rm $REF_03

  # (j) Move FNA file from working DIR [src] to DIR [reference]
    mv *.fna \
       $OUT_REF_PATH

  # (k) Final REFERENCE VARIABLES
  # REFERENCE file
    REF_file=$(echo $OUT_REF_PATH/$common_name.fna)
  # REFERENCE PATH
    REF_DIR=$(echo $OUT_REF_PATH)

  # (l) Move REFERENCE to DIR [ART_FASTQ]
    mv $REF_file \
       $ART_FASTQ

  # (m) Remove accesory DIR created
     rm -R $REF_DIR

  # (n) Task Completion message
	  echo " [ REFERENCE processing COMPLETE ] [ REFERENCE name: $common_name ]"
	  echo " "
	  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  done < REFERENCE_list.txt


###############################################################################
# (03) Generate FASTQ reads from REFERENCE GENOMES
###############################################################################


# (03.1)
# Section message
# -----------------------------------------------------------------------------
  echo " "
  echo " ###############################################################"
  echo " [ Generating FASTQ reads for REFERENCE GENOMES ] "
  echo " ###############################################################"
  echo " "


# (03.2)
# Generate a list of the samples present in DIR "ART_FASTQ"
# -----------------------------------------------------------------------------
# DIR "ART_FASTQ" contains FNA files to be used as an input to generate FASTQ
# Make a list of unique names in the DIR (Keep only the sample name)
# find:   Find samples in DIR "ART_FASTQ"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	SAMPLE_NAME=$(find $ART_FASTQ -name '*.fna' | \
		  					sed 's/\.fna//' | \
		  					sort -u)


# (03.3)
# Convert FASTA files into FASTQ reads
# -----------------------------------------------------------------------------
# For Loop
  for sample in $SAMPLE_NAME
  do
  # (a) Define DIRNAME
	# - - - - - - - - - - - - - - - -
	# Contains only the SAMPLE name with no PATH
	  DIRNAME=${sample##*/}

  # (b) Define INPUT
	# - - - - - - - - - - - - - - - -
  # Use FNA file
    INPUT_FNA=$ART_FASTQ/$DIRNAME.fna

  # (c) Define OUTPUT
  # - - - - - - - - - - - - - - - -
  # PATH to deposite compressed FASTQ reads
    OUT_DIR_processed=../data_PROCESSED

  # (d) Run software
  # - - - - - - - - - - - - - - - -
  # SOFTWARE: ART Illumina
  # USAGE:
  # -p:       Paired end reads
  # -i:       Input FNA files
  # -l:       Read lenght (MiSeq Read Length)
  # -f:       Fold coverage (Enough reads to match sequenced samples)
  # -m:       Mean fragment length for paired end simulations
  # -s:       Standard deviation of fragment size for PE simulations
  # -o:       OUTPUT PATH
    ./$ART -p \
           -i $INPUT_FNA \
           -l 150 \
           -f 150 \
           -m 200 \
           -s 10 \
           -o $ART_FASTQ/

  # (e) Rename FORWARD and REVERSE reads to include REFERENCE name
  # - - - - - - - - - - - - - - - -
  # INPUT Forward FASTQ read (R1) and Reverse FASTQ read (R2)
    R1=$(echo $ART_FASTQ/1.fq)
    R2=$(echo $ART_FASTQ/2.fq)
  # Rename FASTQ reads
    mv $R1 $ART_FASTQ/$DIRNAME\_R1.fastq
    mv $R2 $ART_FASTQ/$DIRNAME\_R2.fastq
  # Completion message
    echo " "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " [ RENAMING COMPLETE ] [ $DIRNAME ] "
    echo " "

  # (f) Remove accesory files generated
  # - - - - - - - - - - - - - - - -
  # - - - - - - - - - - - - - - - -
    rm $ART_FASTQ/1.aln
    rm $ART_FASTQ/2.aln
    rm $ART_FASTQ/$DIRNAME.fna

  # (g) Compress generated FASTQ reads
  # - - - - - - - - - - - - - - - -
  # INPUT Forward FASTQ read (R1) and Reverse FASTQ read (R2)
    R1_FQ=$(echo $ART_FASTQ/$DIRNAME\_R1.fastq)
    R2_FQ=$(echo $ART_FASTQ/$DIRNAME\_R2.fastq)
  # Compress FASTQ reads
  # FASTQ reads should end with this format:
  # - ReferenceName_R1.fastq.gz
  # - ReferenceName_R2.fastq.gz
    gzip $R1_FQ
    gzip $R2_FQ

  # (h) Copy compressed FASTQ files into DIR [data_PROCESSED]
  # - - - - - - - - - - - - - - - -
    cp $ART_FASTQ/*.fastq.gz \
       $OUT_DIR_processed

  # (i) Remove accesory files generated
  # - - - - - - - - - - - - - - - -
    rm $R1_FQ.gz
    rm $R2_FQ.gz

  # (j) Task Completion message
  # - - - - - - - - - - - - - - - -
	  echo " [ FASTQ reads generation COMPLETE ] [ REFERENCE name: $DIRNAME ] "
	  echo " "
	  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	  echo " "
  done


# (03.4)
# Remove accesory files generated
# -----------------------------------------------------------------------------
# Accesory DIR generated [ART_FASTQ]
  rm -R $ART_FASTQ
# REFERENCE list without comments
  rm REFERENCE_list.txt
  rm $List


#############################################################################
# END:
# SCRIPT: 08_Mapping_ReferenceGenomes.sh
#############################################################################
