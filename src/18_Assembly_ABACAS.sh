#!/bin/bash

# -----------------------------------------------------------------------------
# SCRIPT:       18_Assembly_ABACAS.sh
# Written by:   Cieza RJ
# PURPOSE:      Order contigs against a reference genome
# -----------------------------------------------------------------------------

# ABACAS (v 1.3.1):
  # Ordering contigs against a reference genome
  # http://abacas.sourceforge.net/

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  |   └── ABACAS
#  └── projects
#      └── ("PROJECT_name")
#          ├── data
#          ├── data_PROCESSED
#          ├── results
#          |   ├── ABACAS_working                  -- DIR removed when done --
#          |   ├── Contigs_Filtered                -- INPUT --
#          |   └── Contigs_Ordered                 -- OUTPUT --
#          |       ├── contigs_crunch
#          |       ├── contigs_tab
#          |       └── contigs_unused
#          ├── reference
#          |   └── Reference_List_ABACAS.txt       -- FILE removed when done --
#          └── src



###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# Activate Python 3 ENVIRONMENT

  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate mypython3

# (01.2)
# -----------------------------------------------------------------------------
# EXECUTABLES

	# (a) Define PATH for ABACAS executable
	  run_ABACAS=../../../bin/ABACAS/abacas.1.3.1.pl


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ SOFTWARE verification] "
  echo " ###############################################################"
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE

  # (a) ABACAS should have been installed into DIR [ bin ] (view README)
  # Print ABACAS usage
    perl $run_ABACAS -h | head -n 10

  # (b) Mummer should have been installed through Conda (view README)
  # Check Mummer usage
    echo " Mummer Usage "
    mummer -help | head -n 1


###############################################################################
# (03) Download REFERENCE GENOME
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ STARTING REFERENCE GENOME download ] "
  echo " ############################################################### "
  echo " "

# (03.2)
# -----------------------------------------------------------------------------
# REFERENCE List TXT file:
# - Reference_List_ABACAS.txt
# - Reference_List_ABACAS.txt should be in DIR [ reference ]

# (a) REFERENCE list VARIABLE (TXT file)
  List=../reference/Reference_List_ABACAS.txt

# (b) Detecting the presence or absence of REFERENCE list
  if [ -e "$List" ];
  then
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE LIST exists - PROCEED ] "
    echo " "
  else
    echo " "
    echo " [ REFERENCE LIST does NOT exist - ABORT ] "
    echo " "
  fi

# (c) Read TXT file and skip lines starting with #:
  grep -o '^[^#]*' $List > REFERENCE_list.txt

# (03.3)
# -----------------------------------------------------------------------------
# Download & process REFERENCE GENOME

# Read TXT file
# 1st column:   common_name
# 2nd column:   format
# 3rd column:   FTP_site
  while read common_name format FTP_site
  do

  # (a) Skip the header
    [ "$common_name" == "common_name" ] && continue

  # (b) Download REFERENCE from NCBI
  # Begin task message
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE download BEGINS ] [ REFERENCE name: $common_name ] "
    echo " "
  # Download genome
	  wget $FTP_site
  # End task message
	  echo " [ REFERENCE download COMPLETE ] [ REFERENCE name: $common_name ] "

  # (c) DIR where downloaded REFERENCE will be deposited
  # OUTPUT PATH
    OUT_REF_PATH=../reference/$common_name
  # Make DIR
    mkdir -p $OUT_REF_PATH

  # (d) Unzip GZ file
  # INPUT file
    REF_01=$(echo *.gz)
  # Unzip
    gunzip $REF_01

  # (e) Move unzipped file to DIR [ reference ]
  # INPUT file
    REF_02=$(echo *.$format)
  # Move file
    mv $REF_02 \
       $OUT_REF_PATH

  # (f) REFERENCE variable at DIR [reference]
    REF_03=$(echo $OUT_REF_PATH/*.$format)

  # (g) Copy REFERENCE to workind DIR [src]
    cp $REF_03 .

  # (h) Rename REFERENCE using VARIABLE '$common_name'
  # FNA ending required
    mv ./$REF_02 \
       $common_name\.fna

  # (i) Remove accesory files created
    rm $REF_03

  # (j) Remove accesory DIR created
  # REFERENCE PATH
    REF_DIR=$(echo $OUT_REF_PATH)
    rm -R $REF_DIR

  # (k) Loop completion message
   	echo " [ REFERENCE processing COMPLETE ] [ REFERENCE name: $common_name ]"
   	echo " "
   	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "

  done < REFERENCE_list.txt

# (03.4)
# -----------------------------------------------------------------------------
# Remove modified List generated

  # Temporary TXT file generated
    rm REFERENCE_list.txt


###############################################################################
# (04) Run ABACAS
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ############################################################### "
  echo " [ Ordering contigs with ABACAS ] "
  echo " ############################################################### "
  echo " "

# (04.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR [ data_PROCESSED ]

	# DIR [ data_PROCESSED ] contains FASTQ paired end (PE) reads
	# Make a list of unique names in the DIR
	# Each SAMPLE has:
	  	# Forward (*_R1.fastq.gz)
	  	# Reverse (*_R2.fastq.gz)
	# find:   Find SAMPLES in DIR [ data_PROCESSED ]
	# -name:  List unique SAMPLE names
	# sed:    Remove the ending of file names only keeping the unique name
	# sort:   Sort by unique name
		SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
			  					sed 's/\_R[1-2].fastq.gz//' | \
			  					sort -u)

# (04.3)
# -----------------------------------------------------------------------------
# REFERENCE GENOME to input it under Flag -r in ABACAS

  # (a) PATH to REFERENCE GENOME
    REF_ABACAS=$(echo *.fna)

  # (b) Count if there is more than 1 CHROMOSOME or if PLASMIDS are present
    chr_NUMBER=$(grep -c ">" $REF_ABACAS)

  # (c) Merge CHROMOSOME and PLASMIDS into single FASTA file if necessary
  echo " [ REFERENCE check ] "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    if [ $chr_NUMBER -gt 1 ]
    then
      echo " "
      echo " --- FASTA file has more than 1 CHROMOSOME or PLASMIDS --- "
      echo " "
      # Remove plasmid HEADERS
      grep -v 'plasmid' $REF_ABACAS > temp.fna
      rm $REF_ABACAS
      mv temp.fna $REF_ABACAS
      echo " --- REFERENCE processed into single FASTA file --- "
      echo " "
    else
      echo " "
      echo " --- FASTA file has 1 CHROMOSOME --- "
      echo " --- PROCEED --- "
      echo " "
    fi

# (04.4)
# -----------------------------------------------------------------------------
# OUTPUT

  # (a) Working DIR to deposit temporary results
  # PATH & Make DIR
    OUT_DIR_work=../results/ABACAS_working
    mkdir -p $OUT_DIR_work

	# (b) DIR to deposit ordered contigs
	# PATH & Make DIR
		OUT_DIR=../results/Contigs_Ordered
    mkdir -p $OUT_DIR
    mkdir -p $OUT_DIR/contigs_crunch
    mkdir -p $OUT_DIR/contigs_tab
    mkdir -p $OUT_DIR/contigs_unused

# (04.5)
# -----------------------------------------------------------------------------
# Order contigs with ABACAS

# For Loop
  for sample in $SAMPLE_NAME
  do

	# (a) DIR name
	# Contains only the SAMPLE name with no PATH
	  DIRNAME=${sample##*/}

  # (b) INPUT file
  # Contigs - Assembly file (Filtered)
  	INP_contigs=../results/Contigs_Filtered/$DIRNAME\_HCov.contigs.fasta

  # (c) OUTPUT file
  # Contigs ordered
    OUT_contigs=$OUT_DIR_work/$DIRNAME\_contigs_ordered

  # (d) Run SOFTWARE
  # SOFTWARE: ABACAS
  # USAGE:
  # -r:     REFERENCE sequence in a single fasta file
  # -q:     Contigs in a multi-fasta format
  # -p:     MUMmer program to use: 'nucmer' or 'promer'
  # -d:     Increase mapping sensitivity
  # -s:     Minimum length of exact matching word (nucmer default)
  # -b:     Contigs that are not used in generating the pseudomolecule will be
  #         placed in a '.bin' file
  # -a:     Append contigs that re not used to the end of the pseudomolecule
  # -o:     Output files will have this prefix
    perl $run_ABACAS -r $REF_ABACAS \
                     -q $INP_contigs \
                     -p nucmer \
                     -d \
                     -s \
                     -b \
                     -a \
                     -o $OUT_contigs

  # (e) Append SAMPLE NAME to OUTPUT files
  # Rename 'unused_contigs.out' file
  # Contigs with mapping information but could not be used in the ordering
    mv unused_contigs.out \
       $DIRNAME\_unused_contigs.out

  # (f) Move OUTPUT files to final DIR [ Contigs_Ordered ]
  # Ordered contigs
    mv $OUT_contigs.fasta \
       $OUT_DIR
  # Crunch files
    mv $OUT_DIR_work/$DIRNAME\_contigs_ordered.crunch \
       $OUT_DIR/contigs_crunch
  # ACT feature file
    mv $OUT_DIR_work/$DIRNAME\_contigs_ordered.tab \
       $OUT_DIR/contigs_tab
  # Unused contigs
    mv $DIRNAME\_unused_contigs.out \
       $OUT_DIR/contigs_unused

  # (g) Remove accesory files generated
    rm -r $OUT_DIR_work/*
    rm nucmer.delta
    rm nucmer.filtered.delta
    rm nucmer.tiling

  # (h) COMPLETION message
    echo " [ Ordering contigs with ABACAS complete ] "
    echo " [ SAMPLE name: $DIRNAME ] "
    echo " "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "

  done

# (04.6)
# -----------------------------------------------------------------------------
# Remove accesory files generated

	# (a) REFERENCE FNA
		rm $REF_ABACAS
  # (b) REFERENCE list VARIABLE (TXT file)
    rm $List
  # (c) Working DIR [ ABACAS_working ]
    rm -r $OUT_DIR_work


###############################################################################
# (06) Return to base environment
###############################################################################

# (06.1)
# -----------------------------------------------------------------------------
# Return to the conda base ENVIRONMENT

  conda activate


###############################################################################
# END:
# SCRIPT: 18_Assembly_ABACAS.sh
###############################################################################
