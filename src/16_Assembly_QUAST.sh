#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       16_Assembly_QUAST.sh
# Written by:   Cieza RJ
# PURPOSE:      Evaluate genome assemblies with QUAST
# ---------------------------------------------------------------------------

# QUAST (v 5.0.2)
  # Quality Assessment Tool for Genome Assemblies
  # http://quast.bioinf.spbau.ru/manual.html#sec1

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       |   ├── Contigs_Filtered     -- INPUT --
#  |       |   ├── QUAST_working        -- DIR removed upon completion --
#  |       |   └── QUAST_reports        -- OUTPUT --
#  |       |       ├── HTML_reports
#  |       |       └── Icarus
#  |       └── src
#  └── reference
#      └── Reference_List_QUAST.txt     -- FILE removed upon completion --


###############################################################################
# (00) Supply ARGUMENTS to SCRIPT from the command line
###############################################################################

# (00.1)
# -----------------------------------------------------------------------------
# Determine the number of processors used in the SCRIPT.

  # (a) Default VALUE
  # If no value is provided, 4 processors will be used
    PROCESSORS=4

# (00.2)
# -----------------------------------------------------------------------------
# User-supplied values (will overwrite default)

# ARGUMENTS will provided through the command Line
# [-p]
  while [[ "$1" == -* ]];
  do
    case $1 in
      # (a) PROCESSORS option
      # - - - - - - - - - - - - - - - -
      # The argument to the OPTION is placed in the variable $1
      -p|--processsors)
        shift
        PROCESSORS="$1"
        ;;
      # (b) END of arguments
      # - - - - - - - - - - - - - - - -
      --)
        shift
        break
        ;;
      # FINISHING the loop
      esac
      shift
    done

# (00.3)
# -----------------------------------------------------------------------------
# Log message

  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "
  echo " [ PROCESSORS used ] [ - $PROCESSORS - ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# Activate Python 3 ENVIRONMENT

  # (a) PATH to DIR where "conda.sh" is located
    . ~/miniconda3/etc/profile.d/conda.sh
  # (b) conda activate command followed by your ENVIRONMENT name
    conda activate mypython3


###############################################################################
# (02) Verify SOFTWARE
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ SOFTWARE verification] "
  echo " ###############################################################"
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Verify SOFTWARE

  # QUAST should have been installed through Conda (view README)
  # Check QUAST version
    quast.py --version


###############################################################################
# (03) Download REFERENCE GENOME
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ STARTING REFERENCE GENOME download ] "
  echo " ############################################################### "
  echo " "

# (03.2)
# -----------------------------------------------------------------------------
# REFERENCE List TXT file:
# - Reference_List_QUAST.txt
# - Reference_List_QUAST.txt should be in DIR [ reference ]

# (a) REFERENCE list VARIABLE (TXT file)
  List=../../../reference/Reference_List_QUAST.txt

# (b) Detecting the presence or absence of REFERENCE list
  if [ -e "$List" ];
  then
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE LIST exists - PROCEED ] "
    echo " "
  else
    echo " "
    echo " [ REFERENCE LIST does NOT exist - ABORT ] "
    echo " "
  fi

# (c) Read TXT file and skip lines starting with #:
  grep -o '^[^#]*' $List > REFERENCE_list.txt

# (03.3)
# -----------------------------------------------------------------------------
# Download & process REFERENCE GENOME

# Read TXT file
# 1st column:   common_name
# 2nd column:   format
# 3rd column:   FTP_site
  while read common_name format FTP_site
  do

  # (a) Skip the header
    [ "$common_name" == "common_name" ] && continue

  # (b) Download REFERENCE from NCBI
  # Begin task message
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE download BEGINS ] [ REFERENCE name: $common_name ] "
    echo " "
  # Download genome
	  wget $FTP_site
  # End task message
	  echo " [ REFERENCE download COMPLETE ] [ REFERENCE name: $common_name ] "

  # (c) DIR where downloaded REFERENCE will be deposited
  # OUTPUT PATH
    OUT_REF_PATH=../../../reference/$common_name
  # Make DIR
    mkdir -p $OUT_REF_PATH

  # (d) Unzip GZ file
  # INPUT file
    REF_01=$(echo *.gz)
  # Unzip
    gunzip $REF_01

  # (e) Move unzipped file to DIR [ reference ]
  # INPUT file
    REF_02=$(echo *.$format)
  # Move file
    mv $REF_02 \
       $OUT_REF_PATH

  # (f) REFERENCE variable at DIR [reference]
    REF_03=$(echo $OUT_REF_PATH/*.$format)

  # (g) Copy REFERENCE to workind DIR [src]
    cp $REF_03 .

  # (h) Rename REFERENCE using VARIABLE '$common_name'
  # Change FNA extension for FASTA
  # FASTA ending required, otherwise SPANDx won't read it
    mv ./$REF_02 \
       $common_name\.fna

  # (i) Remove accesory files created
    rm $REF_03

  # (j) Remove accesory DIR created
  # REFERENCE PATH
    REF_DIR=$(echo $OUT_REF_PATH)
    rm -R $REF_DIR

  # (k) Loop completion message
   	echo " [ REFERENCE processing COMPLETE ] [ REFERENCE name: $common_name ]"
   	echo " "
   	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "

  done < REFERENCE_list.txt

# (03.4)
# -----------------------------------------------------------------------------
# Remove modified List generated

  rm REFERENCE_list.txt


###############################################################################
# (04) Run QUAST
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ Generating assembly reports with QUAST ] "
  echo " ###############################################################"
  echo " "

# (04.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR "data_PROCESSED"

	# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
	# Make a list of unique names in the DIR (Keep only the sample name)
	# Each sample has:
	  	# Forward (*_R1.fastq.gz)
	  	# Reverse (*_R2.fastq.gz)
	# find:   Find samples in DIR "data_PROCESSED"
	# -name:  List unique names
	# sed:    Remove the ending of file names only keeping the unique name
	# sort:   Sort by unique name
		SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
			  					sed 's/\_R[1-2].fastq.gz//' | \
			  					sort -u)

# (04.3)
# -----------------------------------------------------------------------------
# REFERENCE GENOME to input it under Flag -r in QUAST

  # PATH to REFERENCE GENOME
    REF_QUAST=$(echo *.fna)

# (04.4)
# -----------------------------------------------------------------------------
# OUTPUTS

	# (a) Define OUTPUT
	# OUTPUT PATH
		OUT_DIR=../results/QUAST_working
    OUT_DIR_report=../results/QUAST_reports
	# Make DIR
		mkdir -p $OUT_DIR
    mkdir -p $OUT_DIR_report
    mkdir -p $OUT_DIR_report/HTML_reports
    mkdir -p $OUT_DIR_report/Icarus

# (04.5)
# -----------------------------------------------------------------------------
# Generate report

# For Loop
  for sample in $SAMPLE_NAME
  do

	# (a) Define DIRNAME
	# Contains only the SAMPLE name with no PATH
	  DIRNAME=${sample##*/}

  # (b) Define INPUT
  # Assembly file
  # Used to be able to calculate sequence coverage
  	contigs=../results/Contigs_Filtered/$DIRNAME\_HCov.contigs.fasta

  # (c) START message
    echo " "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " [ Begin running QUAST ] [ SAMPLE name: $DIRNAME ] "
    echo " [ Contigs: ] [ $contigs ] "
    echo " [ REFERENCE: ] [ $REF_QUAST ]"

  # (d) Run SOFTWARE
  # SOFTWARE: QUAST
  # USAGE:
  # -o:     Output directory
  # -r:     Reference genome file FASTA
  # -t:     Number of threads (Adjust accordingly) / Run on High-Performance Computer (HPC) server
  # -l:     Names of assemblies to use in reports
    quast.py -o $OUT_DIR \
             -r $REF_QUAST \
             -t $PROCESSORS \
             -l $DIRNAME \
             $contigs

  # (e) Append SAMPLE NAME to QUAST output files
  # Rename 'report.tsv' file (Summary Table)
    mv $OUT_DIR/report.tsv \
       $OUT_DIR/$DIRNAME\_report.tsv
  # Rename 'report.html' file (Everything in an interactive HTML file)
    mv $OUT_DIR/report.html \
       $OUT_DIR/$DIRNAME\_report.html
  # Rename 'icarus.html' files
  # Interactive viewers
    mv $OUT_DIR/icarus_viewers/alignment_viewer.html \
       $OUT_DIR/icarus_viewers/$DIRNAME\_alignment_viewer.html
    mv $OUT_DIR/icarus_viewers/contig_size_viewer.html \
       $OUT_DIR/icarus_viewers/$DIRNAME\_contig_size_viewer.html

  # (f) Move Report to final DIR [ QUAST_reports ]
  # report.tsv
    mv $OUT_DIR/$DIRNAME\_report.tsv \
       $OUT_DIR_report
  # report.html
    mv $OUT_DIR/$DIRNAME\_report.html \
       $OUT_DIR_report/HTML_reports
  # Icarus report
    mv $OUT_DIR/icarus_viewers/$DIRNAME\_alignment_viewer.html \
       $OUT_DIR_report/Icarus
    mv $OUT_DIR/icarus_viewers/$DIRNAME\_contig_size_viewer.html \
       $OUT_DIR_report/Icarus

  # (g) Remove accesory files generated
    rm -r $OUT_DIR/*

  # (h) COMPLETION message
    echo " [ Finished running QUAST ] [ SAMPLE name: $DIRNAME ] "
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "

done

# (04.6)
# -----------------------------------------------------------------------------
# Remove accesory files generated

	# (a) REFERENCE FNA
		rm $REF_QUAST
  # (b) REFERENCE list VARIABLE (TXT file)
    rm $List
  # (c) Working DIR [ QUAST_working ]
    rm -r $OUT_DIR


#############################################################################
# (05) Return to base environment
#############################################################################

# (05.1)
# Return to the conda base ENVIRONMENT
  conda activate


###############################################################################
# END:
# SCRIPT: 16_Assembly_QUAST.sh
###############################################################################
