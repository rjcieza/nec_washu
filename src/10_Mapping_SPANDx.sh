#!/bin/bash

# ---------------------------------------------------------------------------
# SCRIPT:       10_Mapping_SPANDx.sh
# Written by:   Cieza RJ
# PURPOSE:      Compile SPANDx output in DIR [ results ]
# ---------------------------------------------------------------------------

# SPANDx:
  # Comparative genomics pipeline for microbes
  # Identify SNP and indel variants in haploid genomes
  # SPANDx combines: [BWA, SAMtools, BEDtools, Picard, GATK, VCFtools and SnpEff]
  # https://github.com/dsarov/SPANDx

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       |   └── SPANDx
#  |       |       ├── BEDcov
#  |       |       └── Outputs
#  |       |           ├── Comparative
#  |       |           └── SNPs_indels_PASS
#  |       └── src                     -- SPANDx FILE removed upon completion --
#  └── reference
#      └── Reference_List_SPANDx.txt   -- FILE removed upon completion --


###############################################################################
# (01) Get REFERENCE GENOME name
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ Getting REFERENCE GENOME name ] "
  echo " ############################################################### "
  echo " "

# (01.2)
# -----------------------------------------------------------------------------
# REFERENCE List TXT file:
# - Reference_List_SPANDx.txt
# - Reference_List_SPANDx.txt should be in DIR [ reference ]

# (a) REFERENCE list VARIABLE (TXT file)
  List=../../../reference/Reference_List_SPANDx.txt

# (b) Detecting the presence or absence of REFERENCE list
  if [ -e "$List" ]; then
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE LIST exists - PROCEED ] "
    echo " "
  else
    echo " "
    echo " [ REFERENCE LIST does NOT exist - ABORT ] "
    echo " "
  fi

# (c) Read TXT file and skip lines starting with #:
  grep -o '^[^#]*' $List > REFERENCE_list.txt

# (01.3)
# -----------------------------------------------------------------------------
# Download & process REFERENCE GENOME

# Read TXT file
# 1st column:   common_name
# 2nd column:   format
# 3rd column:   FTP_site
  while read common_name format FTP_site
  do

  # (a) Skip the header
    [ "$common_name" == "common_name" ] && continue

  # (b) Download REFERENCE from NCBI
  # Begin task message
    echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
    echo " "
    echo " [ REFERENCE name: $common_name ] "
    echo " "
  	echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "

  # (c) VARIABLE for removal
    REFERENCE_VARIABLE=$(echo $common_name)

  done < REFERENCE_list.txt
  # Remove modified List generated
  rm REFERENCE_list.txt


###############################################################################
# (02) Format SPANDx results to generate CSV files
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ Formatting SPANDx results to generate CSV ] "
  echo " ############################################################### "
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Transfer SPANDx OUTPUT into DIR [ results/SPANDx ]

# (a) Define DIR [ SPANDx ] PATH
  results_SPANDx=../results/SPANDx

# (b) Make DIR
  mkdir -p $results_SPANDx

# (c) Remove DIR with SNPs that failed to pass filters
  rm -r Outputs/SNPs_indels_FAIL

# (d) OUTPUTS to transfer
# - All indels annotated
# - All SNPs annottated
# - Bedcov merge
# - Matrix generated
# - Single sample summary
  mv Outputs/ \
     $results_SPANDx
# - BEDcov files
  mv BEDcov/ \
     $results_SPANDx

# (02.3)
# -----------------------------------------------------------------------------
# Convert to CSV files

# (a) Define INPUT
# Sample summary
 Summary=$results_SPANDx/Outputs/Single_sample_summary.txt
# BED coverage results
 BEDcoverage=$results_SPANDx/Outputs/Comparative/Bedcov_merge.txt
# All SNPs annotated
 SNP_annotated=$results_SPANDx/Outputs/Comparative/All_SNPs_annotated.txt
# All INDEL annotated
 Indel_annotated=$results_SPANDx/Outputs/Comparative/All_indels_annotated.txt

# (b) Define OUTPUT CSV files
# Sample summary
 Summary_csv=$results_SPANDx/Outputs/Single_sample_summary.csv
# BED coverage results
 BEDcoverage_csv=$results_SPANDx/Outputs/Comparative/Bedcov_merge.csv
# All SNPs annotated
 SNP_annotated_csv=$results_SPANDx/Outputs/Comparative/All_SNPs_annotated.csv
# All INDEL annotated
 Indel_annotated_csv=$results_SPANDx/Outputs/Comparative/All_indels_annotated.csv

# (c) Create CSV files
# Sample summary
 cat $Summary | tr "\t" "," > $Summary_csv
# BED coverage results
 cat $BEDcoverage | tr "\t" "," > $BEDcoverage_csv
# All SNPs annotated
 cat $SNP_annotated | tr "\t" "," > $SNP_annotated_csv
# All INDEL annotated
 cat $Indel_annotated | tr "\t" "," > $Indel_annotated_csv

# (d) Loop completion message
 echo " [ Fomatting COMPLETE ] "
 echo " "


###############################################################################
# (03) Cleaning SPANDx generated files
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ Cleaning SPANDx generated files ] "
  echo " ############################################################### "
  echo " "

# (03.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR "data_PROCESSED"

# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of unique names in the DIR (Keep only the sample name)
# Each sample has:
  	# Forward (*_R1.fastq.gz)
  	# Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
		  					sed 's/\_R[1-2].fastq.gz//' | \
		  					sort -u)

# (03.3)
# -----------------------------------------------------------------------------
# Remove sample data generated by SPANDx in [ src ]

# For Loop
  for sample in $SAMPLE_NAME
  do
  # (a) Define DIRNAME
  # Contains only the SAMPLE name with no PATH
    DIRNAME=${sample##*/}

  # (b) Define INPUT
	# Forward FASTQ reads
		rm $DIRNAME\_1_sequence.fastq.gz
    rm $DIRNAME\_2_sequence.fastq.gz
  # Remove DIR created for each sample - within DIR [ src ]
    rm -r $DIRNAME/

  # (c) Loop completion message
    echo " [ Removal COMPLETE ] [ $DIRNAME ] "
    echo " "
  done

# (03.4)
# -----------------------------------------------------------------------------
# Further removal of DIR created by SPANDx

# (a) Remove DIR created by SPANDx
  rm -R tmp/
  rm -R logs/
  rm -R Phylo/

# (b) Remove DIR created by SPANDx
# rm clean_vcf_id.txt
# rm mastervcf_id.txt
# rm matrix_id.txt
# rm qsub_array_ids.txt
# rm qsub_BED_id.txt
# rm qsub_ids.txt

# (c) REFERENCE list VARIABLE (TXT file)
  List=../../../reference/Reference_List_SPANDx.txt
  rm $List
# Remove REFERENCE index files
  rm $REFERENCE_VARIABLE.*
# User message
  echo " [ Removal of REFERENCE INDEXING files COMPLETE ] "
  echo " "


###############################################################################
# (04) Remove FASTQ files from REFERENCE GENOMES
###############################################################################

# (04.1)
# -----------------------------------------------------------------------------
# USER message
  echo " "
  echo " ############################################################### "
  echo " [ Removing REFERENCE GENOMES FASTQ files ] "
  echo " ############################################################### "
  echo " "

# (04.2)
# -----------------------------------------------------------------------------
# Downloaded REFERENCE GENOMES FASTQ files are not needed after running SPANDx

# (a) Define INPUT - REFERENCE GENOMES
# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of REFERENCE GENOMES in the DIR (Keep only the sample name)
# Each sample has:
  	# Forward (*_R1.fastq.gz)
  	# Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	REFERENCE_NAME=$(find ../data_PROCESSED/ -name 'Ecoli*.fastq.gz' | \
		  					   sed 's/\_R[1-2].fastq.gz//' | \
		  					   sort -u)

# (b) Remove REFERENCE GENOMES FASTQ files
# For Loop
  for sample in $REFERENCE_NAME
  do
  # Define DIRNAME
 	# Contains only the SAMPLE name with no PATH
 	  DIRNAME=${sample##*/}

  # Remove FASTQ files
    rm ../data_PROCESSED/$DIRNAME*.fastq.gz

  # Loop Completion message
    echo " "
    echo " [ REFERENCE GENOME FASTQ removal COMPLETE ] [ $DIRNAME ] "
   done

# (c) USER message
  echo " "
  echo " [ Removal of REFERENCE GENOMES COMPLETE ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "

# (04.3)
# -----------------------------------------------------------------------------
# Downloaded OUTGROUP GENOMES FASTQ files are not needed after running SPANDx

# (a) Define INPUT - OUTGROUP GENOME
# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
# Make a list of OUTGROUP GENOMES in the DIR (Keep only the sample name)
# Each sample has:
 	# Forward (*_R1.fastq.gz)
 	# Reverse (*_R2.fastq.gz)
# find:   Find samples in DIR "data_PROCESSED"
# -name:  List unique names
# sed:    Remove the ending of file names only keeping the unique name
# sort:   Sort by unique name
	OUTGROUP_NAME=$(find ../data_PROCESSED/ -name 'Ealbertii*.fastq.gz' | \
		  					  sed 's/\_R[1-2].fastq.gz//' | \
		  					  sort -u)

# (b) Remove OUTGROUP GENOMES FASTQ files
# For Loop
  for sample in $OUTGROUP_NAME
  do
  # Define DIRNAME
 	# Contains only the SAMPLE name with no PATH
 	  DIRNAME=${sample##*/}

  # Remove FASTQ file
    rm ../data_PROCESSED/$DIRNAME*.fastq.gz

  # Loop Completion message
    echo " "
    echo " [ OUTGROUP GENOME FASTQ removal COMPLETE ] [ $DIRNAME ] "
   done

# (c) USER message
  echo " "
  echo " [ Removal of OUTGROUP GENOMES COMPLETE ] "
  echo " "
  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
  echo " "


############################################################################
# END:
# SCRIPT: 10_Mapping_SPANDx.sh
############################################################################
