#!/bin/bash

### -------------------------------------------------------------------------
### SCRIPT:       Slurm_submission.sh
### Written by:   Roberto J. Cieza
### PURPOSE:      Submit SBATCH jobs with arguments from the command line
## -------------------------------------------------------------------------


###############################################################################
# (01) ARGUMENTS to be passed to SBATCH
###############################################################################

# ----------------- Command line arguments -------------------------------------

# Determine the ARGUMENTS that will be provided to SBATCH
# [-n]  JOBNAME or SCRIPT NAME
# [-u]  UNIQNAME
# [-p]  PROCESSORS
# [-t]  TIME in HOURS
  while [[ "$1" == -* ]];
  do

    case $1 in
    # JOBNAME option
    # The argument to the OPTION is placed in the variable $1
      -n|--jobname)
        shift
        JOBNAME="$1"
        ;;

    # UNIQNAME option
    # The argument to the OPTION is placed in the variable $1
      -u|--uniqname)
        shift
        UNIQNAME="$1"
        ;;

    # PROCESSORS option
    # The argument to the OPTION is placed in the variable $1
      -p|--processors)
        shift
        PROCESSORS="$1"
        ;;

    # TIME option
    # The argument to the OPTION is placed in the variable $1
      -t|--time)
        shift
        TIME="$1"
        ;;

    # END of arguments
      --)
        shift
        break
        ;;
    esac

    # FINISHING the loop
    shift
  done

# ----------------- Detect PATH  ----------------------------------------------

# Detect present working DIR
  WORKDIR_01=$(pwd)
  WORKDIR_02=$(echo $WORKDIR_01)

# ----------------- SLURM VARIABLES -------------------------------------------

# Variables
  SLURM_JOB_NAME=$(echo \$SLURM_JOB_NAME)
  SLURM_JOB_ID=$(echo \$SLURM_JOB_ID)
  SLURM_JOB_NUM_NODES=$(echo \$SLURM_JOB_NUM_NODES)
  SLURM_JOB_NODELIST=$(echo \$SLURM_JOB_NODELIST)
  SLURM_NTASKS=$(echo \$SLURM_NTASKS)
  SLURM_CPUS_PER_TASK=$(echo \$SLURM_CPUS_PER_TASK)


###############################################################################
# (02) Verify that ALL OPTIONS received an aARGUMENT
###############################################################################

# Message summarizing parameters to be used to submit job
  echo " "
  echo " ** Below are shown the PARAMETERS used to submit the JOB ** "

# (02.1)
# Exit if JOBNAME is not specified
  if [[ -z "$JOBNAME" ]];
  then
    echo " "
    echo " ##################   ERROR   ############################# "
    echo " JOBNAME not specified. Provide [-n] " >&2
    echo " JOBNAME MUST be the SCRIPT name "
    echo " "
    exit 1
  else
    echo " "
    echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
    echo "1) JOBNAME:                     [ $JOBNAME ]"
  fi

# (02.2)
# Exit if UNIQNAME is not specified
  if [[ -z "$UNIQNAME" ]];
  then
    echo " "
    echo " ##################   ERROR   ############################# "
    echo " UNIQNAME not specified. Provide [-u] " >&2
    echo " UNIQNAME will be appended to @med.umich.edu "
    echo " "
    exit 1
  else
    echo "2) UNIQNAME:                    [ $UNIQNAME ]"
    echo "3) UNIQNAME mail:               [ $UNIQNAME@med.umich.edu ]"
  fi

# (02.3)
# Exit if PROCESSORS is not a number
  if [[ "$PROCESSORS" -le 0 ]];
  then
    echo " "
    echo " ##################   ERROR   ############################# "
    echo " PROCESSORS must be a positive integer. Provide [-p] " >&2
    echo " "
    exit 1
  else
    echo "4) PROCESSORS used in 1 node:   [ $PROCESSORS ]"
  fi

# (02.4)
# Exit if TIME is not a number
  if [[ "$TIME" -le 0 ]];
  then
    echo " "
    echo " ##################   ERROR   ############################# "
    echo " TIME must be a positive integer. Provide [-t] " >&2
    echo " Provide TIME in HOURS "
    echo " "
    exit 1
  else
    echo "5) TIME in hours:               [ $TIME ]"
  fi


# (02.4)
# Print full PATH to SCRIPT and SCRIPT name:
  echo "6) PATH to SCRIPT:              [ $WORKDIR_02 ]"
  echo "7) SCRIPT name:                 [ $JOBNAME.sh ]"
  echo " - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
  echo " "


###############################################################################
## (03) START of #SBATCH directives
###############################################################################

# Begin message
  echo " "
  echo " ** Below is shown your SBATCH script ** "
  echo " ** This is how the SBATCH script will be submitted: ** "
  echo " ############################################################# "
  echo " "

# Redirect input into an interactive shell script or program (sbatch)
cat << EOF > SUBMISSION_JOB_$JOBNAME
#!/bin/bash
# The interpreter used to execute the script (bash)

# ------------------------------------------------------------------------
# PART 1
# START of #SBATCH directives

#SBATCH --job-name=$JOBNAME
#SBATCH --account=youngvi1
#SBATCH --mail-user=$UNIQNAME@med.umich.edu
#SBATCH --mail-type=BEGIN,END
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=$PROCESSORS
#SBATCH --mem-per-cpu=10000m
#SBATCH --time=$TIME:00:00
#SBATCH --partition=standard
#SBATCH --export=ALL
#SBATCH --output=LOG-%u-%j.log

# ------------------------------------------------------------------------
# PART 2
# Display job information

echo " "
echo " Job identifiers are: "
echo " -------------------- "
echo " Job NAME:            $SLURM_JOB_NAME "
echo " Job ID:              $SLURM_JOB_ID "
echo " PATH to script:      $WORKDIR_02 "
echo " SCRIPT:              $JOBNAME.sh "
echo " DATE:                $(date)"
echo " NODES allocated:     $SLURM_JOB_NUM_NODES "
echo " List of NODES:       $SLURM_JOB_NODELIST "
echo " TASKS per NODE:      $SLURM_NTASKS "
echo " CPUs per TASK:       $SLURM_CPUS_PER_TASK "
echo " "
echo " Submission parameters: "
echo " -------------------- "
head -n 18 SUBMISSION_JOB_$JOBNAME | tail -n 11

# ------------------------------------------------------------------------
# PART 3
# TASK to be submitted to Slurm [ SCRIPT ]

# Set environmental VAR to go through proxy server to handle request
# from the SRA toolkit (computer nodes do not have direct access to the internet)
source /etc/profile.d/http_proxy.sh

echo " "
echo " Launching: "
echo " -------------------- "
echo " $JOBNAME.sh "
sh $WORKDIR_02/$JOBNAME.sh \
-p $PROCESSORS

# ------------------------------------------------------------------------
# PART 4
# Clean SUBMISSION SCRIPT generated
rm SUBMISSION_JOB_$JOBNAME

echo " "
echo " JOB submitted to SLURM COMPLETED "
echo " "

# ------------------------------------------------------------------------
EOF

# Print SBATCH script to STDOUT
  cat SUBMISSION_JOB_$JOBNAME

# End message
  echo " "
  echo " ** END ** "
  echo " ############################################################# "
  echo " "

###############################################################################
## (04) START of #SBATCH directives
###############################################################################

# (04.1)
# Prompt user INPUT to proceed to SBATCH submission
  while true;
  do

  # YES - NO prompt message
    echo " "
    echo " ############################################################# "
    echo " "
    echo " Please review the SBATCH directives above "
    read -p " Is this JOB ready to submit to SLURM [y/n]? " yn

    case $yn in
    [Yy]* )

    # Submit script with SBATCH command
      sbatch SUBMISSION_JOB_$JOBNAME
      echo " "
      echo " [ JOB was submitted ] "
      echo " "
      echo " ############################################################# "
      echo " "

    # Otherwise exit
  	  break;;

    [Nn]* ) exit;;
    esac
  done


###############################################################################
# END:
# SCRIPT: Slurm_submission.sh
###############################################################################
