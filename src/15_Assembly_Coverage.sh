#!/bin/bash

# -----------------------------------------------------------------------------
# SCRIPT:       15_Assembly_Coverage.sh
# Written by:   Cieza RJ
# PURPOSE:      Calculate actual sequencing depth/coverage
# -----------------------------------------------------------------------------

# Fastq-info
	# Bash script to generate information for 1 or 2 fastq files (PE Illumina data)
	# https://github.com/raymondkiu/fastq-info

# DIR tree
	# Script DIR structure
	# Script should be run from the DIR src within a PROJECT
	# DIR structure shown includes temporary DIR generated with the script

#  .
#  ├── bin
#  |   └── fastq-info
#  ├── projects
#  |   └── ("PROJECT_name")
#  |       ├── data
#  |       ├── data_PROCESSED
#  |       ├── results
#  |       |   ├── Coverage
#  |       |   └── Contigs_Filtered
#  |       └── src
#  └── reference


###############################################################################
# (01) Set up ENVIRONMENT
###############################################################################

# (01.1)
# -----------------------------------------------------------------------------
# EXECUTABLES

	# (a) Define PATH for fastq-info executable
		run_Fastqinfo=../../../bin/fastq-info/./fastq_info_3.sh


###############################################################################
# (02) Run Fasq-info
###############################################################################

# (02.1)
# -----------------------------------------------------------------------------
# USER message

  echo " "
  echo " ###############################################################"
  echo " [ Generating sequencing depth/coverage report ] "
  echo " ###############################################################"
  echo " "

# (02.2)
# -----------------------------------------------------------------------------
# Generate a list of the samples present in DIR "data_PROCESSED"

	# DIR "data_PROCESSED" contains FASTQ paired end (PE) reads
	# Make a list of unique names in the DIR (Keep only the sample name)
	# Each sample has:
	  	# Forward (*_R1.fastq.gz)
	  	# Reverse (*_R2.fastq.gz)
	# find:   Find samples in DIR "data_PROCESSED"
	# -name:  List unique names
	# sed:    Remove the ending of file names only keeping the unique name
	# sort:   Sort by unique name
		SAMPLE_NAME=$(find ../data_PROCESSED/ -name '*.fastq.gz' | \
			  					sed 's/\_R[1-2].fastq.gz//' | \
			  					sort -u)

# (02.3)
# -----------------------------------------------------------------------------
# OUTPUTS

	# (a) Define OUTPUT
	# OUTPUT PATH
		OUT_DIR=../results/Coverage
	# Make DIR
		mkdir -p $OUT_DIR
	# (b) OUTPUT files
	# OUTPUT File 1:
	# Contains TXT report
  	OUT_TXT=$OUT_DIR/coverage_report.txt
	# OUTPUT File 2:
	# Contains CSV report
		OUT_CSV=$OUT_DIR/coverage_report.csv

# (02.4)
# -----------------------------------------------------------------------------
# Generate report

# For Loop
  for sample in $SAMPLE_NAME
  do

	# (a) Define DIRNAME
	# Contains only the SAMPLE name with no PATH
	  DIRNAME=${sample##*/}

	# (b) START message
		echo " "
		echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
		echo " [ Begin running fastq-info ] [ SAMPLE name: $DIRNAME ] "

	# (c) Define INPUT
	# Use FASTQ reads after processing with cutadapt
	# Forward FASTQ read (R1)
		R1=$sample\_R1.fastq.gz
	# Reverse FASTQ read (R2)
		R2=$sample\_R2.fastq.gz
  # Assembly file
  # Used to be able to calculate sequence coverage
  	contigs=../results/Contigs_Filtered/$DIRNAME\_HCov.contigs.fasta

	# (d) Run SOFTWARE
  # SOFTWARE: fastq-info
	  $run_Fastqinfo $R1 \
  						     $R2 \
							     $contigs \
							     >> \
							     $OUT_TXT

  # (e) COMPLETION message
	  echo " [ Finished running fastq-info ] [ SAMPLE name: $DIRNAME ] "
		echo " [ Contigs location: ]"
		echo " [ $contigs ] "
	  echo " = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = "
	  echo " "

	done


###############################################################################
# (03) Format final report ( contains SAMPLE names and COLUMN headers )
###############################################################################

# (03.1)
# -----------------------------------------------------------------------------
# Define OUTPUT

	# (a) Temporary reports
		TMP1=Out1.txt
		TMP2=Out2.txt
		TMP3=Out3.txt
		TMP4=Out4.txt
		TMP5=Out5.txt
		TMP6=Out6.txt

# (03.2)
# -----------------------------------------------------------------------------
# Define COLUMN header variables to print to REPORT

	# (a) COLUMN headers used in REPORT
	  COL_Header=$( echo SAMPLE'\t'\
	                     AVG_LENGTH'\t'\
	                     READS'\t'\
	                     GENOME'\t'\
	                     COVERAGE )

# (03.3)
# -----------------------------------------------------------------------------
# Format report

	# (a) Remove headers from each individual sample results
		awk 'NR%2==0' $OUT_TXT >> $TMP1
	# (b) Modify names of column 1 ($1) to keep only the sample name
		awk 'BEGIN {FS = "/"}; {print $4}' $TMP1 > $TMP2
		awk '{gsub(/_.*/,""); print $1}' $TMP2 > $TMP3
	# (c) Combine formatted column 1 ($TMP3) with the rest of columns ($TMP1)
		paste $TMP3 $TMP1 | \
		awk '{FS = "\t"} ; {print $1,$3,$4,$5,$6}' > $TMP4

# (03.4)
# -----------------------------------------------------------------------------
# Prints the respective COLUMN headers to REPORT

	# (a) Generate a TSV report
		cat $TMP4 | tr " " "\t" >  $TMP5
	# (b) Print column headers to TSV report (FIRST row)
	  awk -v VAR="$COL_Header" 'BEGIN { print VAR }; { print }' \
					 $TMP5 > $TMP6
	# (c) Generate a CSV delimited report
		cat $TMP6 | tr "\t" "," > $OUT_CSV

# (03.5)
# -----------------------------------------------------------------------------
# Remove accesory files generated

	# (a) Temporary reports
		rm $TMP1
		rm $TMP2
		rm $TMP3
		rm $TMP4
		rm $TMP5
		rm $TMP6
	# (b) TXT report
		rm $OUT_TXT


###############################################################################
# END:
# SCRIPT: 15_Assembly_Coverage.sh
###############################################################################
