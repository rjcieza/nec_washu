# BioRxiv formatted manuscript
#+EXCLUDE_TAGS: noexport
#+OPTIONS: tex:t
#+OPTIONS: toc:nil
#+OPTIONS: H:3 num:0
#+STARTUP: overview
#+STARTUP: hideblocks
#+NAME: pdf_metadata
#+begin_src sh :tangle report.txt :results silent :exports none :eval no
InfoKey: Title
InfoValue: Draft manuscript
InfoKey: Author
InfoValue: David R. Hill & Roberto Cieza
InfoKey: Subject
InfoValue: BioRxiv
#+end_src
#+NAME: latex-class-setup
#+begin_src emacs-lisp :results silent :exports none :eval yes
;; latex header for nih grant format
(unless (find "biorxiv" org-latex-classes :key 'car
          :test 'equal)
	 (add-to-list 'org-latex-classes
	  '("biorxiv" 
  "\\documentclass[12pt,notitlepage]{article}
  [NO-DEFAULT-PACKAGES]
  [EXTRA]"
  ("\\section{%s}" . "\\section*{%s}")
  ("\\subsection{%s}" . "\\subsection*{%s}")
  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
  ("\\paragraph{%s}" . "\\paragraph*{%s}")
  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
  )
)
#+end_src
#+CALL: latex-class-setup()
#+LATEX_CLASS: biorxiv
#+LATEX_HEADER: \input{header.tex}
#+LATEX_HEADER: \usepackage{lineno}
#+LATEX_CLASS_OPTIONS: [11pt]
#+begin_src latex
% title goes here:
\begin{flushleft}
{\Large
\textbf\newline{Identification of genetic determinants of neonatal intestinal barrier function in NICU \textit{E. coli} isolates.}
}
\newline
% authors go here:
\\
David R. Hill\textsuperscript{1,*},
Roberto J. Cieza\textsuperscript{2,*},
Veda K. Yadagiri\textsuperscript{2},
Sha Huang\textsuperscript{1},
Phillip K. Tarr\textsuperscript{3},
Jason R. Spence\textsuperscript{1,4},
and Vincent B. Young\textsuperscript{2,5}
\\
\bigskip
\small{
$^{1}$ Division of Gastroeneterology and Hepatology
\\
$^{2}$ Division of Infectious Disease, Department of Internal Medicine, University of Michigan
\\
$^{3}$ Department of Pediatrics, Washington University in St. Louis
\\
$^{4}$ Department of Cell and Developmental Biology
\\
$^{5}$ Department of Microbiology and Immunology, University of Michigan
}
\\
\bigskip
$\ast$ These authors contributed equally to this work\\
%$\ast$ Corresponding authors:  
\end{flushleft}
#+end_src
* Abstract
 Necrotizing enterocolitis (NEC) is a leading cause of mortality among premature infants characterized by loss of intestinal barrier integrity and necrosis of the bowel. Recent work suggests a multifactorial etiology in which the immature intestinal barrier predisposes preterm infants to intestinal injury and inflammation following postpartum microbial colonization. Although \textit{E. coli} is a widely prevalent and abundant colonizer of the neonatal gut, specific \textit{E. coli} genetic factors have also been associated with increased NEC risk. However, the microbial factors that determine success or failure of the immature intestinal barrier remain undefined, presenting a significant limitation to our ability to address the fundamental causes of NEC. We evaluated the effects of clinical \textit{E. coli} isolated from both NEC and non-NEC NICU patients on the function of the immature intestinal epithelial barrier using stem cell derived human intestinal organoids. Our results demonstrate that these organoid systems can be colonized with clinical \textit{E. coli} isolates and induce a wide range of isolate-specific functional phenotypes in the immature epithelial barrier. Whole genome sequencing and comparative phylogenetic analysis of our cohort of \textit{E. coli} isolates reveals diverse genetic composition. Traditional phylogroup and sequence type categorization of these \textit{E. coli} isolates was not predictive of their effects on epithelial barrier phenotype. However, the presence or absence of individual genes or small sets of genes was strongly associated with the loss of epithelial barrier permeability and inflammatory cytokine expression. \textit{We confirmed the function of bacterial genes highly associated with loss of epithelial barrier integrity using genetic deletion and substitution to modify individual E. coli isolates.} Taken together, our results suggest that individual genes shared within a group of mutually genetically competent human symbionts such as \textit{E. coli} may shape functional outcomes in the neonatal intestinal barrier. \textit{Future studies may examine the prevalence of such genes within the intestinal microbiota of premature infant and determine whether these bacterial genetic factors are associated with NEC outcomes.}

* Introduction
Necrotizing enterocolits (NEC) affects 1% of all newborns in the Unites States \cite{Holman:2006}, with mortality occurring in up to 30% of cases \cite{Lin:2006}. NEC risk increases by seven-fold among premature and low birth-weight infants \cite{Boccia:2001}, and the prevalence of NEC may be increasing over time \cite{Schullinger:1981}\cite{Gephart:2012}. Infants who do develop NEC are more frequently colonized with certain types of bacteria \cite{Morrow:2013,Ward:2016,Torrazza:2013,Boccia:2001,Pammi:2017}. Studies of tissue from NEC patients indicate the loss of epithelial barrier integrity and hyperacute inflammation \cite{Tanner:2015,Hackam:2013,Nanthakumar:2011}. This suggests a multifactorial etiology by which immature intestinal barrier function predisposes the preterm infant to intestinal injury and inflammation following postpartum microbial colonization with specific organisms \cite{Tanner:2015,Morrow:2013,Neu:2011}. However, the interactions at the host-microbe interface that determine success or failure of the epithelial barrier in the immature intestine remain undefined, presenting a significant limitation to our ability to address the fundamental causes of NEC.
* Results
** The functional phenotype of HIOs innoculated with \textit{E. coli} is isolate-specific
#+begin_src latex
\begin{figure}[ht] %s state preferences regarding figure placement here

% use to correct figure counter if necessary
%\renewcommand{\thefigure}{2}

\includegraphics[width=0.5\textwidth]{../../results/barrier/fitc_boxplot_thalf.pdf}

\caption{\color{Gray} \textbf{The functional phenotype of HIOs innoculated with \textit{E. coli} is isolate-specific}. \textbf{A}, This figure is wrapped into the standard floating environment.}

%\label{fig2} % \label works only AFTER \caption within figure environment

\end{figure}
#+end_src

** Comparative genomic analysis of \textit{E. coli} isolates reveals diverse genetic composition

** Presence or absence of specific genetic factors is associated with epithelial barrier phenotype



* Discussion
* Materials and Methods
#+LATEX:\let\oldbibliography\thebibliography
#+LATEX:\renewcommand{\thebibliography}[1]{\oldbibliography{#1}
#+LATEX:\setlength{\itemsep}{-1pt}} %Reducing spacing in the bibliography.
#+LATEX:\footnotesize{ % https://www.sharelatex.com/learn/Font_sizes,_families,_and_styles#Reference_guide
#+LATEX:\bibliography{bibliography.bib} 
#+LATEX:\bibliographystyle{nihunsrt} % Use the custom nihunsrt bibliography style included with the template
#+LATEX:}\normalsize
* Local Variables :noexport:
# Local Variables:
# mode: org
# word-wrap: t
# truncate-lines: nil
# reftex-default-bibliography: ("bibliography.bib")
# reftex-cite-format: "\\cite{%l}"
# org-latex-with-hyperref: nil
# org-latex-title-command: nil
# ispell-dictionary: "american"
# End:
