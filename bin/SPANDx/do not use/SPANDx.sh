#!/bin/bash

###############################################################################
# Thanks for using SPANDx!!

# USAGE: SPANDx.sh
  # <parameters, required>
  # -r <reference, without .fasta extension>
  #
  # [parameters, optional]
  # -o [organism]
  # -m [generate SNP matrix yes/no]
  # -i [generate indel matrix yes/no]
  # -a [include annotation yes/no]
  # -v [Variant genome file. Name must match the SnpEff database]
  # -s [Specify read prefix to run single strain or none to construct
  #     a SNP matrix from a previous analysis ]
  # -t [Sequencing technology used Illumina/Illumina_old/454/PGM]
  # -p [Pairing of reads PE/SE]
  # -w [Window size in base pairs for BEDcoverage module]

# -----------------------------------------------------------------------------
# SPANDx by default expects reads to be paired end, in the following format:
# STRAIN_1_sequence.fastq.gz for the first pair and
# STRAIN_2_sequence.fastq.gz for the second pair.

# Reads not in this format will be ignored.
# If your data are not paired, you must set the -p parameter to SE to denote unpaired reads.
# By default -p is set to PE.
#
# -----------------------------------------------------------------------------
# SPANDx expects at least a reference file in FASTA format.
# For compatibility with all programs in SPANDx, FASTA files should conform to
# the specifications listed here: http://www.ncbi.nlm.nih.gov/BLAST/blastcgihelp.shtml
#
# Note that the use of nucleotides other than A, C, G, or T is not supported by
# certain programs in SPANDx and these should not be used in reference FASTA files.
#
# In addition, Picard, GATK and SAMtools handle spaces within contig names differently.
# Therefore, please avoid using spaces or special characters (e.g. $/*) in contig names.
#
# By default all read files present in the current working DIR will be processed.
# Sequence files within current DIR will be aligned against the reference using BWA,
# SNPs and indels will be called with GATK and a SNP matrix will be generated with
# GATK and VCFTools
#
# -----------------------------------------------------------------------------
# Optionally to run the SNP matrix without processing any sequence files set:
# -s to none and
# -m to yes.
# If -s is set to none SPANDx will skip all sequence files in the current DIR
# and will not perform the alignment or variant calls.
#
# Instead SPANDx will merge all VCF files contained in $PWD/Phylo/snps,
# interrogate those calls in the alignment files contained within $PWD/Phylo/bams
# and output a SNP matrix in $PWD/Phylo/out.
#
# Before running this module please check that all VCF files located within $PWD/Phylo/snps
# match the alignment files within $PWD/Phylo/bams
#
# e.g. SPANDx.sh -r K96243 -m yes -s none
# -----------------------------------------------------------------------------
#
# Written by Derek Sarovich and Erin Price - Menzies School of Health Research, Darwin, Australia
# Please send bug reports to mshr.bioinformatics@gmail.com
# If you find SPANDx useful and use it in published work please cite:
#
# SPANDx: a genomics pipeline for comparative analysis of large haploid whole genome re-sequencing datasets
# BMC Research Notes 2014, 7:618"
#
# Version 2.2
# 2.0-2.1 Added SGE job handling
# 2.1-2.2 Added SLURM job handling
# 2.3 Added "no resource manager (NONE)" for job handling
#
#################################################################
usage()
{
echo -e  "Thanks for using SPANDx v3.2
Usage:
   SPANDx.sh -r <reference, without .fasta extension>
Optional Parameter:
  -o       Organism
  -m       Generate SNP matrix (yes/no)
  -i       Generate indel matrix (yes/no)
  -a       Include annotation (yes/no)
  -v       Variant genome file. Name must match the SnpEff database
  -s       Specify read prefix to run single strain or none to just construct SNP matrix
  -t       Sequencing technology used Illumina/Illumina_old/454/PGM
  -p       Pairing of reads (PE/SE)
  -w       BEDTools window size in base pairs
  -z       Include tri-allelic and tetra-allelic SNPs (yes/no)\n"
}
help()
{
usage
cat << _EOF_

Thanks for using SPANDx!!

SPANDx requires a reference in FASTA format and for this to be specified with the -r flag. Please do not include the .fasta extension in the reference name.

FASTA files should conform to the specifications listed here: http://www.ncbi.nlm.nih.gov/BLAST/blastcgihelp.shtml. Some programs used within SPANDx do not allow the use of IUPAC codes and these should not be used in reference FASTA files.

To output a merged SNP file for phylogenetic reconstruction, set the -m flag to yes.

By default, SPANDx will process all sequence data in the present working directory.

By default, SPANDx expects reads to be paired-end (PE) Illumina data, named in the following format: STRAIN_1_sequence.fastq.gz for the first pair and STRAIN_2_sequence.fastq.gz for the second pair. Reads not in this format will be ignored by SPANDx.

If you do not have PE data, you must set the -p flag to SE to denote single-end reads. By default, -p is set to PE.

Sequences will be aligned against the reference using BWA. SNPs and indels will be called with GATK and a SNP matrix will be generated with GATK and VCFTools.

For a more detailed description of how to use SPANDx and its capability, refer to the SPANDx manual or the BMC Research Notes publication. If you have any questions, requests or issues with SPANDx, please send an e-mail to mshr.bioinformatics@gmail.com or derek.sarovich@menzies.edu.au.

If you use SPANDx in published work please cite it!: SPANDx: a genomics pipeline for comparative analysis of large haploid whole genome re-sequencing datasets - BMC Research Notes 2014, 7:618

_EOF_

}

#Define path to SPANDx install
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

if  [ $# -lt 1 ]
    then
	    usage
		exit 1
fi

# source dependencies
source "$SCRIPTPATH"/SPANDx.config
source "$SCRIPTPATH"/scheduler.config

# DECLARE VARIABLES
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
declare -rx SCRIPT=${0##*/}
OPTSTRING="hr:o:d:m:a:v:s:t:p:w:i:z:"
declare SWITCH
declare ref
org=haploid
seq_directory="$PWD"
matrix=no
annotate=no
strain=all
tech=Illumina
pairing=PE
variant_genome=no
window=1000
indel_merge=no
tri_tetra_alelic=no

# Examine individual options
while getopts "$OPTSTRING" SWITCH; do
		case $SWITCH in

		r) ref="$OPTARG"
		   echo "Reference = $ref"
		   ;;

		o) org="$OPTARG"
		   echo "Organism = $org"
		   ;;

		d) seq_directory="$OPTARG"
		   echo "SPANDx expects read files to be in $seq_directory"
		   ;;

        m) matrix="$OPTARG"
           if [ "$matrix" == yes -o "$matrix" == no ]; then
           echo "SNP matrix will be generated? $matrix"
		   else
		       echo -e "\nIf the optional -m parameter is used it must be set to yes or no\n"
			   echo -e "By default -m is set to no and SPANDx will not generate a SNP matrix\n"
		       usage
			   exit 1
           fi
           ;;

		a) annotate="$OPTARG"
		   if [ "$annotate" == yes -o "$annotate" == no ]; then
		   echo "VCF files will be annotated with SnpEff? $annotate"
		   else
		       echo -e "\nIf the optional -a parameter is used it must be set to yes or no\n"
			   echo -e "By default -a is set to no and variants will not be annotated\n\n"
		       usage
			   exit 1
           fi
           ;;

		s) strain="$OPTARG"
		   echo "Only strain $strain will be processed. By default all strains within current directory will be processed"
		   ;;

		t) tech="$OPTARG"
			if [ "$tech" == Illumina -o "$tech" == Illumina_old -o "$tech" == 454 -o "$tech" == PGM ]; then
			    echo -e "Technology used is $tech\n"
			else
			   echo -e "If the optional parameter -t is used it must be set to Illumina, Illumina_old, 454 or PGM.\n"
			   echo -e "By default -t is set to Illumina and SPANDx will assume your sequence data is in Illumina format with quality encoding of 1.8+\n"
			   usage
			   exit 1
			fi
			;;

		p) pairing="$OPTARG"
		   if [ "$pairing" == PE -o "$pairing" == SE ]; then
               echo "The pairing of your reads has been indicated as $pairing"
		   else
		       echo -e "\nIf the optional -p parameter is used it must be set to PE or SE\n"
			   echo -e "By default -p is set to PE and SPANDx assumes your reads are paired end\n"
		       usage
			   exit 1
		   fi
		   ;;

		w) window="$OPTARG"
		   echo "BEDTools BEDCoverage window size has been set to $window base pairs. Default is 1000bp"
		   ;;

		v) variant_genome="$OPTARG"
		   echo "SnpEff will use $variant_genome as the annotated reference"
		   echo "Please make sure this name matches the name in SnpEff database. Also verify chromosome names are correct. These can be found easily at ftp://ftp.ncbi.nih.gov/genomes"
           ;;

        i) indel_merge="$OPTARG"
		   if [ "$indel_merge" == yes -o "$indel_merge" == no ]; then
               echo -e "Indels will be merged and checked across genomes = $indel_merge\n"
		   else
		       echo -e "Indel merge must be set to yes or no. Please refer to the manual for more details\n"
			   exit 1
			   fi
           ;;

		z) tri_tetra_allelic="$OPTARG"
           if [ "$tri_tetra_allelic" == yes -o "$tri_tetra_allelic" == no ]; then
               echo -e "Tri- and tetra-allelic SNPs will be included in the phylogenetic analyses\n"
		   else
		       echo -e "Tri- and tetra-allelic SNPs (-z) must be set to yes or no. Please refer to the manual for more details\n"
			   exit 1
		   fi
           ;;


		\?) usage
		    exit 1
		    ;;

		h) help
		   exit 1
		   ;;

		*) echo "script error: unhandled argument"
           usage
		   exit 1
		   ;;


	esac
done

echo -e "\nThe following parameters will be used\n"
echo -e "-------------------------------------\n"
echo "Organism = $org"
echo "Output directory and directory containing sequence files = $seq_directory"
echo "SNP matrix will be created? = $matrix"
echo "Genomes will be annotated? = $annotate"
echo "Strain(s) to be processed = $strain"
echo "Sequence technology used = $tech"
echo "Pairing of reads = $pairing"
echo "Variant genome specified for SnpEff = $variant_genome"
echo "Window size for BedTools = $window"
echo "Indels will be merged and corrected = $indel_merge"
echo -e "-------------------------------------\n\n"


# REFERNECE INDEX files generated:
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# INDEX file created with BWA
  ref_index=${seq_directory}/${ref}.bwt
# INDEX created with SAMtools
  REF_INDEX_FILE=${seq_directory}/${ref}.fasta.fai
# BED file from REFERENCE
  REF_BED=${seq_directory}/${ref}.bed
# DICTIONARY files created with Picard tools
  REF_DICT=${seq_directory}/${ref}.dict

# $PBS_O_WORKDIRP: directory from which you submitted the job
  # ! EXPRESSION:	  The EXPRESSION is FALSE
if [ ! $PBS_O_WORKDIR ];
then
  # If PBS_O_WORKDIR exist, equals to $seq_directory
  PBS_O_WORKDIR="$seq_directory"
fi

cd $PBS_O_WORKDIR

# FILE checks
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

# Find REFERENCE GENOME in the correct format
if [ ! -s "$ref.fasta" ];
then
  echo -e "Couldn't locate REFERENCE file. \
           Please make sure that REFERENCE file is in the analysis\
           directory,\n you have specified the REFERENCE name correctly, \
           and that the .fasta extension is not included\n"
  exit 1
else
  echo -e "Found FASTA file\n"
fi

# Check for blank lines in the REFERENCE
ref_blank_lines=`grep -c '^$' $ref.fasta`
if [ "$ref_blank_lines" != 0 ];
then
  echo -e "ERROR: REFERENCE FASTA file is formatted incorrectly and must \
           contain 0 blank lines. Blank lines will cause BWA and GATK to fail."
  exit 1
else
  echo -e "FASTA file looks to contain no blank lines. Good.\n"
fi


# PROGRAM checks
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Test for dependencies required by SPANDx
  bwa_test=`command -v "$BWA"`
  samtools_test=`command -v "$SAMTOOLS"`
  bedtools_test=`command -v "$BEDTOOLS"`
  vcftools_test=`command -v "$VCFTOOLS"`
  vcfmerge_test=`command -v "$VCFMERGE"`
  bgzip_test=`command -v "$BGZIP"`
  tabix_test=`command -v "$TABIX"`
  java_test=`command -v "$JAVA"`

# BWA
if [ -z "$bwa_test" ];
then
  echo "ERROR: SPANDx requires BWA to function. \
        Please make sure PATH is specified in SPANDx.config"
	exit 1
fi

# SAMtools
if [ -z "$samtools_test" ];
then
  echo "ERROR: SPANDx requires SAMtools to function. \
        Please make sure PATH is specified in SPANDx.config"
	exit 1
fi

# GATK
if [ ! -f "$GATK" ];
then
  echo "ERROR: SPANDx requires GATK and java to function. \
        Please make sure the PATH is specified in SPANDx.config"
	exit 1
fi

# BEDtools
if [ -z "$bedtools_test" ];
then
  echo "ERROR: SPANDx requires  BEDtools to function. \
        Please make sure the PATH is specified in SPANDx.config"
	exit 1
fi

# Picard
if [ ! -f "$PICARD" ];
then
  echo "ERROR: SPANDx requires Picard to function. \
        Please make sure the PATH is specified in SPANDx.config"
	exit 1
fi

#SnpEff
if [ "$annotate" == yes ];
then
  if [ ! -f "$SNPEFF" ];
  then
    echo "ERROR: SPANDx requires SnpEff to function. \
          Please make sure the PATH is specified in SPANDx.config"
		exit 1
  fi
fi

#VCFtools
if [ -z "$vcftools_test" ];
then
  echo "ERROR: SPANDx requires VCFtools to function. \
        Please make sure the PATH is specified in SPANDx.config"
	exit 1
fi

#vcf-merge function
if [ -z "$vcfmerge_test" ];
then
  echo "ERROR: SPANDx requires vcf-merge to function. \
        Please make sure the PATH is specified in SPANDx.config"
	exit 1
fi

# bgzip
if [ -z "$bgzip_test" ];
then
  echo "ERROR: SPANDx requires bgzip to function. \
        Please make sure the PATH is specified in SPANDx.config"
	exit 1
fi

# tabix
if [ -z "$tabix_test" ];
then
  echo "ERROR: SPANDx requires tabix to function. \
        Please make sure the PATH is specified in SPANDx.config"
	exit 1
fi

# Java
if [ -z "$java_test" ];
then
  echo "ERROR: SPANDx requires java. \
        Please make sure JAVA is available on your system"
	exit 1
fi


# SEQUENCE TECHNOLOGY check for pairing
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if [ "$tech" == 454 -o "$tech" == PGM ];
then
  if [ "$pairing" == PE ];
  then
    echo -e "ERROR: SPANDx does not currently support paired end PGM or 454 data. \
             If your PGM or 454 data is single end please change the -p switch to SE\n"
		exit 1
  fi
fi

# HANDLING and CHECKS for read files
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if [ "$strain" == all ];
then
  sequences_tmp=(`find $PBS_O_WORKDIR/*_1_sequence.fastq.gz -printf "%f "`)
  sequences_rename=("${sequences_tmp[@]/_1_sequence.fastq.gz/}")
	IFS=$'\n' sequences=($(sort <<<"${sequences_rename[*]}"))
	unset IFS
  n=${#sequences[@]}
  if [ $n == 0 ];
  then
    echo -e "Program couldn't find any sequence files to process"
    echo -e "Sequences must be in the format STRAIN_1_sequence.fastq.gz \
             STRAIN_2_sequence.fastq.gz for paired end reads"
    echo -e "and STRAIN_1_sequence.fastq.gz for single end data\n"
	  exit 1
    else
    echo -e "Sequences have been loaded into SPANDx\n"
  fi
fi


# Check READ PAIRING and CORRECT NOTATION
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if [ "$pairing" == PE -a "$strain" == all ];
then
  sequences_tmp2=(`find $PBS_O_WORKDIR/*_2_sequence.fastq.gz -printf "%f "`)
  sequences2_rename=("${sequences_tmp2[@]/_2_sequence.fastq.gz/}")
	IFS=$'\n' sequences2=($(sort <<<"${sequences2_rename[*]}"))
	unset IFS
  n2=${#sequences2[@]}

  if [ $n != $n2 ];
  then
    echo "Number of FORWARD reads don't match number of reverse reads"
		exit 1
	fi

	for (( i=0; i<n; i++ ));
  do

    if [ ${sequences[$i]} != ${sequences2[$i]} ];
    then
      echo "Names of FORWARD reads don't match names of REVERSE reads. \
            RUNNING in PE mode needs correctly named pairs"
			echo -e "The offending read pair is ${sequences[$i]} and ${sequences2[$i]}\n"
			#echo "Forward read names are ${sequences[@]}"
			#echo "Reverse read names are ${sequences2[@]}"
      exit 1
    fi
  done;
fi

# ERROR CHECKING for strain VARIABLE
# Makes sure that if a single STRAIN is specified, the read data is present
# include if n == 1 and matrix == yes then exit
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if [ "$strain" != all -a "$strain" != none ];
then
  sequences="$strain"
	echo -e "SPANDx will process the single strain $strain.\n"

  if [ "$matrix" == yes ];
  then
    matrix=no
		echo -e "SPANDx will not run [ SNP matrix ] with ONE STRAIN specified. \
             If [ SNP matrix ] is desired please run SPANDx with more STRAINS. \
             If strains have already been run link the SNP VCF and BAM files to \
             the appropriate directories and set -s to none.\n"
	fi

	if [ "$pairing" == PE ];
  then

    if [ ! -s ${strain}_1_sequence.fastq.gz ];
    then
      echo -e "ERROR: SPANDx cannot find sequence file ${strain}_1_sequence.fastq.gz\n"
			echo -e "Please make sure the correct sequence reads are in the sequence directory\n"
			exit 1
    fi

    if [ ! -s ${strain}_2_sequence.fastq.gz ];
    then
      echo -e "ERROR: SPANDx cannot find sequence file ${strain}_2_sequence.fastq.gz\n"
			echo -e "Please make sure the correct sequence reads are in the sequence directory\n"
			exit 1
    fi

  fi

  if [ "$pairing" == SE ];
  then

    if [ ! -s ${strain}_1_sequence.fastq.gz ];
    then
      echo -e "ERROR: SPANDx cannot find sequence file ${strain}_1_sequence.fastq.gz\n"
			echo -e "Please make sure the correct sequence reads are in the sequence directory\n"
			exit 1
    fi
	fi
fi


#to do. Needs to have an n > 1 check before the test
if [ "$matrix" == no -a "$indel_merge" == yes ]; then
    echo -e "Indel merge has been requested. \
            As this cannot be determine without creation of a SNP matrix \
            the matrix variable has been set to yes\n"
	matrix=yes
fi

# Create DIR structure to deposite RESULTS
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# BEDcov
if [ ! -d "BEDcov" ];
then
  mkdir $seq_directory/BEDcov
fi

# TMP files
if [ ! -d "tmp" ];
then
  mkdir $seq_directory/tmp
fi

# Outputs
if [ ! -d "Outputs" ];
then
  mkdir $seq_directory/Outputs
fi

# Outputs, SNPs indels PASS
if [ ! -d "Outputs/SNPs_indels_PASS" ];
then
  mkdir $seq_directory/Outputs/SNPs_indels_PASS
fi

# Outputs, SNPs indels FAIL
if [ ! -d "Outputs/SNPs_indels_FAIL" ];
then
  mkdir $seq_directory/Outputs/SNPs_indels_FAIL
fi

# Outputs comparativa
if [ ! -d "Outputs/Comparative" ];
then
  mkdir $seq_directory/Outputs/Comparative
fi

# Logs
if [ ! -d "logs" ];
then
  mkdir $seq_directory/logs
fi

# VARIABLES for ANNOTATION MODULE
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
if [ "$annotate" == yes ];
then
  grep "$variant_genome" "$SNPEFF_CONFIG" &> /dev/null
  status=$?

  if [ ! $status == 0 ];
  then
    # USER message if SNPEFF_CONFIG NOT FOUND
    echo "SPANDx couldn't find the REFERENCE genome in the SnpEff config file"
		echo "The name of the annotated REFERENCE genome specified with the -v switch \
          must match a REFERENCE genome in the SnpEff database"
    echo "Does the SnpEff.config file contain the REFERENCE specified with the -v switch?"
		echo "Is the SnpEff.config file in the location specified by SPANDx.config?"
	  echo "If both of these parameters are correct please refer to the SnpEff \
          manual for further details on the setup of SnpEff"
		exit 1
  else
    echo -e "SPANDx found the REFERENCE file in the SnpEff.config file\n"
  fi

  # TEST if CHROMOSOME NAME in SnpEff database match those of REFERENCE file
		CHR_NAME=`$JAVA -jar $SNPEFF dump "$variant_genome" | grep -A1 'Chromosomes names' \
                                                      | tail -n1 \
                                                      | awk '{print $2}'\
                                                      |sed "s/'//g"`
	REF_CHR=`head -n1 "$ref".fasta | sed 's/>//'`

	if [ "$CHR_NAME" == "$REF_CHR" ];
  then
    echo -e "Chromosome names in the SnpEff DATABASE match the REFERENCE \
             chromosome names, good\n"
	else
    echo -e "Chromosome names in the SnpEff database DON'T match the REFERENCE \
            chromosome names.\n"
		echo -e "Please change the names of the REFERENCE file to match those in \
            the SnpEff database.\n"
		echo -e "If you are unsure what these are, \
            run: $JAVA -jar $SNPEFF dump $variant_genome\n"
		echo -e "The first chromosome name is $CHR_NAME.\n\n"
    echo -e "If you choose to continue the annotation component of SPANDx \
            may fail.\n"
    echo -e "Do you want to continue?\n"
    echo -e "Type Y to continue or anything else to exit\n"
    read ref_test

    if [ "$ref_test" == "Y" -o "$ref_test" == "y" -o "$ref_test" == "yes" ];
    then
      echo -e "Continuing\n\n"
    else
      exit 1
    fi
	fi

	if [ ! -d "$SNPEFF_DATA/$variant_genome" ];
  then
    echo -e "Downloading reference genome to SnpEff database\n"
		echo -e "If the program hangs here please check that the proxy settings are \
            correct and the cluster has internet access\n"
		echo -e "If required SNPEff databases can be manually downloaded and added \
            to the SPANDx pipeline\n"
		echo -e "Running the following command:"
		echo "$JAVA $JAVA_PROXY -jar $SNPEFF download -v $variant_genome"
    echo -e "In the following directory $PBS_O_WORKDIR\n"
		$JAVA ${JAVA_PROXY} -jar $SNPEFF download -v $variant_genome
	else
    echo -e "Annotated REFERENCE database has already been downloaded \
            for SnpEff\n"
  fi
fi


if [ "$SCHEDULER" == PBS -o "$SCHEDULER" == SGE -o "$SCHEDULER" == SLURM -o "$SCHEDULER" == NONE ];
then
  echo -e "SPANDx will use $SCHEDULER for resource management\n"
else
	echo -e "SPANDx requires you to set the SCHEDULER variable \
          to one of the following: PBS, SGE, SLURM or NONE. \nIt looks like \
          you might have specified the variable incorrectly.\n"
fi


# The following SECTIONS use the specified RESOURCE MANAGER to queue SPANDx jobs

###############################################################################
###############################################################################
###############################################################################
#####                                                                     #####
#####                     RESOURCE MANAGER:                               #####
#####                Portable Batch System (PBS)                          #####
#####                                                                     #####
###############################################################################
###############################################################################
###############################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# CLEAN Batch system LOG files
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
if [ "$SCHEDULER" == PBS ];
then
  # ---------------------------------------------------------------------------
  if [ -s qsub_ids.txt ];
  then
    rm qsub_ids.txt
  fi

  if [ -s qsub_ids2.txt ];
  then
    rm qsub_ids2.txt
  fi

  if [ -s qsub_array_ids.txt ];
  then
    rm qsub_array_ids.txt
  fi

  if [ -s mastervcf_id.txt ];
  then
    rm mastervcf_id.txt
  fi

  if [ -s clean_vcf_id.txt ];
  then
    rm clean_vcf_id.txt
  fi

  if [ -s matrix_id.txt ];
  then
    rm matrix_id.txt
  fi


###############################################################################
# INDEXING:
# - (STEP 01, STEP 02): Indexing of the reference with SAMTools and BWA
# - (STEP 03) Creation of the GATK and picard reference dictionaries
# - (STEP 04) Creation of the BED file for the bedcoverage module
###############################################################################

# STEP 01:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# BWA REFERENCE INDEXING
# Submission ID:    [ qsub_id_bwa ]
# -----------------------------------------------------------------------------
# Conditional:
# IF:         BWA INDEX FILE            =   DOES NOT EXIST
# THEN:       Run BWA
# -----------------------------------------------------------------------------
if [ ! -s "$ref_index" ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): BWA REFERENCE INDEXING\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # BWA:
  # -p STR	Prefix of the output database [same as db filename]
  # -a STR	Algorithm for constructing BWT index
  #         IS linear-time algorithm for constructing suffix array.
  cmd="$BWA index -a is \
                  -p ${seq_directory}/${ref} \
                  ${seq_directory}/${ref}.fasta"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_id_bwa=`qsub -N index \
                    -j $ERROR_PBS \
                    -A $ACCOUNT_PBS \
                    -q $QUEUE_PBS \
                    -m b \
                    -M $ADDRESS \
                    -l nodes=1:ppn=1,pmem=10gb \
                    -l walltime=1:00:00 \
                    -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ qsub_ids.txt ]
  echo -e "index\t$qsub_id_bwa" >> qsub_ids.txt
fi

# STEP 02:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# SAMtools REFERENCE INDEXING
# Submission ID:    [ qsub_id_SAMtools ]
# -----------------------------------------------------------------------------
# Conditional:
# IF:         SAMtools INDEX FILE       =   DOES NOT EXIST
# THEN:       Run SAMtools
# -----------------------------------------------------------------------------
if [ ! -s "$REF_INDEX_FILE" ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): SAMtools REFERENCE indexing\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SAMtools:
  cmd="$SAMTOOLS faidx ${seq_directory}/${ref}.fasta"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_id_SAMtools=`qsub -N SAM_index \
                         -W depend=afterok:$qsub_id_bwa \
                         -j $ERROR_PBS \
                         -A $ACCOUNT_PBS \
                         -q $QUEUE_PBS \
                         -m n \
                         -M $ADDRESS \
                         -l nodes=1:ppn=1,pmem=10gb \
                         -l walltime=1:00:00 \
                         -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ qsub_ids.txt ]
  echo -e "SAM_index\t$qsub_id_SAMtools" >> qsub_ids.txt
fi

# STEP 03:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# DICTIONARY CREATION
# Submission ID:    [ qsub_id_Picard ]
# -----------------------------------------------------------------------------
# Conditional:
# IF:         INDEX DICTIONARY       =   DOES NOT EXIST
# THEN:       Run Picard
# -----------------------------------------------------------------------------
if [ ! -s "$REF_DICT" ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): ${ref}.dict creation\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Picard:
  cmd="$JAVA $SET_VAR $CREATEDICT \
       R=${seq_directory}/${ref}.fasta \
       O=$REF_DICT"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_id_Picard=`qsub -N PICARD_dict \
                       -W depend=afterok:$qsub_id_SAMtools \
                       -j $ERROR_PBS \
                       -A $ACCOUNT_PBS \
                       -q $QUEUE_PBS \
                       -m n \
                       -M $ADDRESS \
                       -l nodes=1:ppn=1,pmem=10gb \
                       -l walltime=1:00:00 \
                       -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ qsub_ids.txt ]
  echo -e "PICARD_dict\t$qsub_id_Picard" >> qsub_ids.txt
fi

# STEP 04:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# MAKE BED file
# Window size by DEFAULT is set to 1000 bp
# Submission ID:    [ sbatch_id_BED ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         BED file        = DOES NOT EXIST
#             qsub_ids.txt    = EXIST
# THEN:       Extract information in COLUMN 2
#             Run Bedtools
# -----------------------------------------------------------------------------
if [ ! -s "$REF_BED" \
       -a qsub_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): BED file construction with BEDTools\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ qsub_ids.txt ]
  qsub_cat_ids=`cat qsub_ids.txt |\
                cut -f2 |\
                sed -e 's/^/:/' |\
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # BEDtools:
  cmd="$BEDTOOLS makewindows -g $REF_INDEX_FILE \
                 -w $window > $REF_BED"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_id_BED=`qsub -N BED_window \
                    -W depend=afterok:$qsub_id_Picard \
                    -j $ERROR_PBS \
                    -A $ACCOUNT_PBS \
                    -q $QUEUE_PBS \
                    -m n \
                    -M $ADDRESS \
                    -l nodes=1:ppn=1,pmem=10gb \
                    -l walltime=1:00:00 \
                    -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ qsub_ids.txt ]
  echo -e "BED_window\t$qsub_id_BED" >> qsub_ids.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         BED file        = DOES NOT EXIST
#             qsub_ids.txt    = DOES NOT EXIST
# THEN:       Run Bedtools
# -----------------------------------------------------------------------------
if [ ! -s "$REF_BED" \
       -a ! qsub_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): BED file construction with BEDTools\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # BEDtools:
  cmd="$BEDTOOLS makewindows -g $REF_INDEX_FILE \
      -w $window > $REF_BED"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_id_BED=`qsub -N BED_window \
                    -W depend=afterok:$qsub_id_Picard \
                    -j $ERROR_PBS \
                    -A $ACCOUNT_PBS \
                    -q $QUEUE_PBS \
                    -m n \
                    -M $ADDRESS \
                    -l nodes=1:ppn=1,pmem=10gb \
                    -l walltime=1:00:00 \
                    -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
  ## -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ qsub_ids.txt ]
  echo -e "BED_window\t$qsub_id_BED" >> qsub_ids.txt
fi


###############################################################################
# SEQUENCE ALIGNMENT and VARIANT CALLING for [ $sequences ]:
# MAIN alignment and variant calling SCRIPT contained with SPANDx
# - STEP 01:    Variants single
# - STEP 02:    Variants
# - SCRIPT:     Align_SNP_indel.sh
###############################################################################

# STEP 01:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# VARIANTS_SINGLE
# "qsub_ids.txt" was generated in INDEXING
# Submission ID:  [ qsub_array_id ]
variants_single ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         qsub_ids.txt              =   EXIST
#             $sequences.snps.PASS.vcf  =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT Align_SNP_indel.sh
# -----------------------------------------------------------------------------
if [ -s qsub_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ qsub_ids.txt ]
  qsub_cat_ids=`cat qsub_ids.txt | \
                cut -f2 | \
                sed -e 's/^/:/' | \
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/$sequences.snps.PASS.vcf ];
  then
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # PBS submission message - PRINTS on screen at the time of submission
    echo -e "Submit JOB (qsub): SEQUENCE ALIGNMENT and VARIANT CALLING for \
            $sequences\n"
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # VARIABLES
    var="seq=$sequences,\
         ref=$ref,\
         org=$org,\
         strain=$strain,\
         variant_genome=$variant_genome,\
         annotate=$annotate,\
         tech=$tech,\
         pairing=$pairing,\
         seq_path=$seq_directory,\
         SCRIPTPATH=$SCRIPTPATH"
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # Resource manager submission parameters
    qsub_array_id=`qsub -N aln_sequences \
                        -W depend=afterok:$qsub_id_BED \
                        -j $ERROR_PBS \
                        -A $ACCOUNT_PBS \
                        -q $QUEUE_PBS \
                        -m n \
                        -M $ADDRESS \
                        -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                        -l walltime=$TIME_PBS \
                        -v "$var" "$SCRIPTPATH"/Align_SNP_indel.sh`
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # PRINT the JOB ID into [ qsub_array_ids.txt ]
    echo -e "aln_sequences\t$qsub_array_id" >> qsub_array_ids.txt
	fi
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         qsub_ids.txt              =   DOES NOT EXIST
#             $sequences.snps.PASS.vcf  =   DOES NOT EXIST
# THEN:       Run SCRIPT Align_SNP_indel.sh
# -----------------------------------------------------------------------------
if [ ! -s qsub_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/$sequences.snps.PASS.vcf ];
  then
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # PBS submission message - PRINTS on screen at the time of submission
    echo -e "Submit JOB (qsub): SEQUENCE ALIGNMENT and VARIANT CALLING for \
            ${sequences[$i]}\n"
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # VARIABLES
    var="seq=$sequences,\
        ref=$ref,\
        org=$org,\
        strain=$strain,\
        variant_genome=$variant_genome,\
        annotate=$annotate,\
        tech=$tech,\
        pairing=$pairing,\
        seq_path=$seq_directory,\
        SCRIPTPATH=$SCRIPTPATH"
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # Resource manager submission parameters
    qsub_array_id=`qsub -N aln_$sequences \
                        -W depend=afterok:$qsub_id_BED \
                        -j $ERROR_PBS \
                        -A $ACCOUNT_PBS \
                        -q $QUEUE_PBS \
                        -m n \
                        -M $ADDRESS \
                        -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                        -l walltime=$TIME_PBS \
                        -v "$var" "$SCRIPTPATH"/Align_SNP_indel.sh`
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # PRINT the JOB ID into [ qsub_array_ids.txt ]
    echo -e "aln_$sequences\t$qsub_array_id" >> qsub_array_ids.txt
	fi
fi
}

# STEP 02:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# VARIANTS
# "qsub_ids.txt" was generated in INDEXING
# Submission ID:  [ qsub_array_id ]
variants ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         qsub_ids.txt              =   EXIST
#             $sequences.snps.PASS.vcf  =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT Align_SNP_indel.sh
# -----------------------------------------------------------------------------
if [ -s qsub_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ qsub_ids.txt ]
  qsub_cat_ids=`cat qsub_ids.txt |\
                cut -f2 |\
                sed -e 's/^/:/' |\
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/${sequences[$i]}.snps.PASS.vcf ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PBS submission message - PRINTS on screen at the time of submission
      echo -e "Submit JOB (qsub): SEQUENCE ALIGNMENT and VARIANT CALLING for \
              ${sequences[$i]}\n"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # VARIABLES
      var="seq=${sequences[$i]},\
          ref=$ref,\
          org=$org,\
          strain=$strain,\
          variant_genome=$variant_genome,\
          annotate=$annotate,\
          tech=$tech,\
          pairing=$pairing,\
          seq_path=$seq_directory,\
          SCRIPTPATH=$SCRIPTPATH"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_array_id=`qsub -N aln_${sequences[$i]} \
                          -W depend=afterok:$qsub_id_BED \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v "$var" "$SCRIPTPATH"/Align_SNP_indel.sh`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ qsub_array_ids.txt ]
      echo -e "aln_${sequences[$i]}\t$qsub_array_id" >> qsub_array_ids.txt
	  fi
  done
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         qsub_ids.txt              =   DOES NOT EXIST
#             $sequences.snps.PASS.vcf  =   DOES NOT EXIST
# THEN:       Run SCRIPT Align_SNP_indel.sh
# -----------------------------------------------------------------------------
if [ ! -s qsub_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/${sequences[$i]}.snps.PASS.vcf ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PBS submission message - PRINTS on screen at the time of submission
      echo -e "Submit JOB (qsub): SEQUENCE ALIGNMENT and VARIANT CALLING for \
              ${sequences[$i]}\n"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # VARIABLES
      var="seq=${sequences[$i]},\
          ref=$ref,\
          org=$org,\
          strain=$strain,\
          variant_genome=$variant_genome,\
          annotate=$annotate,\
          tech=$tech,\
          pairing=$pairing,\
          seq_path=$seq_directory,\
          SCRIPTPATH=$SCRIPTPATH"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_array_id=`qsub -N aln_${sequences[$i]} \
                          -W depend=afterok:$qsub_id_BED \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v "$var" "$SCRIPTPATH"/Align_SNP_indel.sh`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ qsub_array_ids.txt ]
      echo -e "aln_${sequences[$i]}\t$qsub_array_id" >> qsub_array_ids.txt
	  fi
  done
fi
}


###############################################################################
# SNP MATRIX GENERATION
# Main comparative genomics section of SPANDx
# Relies on the OUTPUTS from the VARIANT function above
# Final product is a clean.vcf file for SNP calls across genomes
# IF indels merge is set to yes creates clean.vcf files for indels across genomes
# - STEP 01:  Matrix
# - STEP 02:  Matrix ERROR CHECKING
# - STEP 03:  Creation of SNP array
# - SCRIPT:   Master_vcf.sh
# - SCRIPT:   SNP_matrix.sh
###############################################################################

# STEP 01:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# MATRIX
# "qsub_array_ids.txt" is generated in VARIANT
# Submission ID:  [ qsub_matrix_id ]
matrix ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         qsub_array_ids.txt        =   EXIST
#             master.vcf                =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT Master_vcf.sh
# -----------------------------------------------------------------------------
if [ -s qsub_array_ids.txt \
     -a \
     ! -s Phylo/out/master.vcf ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ qsub_array_ids.txt ]
  qsub_cat_ids=`cat qsub_array_ids.txt | \
                cut -f2 | \
                sed -e 's/^/:/' | \
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): CREATION OF MASTER VCF file\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_matrix_id=`qsub -N Master_vcf \
                       -W depend=afterok:$qsub_array_id \
                       -j $ERROR_PBS \
                       -A $ACCOUNT_PBS \
                       -q $QUEUE_PBS \
                       -m n \
                       -M $ADDRESS \
                       -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                       -l walltime=$TIME_PBS \
                       -v "$var" "$SCRIPTPATH"/Master_vcf.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ mastervcf_id.txt ]
  echo -e "Matrix_vcf\t$qsub_matrix_id" >> mastervcf_id.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         qsub_array_ids.txt        =   DOES NOT EXIST
#             master.vcf                =   DOES NOT EXIST
# THEN:       Run SCRIPT Master_vcf.sh
# -----------------------------------------------------------------------------
if [ ! -s qsub_array_ids.txt \
     -a \
     ! -s Phylo/out/master.vcf ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): CREATION OF MASTER VCF file\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  	qsub_matrix_id=`qsub -N Master_vcf \
                         -W depend=afterok:$qsub_array_id \
                         -j $ERROR_PBS \
                         -A $ACCOUNT_PBS \
                         -q $QUEUE_PBS \
                         -m n \
                         -M $ADDRESS \
                         -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                         -l walltime=$TIME_PBS \
                         -v "$var" "$SCRIPTPATH"/Master_vcf.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ mastervcf_id.txt ]
	echo -e "Matrix_vcf\t$qsub_matrix_id" >> mastervcf_id.txt
fi

# STEP 02:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# Matrix ERROR CHECKING SNP ACROSS ALL GENOMES IF INDELS ARE PRESENT
# "mastervcf_id.txt" was generated in MATRIX
# Submission ID:  [ qsub_clean_id ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         mastervcf_id.txt            =   EXIST
#             ${sequences[$i]}.clean.vcf  =   DOES NOT EXIST
#             INDEL MERGE                 =   YES
# THEN:       Extract information in COLUMN 2
#             Run GATK
# -----------------------------------------------------------------------------
if [ -s mastervcf_id.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ mastervcf_id.txt ]
  qsub_cat_ids=`cat mastervcf_id.txt | \
                cut -f2 | \
                sed -e 's/^/:/' | \
                tr -d '\n'`
	# -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
	echo -e "Submit JOB (qsub): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	out=("${sequences_tmp[@]/_1_sequence.fastq.gz/.clean.vcf}")
	bam=("${sequences_tmp[@]/_1_sequence.fastq.gz/.bam}")
	bam_array=("${bam[@]/#/$PBS_O_WORKDIR/Phylo/bams/}")
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${sequences[$i]}.clean.vcf ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_clean_id=`qsub -N clean_vcf \
                          -W depend=afterok:$qsub_matrix_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "clean_vcf\t$qsub_clean_id" >> clean_vcf_id.txt
		fi
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/indels/out/${sequences[$i]}.clean.vcf \
           -a "$indel_merge" == yes ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_clean_id=`qsub -N clean_vcf \
                          -W depend=afterok:$qsub_matrix_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "clean_vcf\t$qsub_clean_id" >> clean_vcf_id.txt
		fi
  done
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         mastervcf_id.txt            =   DOES NOT EXIST
#             ${sequences[$i]}.clean.vcf  =   DOES NOT EXIST
#             INDEL MERGE                 =   YES
# THEN:       Run GATK
# -----------------------------------------------------------------------------
if [ ! -s mastervcf_id.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub):ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  out=("${sequences_tmp[@]/_1_sequence.fastq.gz/.clean.vcf}")
	bam=("${sequences_tmp[@]/_1_sequence.fastq.gz/.bam}")
	bam_array=("${bam[@]/#/$PBS_O_WORKDIR/Phylo/bams/}")
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${sequences[$i]}.clean.vcf ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_clean_id=`qsub -N clean_vcf \
                          -W depend=afterok:$qsub_matrix_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "clean_vcf\t$qsub_clean_id" >> clean_vcf_id.txt
		fi
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/indels/out/${sequences[$i]}.clean.vcf \
           -a "$indel_merge" == yes ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_clean_id=`qsub -N clean_vcf \
                          -W depend=afterok:$qsub_matrix_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "clean_vcf\t$qsub_clean_id" >> clean_vcf_id.txt
		fi
  done
fi

# STEP 03:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# CREATION OF SNP ARRAY
# "clean_vcf_id.txt" was generated in ERROR CHECKING
# Submission ID:  [ qsub_matrix_final ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         clean_vcf_id.txt          =   EXIST
#             Ortho_SNP_matrix.nex      =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT SNP_matrix.sh
# -----------------------------------------------------------------------------
if [ -s clean_vcf_id.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ clean_vcf_id.txt ]
  qsub_cat_ids=`cat clean_vcf_id.txt | \
                cut -f2 | \
                sed -e 's/^/:/' | \
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): CREATION OF SNP ARRAY\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      variant_genome=$variant_genome,\
      annotate=$annotate,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge,\
      tri_tetra_allelic=$tri_tetra_allelic"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_matrix_final=`qsub -N Matrix_vcf \
                          -W depend=afterok:$qsub_clean_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v "$var" "$SCRIPTPATH"/SNP_matrix.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ matrix_id.txt ]
  echo -e "Matrix_vcf\t$qsub_matrix_final" >> matrix_id.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         clean_vcf_id.txt          =   DOES NOT EXIST
#             Ortho_SNP_matrix.nex      =   DOES NOT EXIST
# THEN:       Run SCRIPT SNP_matrix.sh
# -----------------------------------------------------------------------------
if [ ! -s clean_vcf_id.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): CREATION OF SNP ARRAY\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      variant_genome=$variant_genome,\
      annotate=$annotate,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge,\
      tri_tetra_allelic=$tri_tetra_allelic"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_matrix_final=`qsub -N Matrix_vcf \
                          -W depend=afterok:$qsub_clean_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v "$var" "$SCRIPTPATH"/SNP_matrix.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ matrix_id.txt ]
  echo -e "Matrix_vcf\t$qsub_matrix_final" >> matrix_id.txt
fi
}


###############################################################################
# FINAL SNP MATRIX GENERATION
# Generate a SNP matrix from the Phylo DIR assuming all SNP and BAM files have
# been linked into these DIR.
# Relies on the OUTPUTS from the VARIANT function above
# Final product is a clean.vcf file for SNP calls across genomes
# IF indels merge is set to yes creates clean.vcf for indels across genomes
# - STEP 01:  Final Matrix
# - STEP 02:  Matrix final ERROR CHECKING
# - STEP 03:  Creation of Final SNP array
# - SCRIPT:   Master_vcf_final.sh
# - SCRIPT:   SNP_matrix.sh
###############################################################################

# STEP 01:
# MATRIX FINAL
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# "qsub_ids.txt" is generated in INDEXING
# Submission ID:  [ qsub_matrix_id ]
matrix_final ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         qsub_ids.txt              =   EXIST
#             master.vcf                =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT Master_vcf_final.sh
# -----------------------------------------------------------------------------
if [ -s qsub_ids.txt \
     -a \
     ! -s Phylo/out/master.vcf ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ qsub_ids.txt ]
  qsub_cat_ids=`cat qsub_ids.txt | \
                cut -f2 | \
                sed -e 's/^/:/' | \
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
	echo -e "Submit JOB (qsub): CREATION OF MASTER VCF file\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
	qsub_matrix_id=`qsub -N Master_vcf \
                       -W depend=afterok:$qsub_array_id \
                       -j $ERROR_PBS \
                       -A $ACCOUNT_PBS \
                       -q $QUEUE_PBS \
                       -m n \
                       -M $ADDRESS \
                       -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                       -l walltime=$TIME_PBS \
                       -v "$var" "$SCRIPTPATH"/Master_vcf_final.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ mastervcf_id.txt ]
  echo -e "Matrix_vcf\t$qsub_matrix_id" >> mastervcf_id.txt
# -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         qsub_ids.txt            =   DOES NOT EXIST
#             master.vcf                =   DOES NOT EXIST
# THEN:       Run SCRIPT Master_vcf_final.sh
# -----------------------------------------------------------------------------
if [ ! -s qsub_ids.txt \
     -a \
     ! -s Phylo/out/master.vcf ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): CREATION OF MASTER VCF file\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
	qsub_matrix_id=`qsub -N Master_vcf \
                       -W depend=afterok:$qsub_array_id \
                       -j $ERROR_PBS \
                       -A $ACCOUNT_PBS \
                       -q $QUEUE_PBS \
                       -m n \
                       -M $ADDRESS \
                       -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                       -l walltime=$TIME_PBS \
                       -v "$var" "$SCRIPTPATH"/Master_vcf_final.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ mastervcf_id.txt ]
  echo -e "Matrix_vcf\t$qsub_matrix_id" >> mastervcf_id.txt
fi

# STEP 02:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# Matrix final ERROR CHECKING SNP ACROSS ALL GENOMES IF INDELS ARE PRESENT
# "mastervcf_id.txt" was generated in MATRIX
# Submission ID:  [ qsub_clean_id ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         mastervcf_id.txt            =   EXIST
#             Phylo/out/${out[$i]         =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run GATK
# -----------------------------------------------------------------------------
if [ -s mastervcf_id.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ mastervcf_id.txt ]
  qsub_cat_ids=`cat mastervcf_id.txt | \
                cut -f2 | \
                sed -e 's/^/:/' | \
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
	echo -e "Submit JOB (qsub): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	clean_array=(`find $PBS_O_WORKDIR/Phylo/snps/*.vcf -printf "%f "`)
  out=("${clean_array[@]/.vcf/.clean.vcf}")
  bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${out[$i]} ];
    then
      # -----------------------------------------------------------------------
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
           $GATK -T UnifiedGenotyper \
                 -rf BadCigar \
                 -R $PBS_O_WORKDIR/${ref}.fasta \
                 -I ${bam_array[$i]} \
                 -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                 -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                 -gt_mode GENOTYPE_GIVEN_ALLELES \
                 -out_mode EMIT_ALL_SITES \
                 -stand_call_conf 0.0 \
                 -glm BOTH \
                 -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_clean_id=`qsub -N clean_vcf \
                          -W depend=afterok:$qsub_matrix_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "clean_vcf\t$qsub_clean_id" >> clean_vcf_id.txt
		fi
  done
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         mastervcf_id.txt            =   DOES NOT EXIST
#             Phylo/out/${out[$i]         =   DOES NOT EXIST
# THEN:       Run GATK
# -----------------------------------------------------------------------------
if [ ! -s mastervcf_id.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  clean_array=(`find $PBS_O_WORKDIR/Phylo/snps/*.vcf -printf "%f "`)
  out=("${clean_array[@]/.vcf/.clean.vcf}")
  bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${out[$i]} ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_clean_id=`qsub -N clean_vcf \
                          -W depend=afterok:$qsub_matrix_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "clean_vcf\t$qsub_clean_id" >> clean_vcf_id.txt
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		fi
  done
fi
# -----------------------------------------------------------------------------
# 3rd Conditional:
# IF:         mastervcf_id.txt            =   EXIST
#             Phylo/out/${out[$i]         =   DOES NOT EXIST
#             INDEL MERGE                 =   YES
# THEN:       Extract information in COLUMN 2
#             Run GATK
# -----------------------------------------------------------------------------
if [ -s mastervcf_id.txt \
     -a "$indel_merge" == yes ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ mastervcf_id.txt ]
  qsub_cat_ids=`cat mastervcf_id.txt | \
                cut -f2 | \
                sed -e 's/^/:/' | \
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
	echo -e "Submit JOB (qsub): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	clean_array=(`find $PBS_O_WORKDIR/Phylo/indels/*.vcf -printf "%f "`)
  out=("${clean_array[@]/.vcf/.clean.vcf}")
  bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
           $GATK -T UnifiedGenotyper \
                 -rf BadCigar \
                 -R $PBS_O_WORKDIR/${ref}.fasta \
                 -I ${bam_array[$i]} \
                 -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                 -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                 -gt_mode GENOTYPE_GIVEN_ALLELES \
                 -out_mode EMIT_ALL_SITES \
                 -stand_call_conf 0.0 \
                 -glm BOTH \
                 -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_clean_id=`qsub -N clean_vcf \
                          -W depend=afterok:$qsub_matrix_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "clean_vcf\t$qsub_clean_id" >> clean_vcf_id.txt
		fi
  done
fi
# -----------------------------------------------------------------------------
# 4th Conditional:
# IF:         mastervcf_id.txt            =   DOES NOT EXIST
#             Phylo/out/${out[$i]         =   DOES NOT EXIST
#             INDEL MERGE                 =   YES
# THEN:       Run GATK
# -----------------------------------------------------------------------------
if [ ! -s mastervcf_id.txt \
       -a "$indel_merge" == yes ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  clean_array=(`find $PBS_O_WORKDIR/Phylo/indels/*.vcf -printf "%f "`)
  out=("${clean_array[@]/.vcf/.clean.vcf}")
  bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${out[$i]} ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      qsub_clean_id=`qsub -N clean_vcf \
                          -W depend=afterok:$qsub_matrix_id \
                          -j $ERROR_PBS \
                          -A $ACCOUNT_PBS \
                          -q $QUEUE_PBS \
                          -m n \
                          -M $ADDRESS \
                          -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                          -l walltime=$TIME_PBS \
                          -v command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "clean_vcf\t$qsub_clean_id" >> clean_vcf_id.txt
		fi
  done
fi

# STEP 03:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# CREATION OF FINAL SNP ARRAY
# "clean_vcf_id.txt" was generated in ERROR CHECKING
# Submission ID:  [ qsub_matrix_final ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         clean_vcf_id.txt          =   EXIST
#             Ortho_SNP_matrix.nex      =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT SNP_matrix.sh
# -----------------------------------------------------------------------------
if [ -s clean_vcf_id.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ clean_vcf_id.txt ]
  qsub_cat_ids=`cat clean_vcf_id.txt | \
                cut -f2 | \
                sed -e 's/^/:/' | \
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): CREATION OF SNP ARRAY\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      variant_genome=$variant_genome,\
      annotate=$annotate,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge,\
      tri_tetra_allelic=$tri_tetra_allelic"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_matrix_final=`qsub -N ARRAY_vcf \
                         -W depend=afterok:$qsub_clean_id \
                         -j $ERROR_PBS \
                         -A $ACCOUNT_PBS \
                         -q $QUEUE_PBS \
                         -m n \
                         -M $ADDRESS \
                         -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                         -l walltime=$TIME_PBS \
                         -v "$var" "$SCRIPTPATH"/SNP_matrix.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ matrix_id.txt ]
	echo -e "Matrix_vcf\t$qsub_matrix_final" >> matrix_id.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         clean_vcf_id.txt          =   DOES NOT EXIST
#             Ortho_SNP_matrix.nex      =   DOES NOT EXIST
# THEN:       Run SCRIPT SNP_matrix.sh
# -----------------------------------------------------------------------------
if [ ! -s clean_vcf_id.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): CREATION OF SNP ARRAY\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      variant_genome=$variant_genome,\
      annotate=$annotate,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge,\
      tri_tetra_allelic=$tri_tetra_allelic"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  qsub_matrix_final=`qsub -N ARRAY_vcf \
                         -W depend=afterok:$qsub_clean_id \
                         -j $ERROR_PBS \
                         -A $ACCOUNT_PBS \
                         -q $QUEUE_PBS \
                         -m n \
                         -M $ADDRESS \
                         -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                         -l walltime=$TIME_PBS \
                         -v "$var" "$SCRIPTPATH"/SNP_matrix.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ matrix_id.txt ]
  echo -e "Matrix_vcf\t$qsub_matrix_final" >> matrix_id.txt
fi
}


###############################################################################
# MERGE BED:
# Takes OUTPUT of each BED coverage assessment of the sequence alignment
# AND merges them in a comparative matrix
# Run when $strain=all
# NOT run When a single strain is analysed i.e. $strain doesn't equal all
# - STEP 01:  BED Coverage MERGE
# - SCRIPT:   BEDCov_merge.sh
###############################################################################

# STEP 01:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# BED Coverage MERGE
# "qsub_array_ids.txt" is generated in VARIANT
# Submission ID:  [ qsub_BED_id ]
merge_BED ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         qsub_array_ids.txt        =   EXIST
#             Bedcov_merge.txt          =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT BedCov_merge.sh
# -----------------------------------------------------------------------------
if [ -s qsub_array_ids.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Bedcov_merge.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ qsub_array_ids.txt ]
  qsub_cat_ids=`cat qsub_array_ids.txt | \
                cut -f2 | \
                sed -e 's/^/:/' | \
                tr -d '\n'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): BEDcov merge\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="seq_path=$seq_directory"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
	qsub_BED_id=`qsub -N BEDcov_merge \
                    -W depend=afterok:$qsub_array_id \
                    -j $ERROR_PBS \
                    -A $ACCOUNT_PBS \
                    -q $QUEUE_PBS \
                    -m b \
                    -M $ADDRESS \
                    -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                    -l walltime=$TIME_PBS \
                    -v "$var" "$SCRIPTPATH"/BedCov_merge.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ qsub_BED_id.txt ]
  echo -e "BEDcov_merge\t$qsub_BED_id" >> qsub_BED_id.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         qsub_array_ids.txt        =   DOES NOT EXIST
#             Bedcov_merge.txt          =   DOES NOT EXIST
# THEN:       Run SCRIPT BedCov_merge.sh
# -----------------------------------------------------------------------------
if [ ! -s qsub_array_ids.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Bedcov_merge.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PBS submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (qsub): BEDcov merge\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
       seq_path=$seq_directory"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
	qsub_BED_id=`qsub -N BEDcov_merge \
                    -W depend=afterok:$qsub_array_id \
                    -j $ERROR_PBS \
                    -A $ACCOUNT_PBS \
                    -q $QUEUE_PBS \
                    -m n \
                    -M $ADDRESS \
                    -l nodes=1:ppn=$PROCESSORS_PBS,pmem=$MEMORY_PBS \
                    -l walltime=$TIME_PBS \
                    -v "$var" "$SCRIPTPATH"/BedCov_merge.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ qsub_BED_id.txt ]
	echo -e "BEDcov_merge\t$qsub_BED_id" >> qsub_BED_id.txt
fi
}


###############################################################################
# VARIABLE TESTS
# Determine which functions need to be run for the SPANDx pipeline
###############################################################################

  # More than ONE STRAIN is present
  if [ "$strain" == all ];
  then
    variants
    merge_BED
  fi
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # ONE STRAIN is present
  if [ "$strain" != all \
        -a "$strain" != none ];
  then
    variants_single
  fi
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  --
  # RUN matrix if more then ONE STRAIN is present
  if [ "$matrix" == yes -a "$strain" != none ];
  then
    matrix
  fi

  if [ "$matrix" == yes -a "$strain" == none ];
  then
    matrix_final
  fi
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
fi


###############################################################################
###############################################################################
###############################################################################
#####                                                                     #####
#####                     RESOURCE MANAGER:                               #####
#####                          SLURM                                      #####
#####                                                                     #####
###############################################################################
###############################################################################
###############################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# CLEAN Batch system LOG files
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
if [ "$SCHEDULER" == SLURM ];
then
  # ---------------------------------------------------------------------------
  if [ -s sbatch_ids.txt ];
  then
    rm sbatch_ids.txt
  fi

  if [ -s sbatch_ids2.txt ];
  then
    rm sbatch_ids2.txt
  fi

  if [ -s sbatch_array_ids.txt ];
  then
    rm sbatch_array_ids.txt
  fi

  if [ -s mastervcf_id.txt ];
  then
    rm mastervcf_id.txt
  fi

  if [ -s clean_vcf_id.txt ];
  then
    rm clean_vcf_id.txt
  fi

  if [ -s matrix_id.txt ];
  then
    rm matrix_id.txt
  fi


###############################################################################
# INDEXING:
# - (STEP 01, STEP 02): Indexing of the reference with SAMTools and BWA
# - (STEP 03) Creation of the GATK and picard reference dictionaries
# - (STEP 04) Creation of the BED file for the bedcoverage module
###############################################################################

# STEP 01:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# BWA REFERENCE INDEXING
# Submission ID:    [ sbatch_id_bwa ]
# -----------------------------------------------------------------------------
# Conditional:
# IF:         BWA INDEX FILE            =   DOES NOT EXIST
# THEN:       Run BWA
# -----------------------------------------------------------------------------
if [ ! -s "$ref_index" ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): BWA REFERENCE INDEXING\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # BWA:
  # -p STR	Prefix of the output database [same as db filename]
  # -a STR	Algorithm for constructing BWT index
  #         IS linear-time algorithm for constructing suffix array.
  cmd="$BWA index -a is \
                  -p ${seq_directory}/${ref} \
                  ${seq_directory}/${ref}.fasta"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_id_bwa=`sbatch --job-name=index \
                        --account=$ACCOUNT_SLURM \
                        --partition=$PARTITION_SLURM \
                        --mail-user=$ADDRESS \
                        --mail-type=BEGIN \
                        --nodes=1 \
                        --ntasks-per-node=1 \
                        --mem-per-cpu=10000 \
                        --time=1:00:00 \
                        --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ sbatch_ids.txt ]
  echo -e "$sbatch_id_bwa" >> sbatch_ids.txt
fi

# STEP 02:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# SAMtools REFERENCE INDEXING
# Submission ID:    [ sbatch_id_SAMtools ]
# -----------------------------------------------------------------------------
# Conditional:
# IF:         SAMtools INDEX FILE       =   DOES NOT EXIST
# THEN:       Run SAMtools
# -----------------------------------------------------------------------------
if [ ! -s "$REF_INDEX_FILE" ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  --
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): SAMtools REFERENCE indexing\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SAMtools:
  cmd="$SAMTOOLS faidx ${seq_directory}/${ref}.fasta"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_id_SAMtools=`sbatch --job-name=SAM_index \
                             --dependency=afterok:$sbatch_id_bwa \
                             --account=$ACCOUNT_SLURM \
                             --partition=$PARTITION_SLURM \
                             --mail-user=$ADDRESS \
                             --mail-type=NONE \
                             --nodes=1 \
                             --ntasks-per-node=1 \
                             --mem-per-cpu=10000 \
                             --time=1:00:00 \
                             --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ sbatch_ids.txt ]
  echo -e "$sbatch_id_SAMtools" >> sbatch_ids.txt
fi

# STEP 03:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# DICTIONARY CREATION
# Submission ID:    [ sbatch_id_Picard ]
# -----------------------------------------------------------------------------
# Conditional:
# IF:         INDEX DICTIONARY       =   DOES NOT EXIST
# THEN:       Run Picard
# -----------------------------------------------------------------------------
if [ ! -s "$REF_DICT" ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): ${ref}.dict creation\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Picard:
  cmd="$JAVA $SET_VAR $CREATEDICT \
      R=${seq_directory}/${ref}.fasta \
      O=$REF_DICT"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
	sbatch_id_Picard=`sbatch --job-name=PICARD_dict \
                           --dependency=afterok:$sbatch_id_SAMtools \
                           --account=$ACCOUNT_SLURM \
                           --partition=$PARTITION_SLURM \
                           --mail-user=$ADDRESS \
                           --mail-type=NONE \
                           --nodes=1 \
                           --ntasks-per-node=1 \
                           --mem-per-cpu=10000 \
                           --time=1:00:00 \
                           --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ sbatch_ids.txt ]
	echo -e "$sbatch_id_Picard" >> sbatch_ids.txt
fi

# STEP 04:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# MAKE BED file
# Window size by DEFAULT is set to 1000 bp
# Submission ID:    [ sbatch_id_BED ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         BED file        = DOES NOT EXIST
#             qsub_ids.txt    = EXIST
# THEN:       Extract information in COLUMN 2
#             Run Bedtools
# -----------------------------------------------------------------------------
if [ ! -s "$REF_BED" \
     -a sbatch_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): BED file construction with BEDTools\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ sbatch_cat_ids ]
  sbatch_cat_ids=`cat sbatch_ids.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # BEDtools:
  cmd="$BEDTOOLS makewindows -g $REF_INDEX_FILE \
      -w $window > $REF_BED"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_id_BED=`sbatch --job-name=BED_window \
                        --dependency=afterok:$sbatch_id_Picard \
                        --account=$ACCOUNT_SLURM \
                        --partition=$PARTITION_SLURM \
                        --mail-user=$ADDRESS \
                        --mail-type=NONE \
                        --nodes=1 \
                        --ntasks-per-node=1 \
                        --mem-per-cpu=10000 \
                        --time=1:00:00 \
                        --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ sbatch_ids.txt ]
  echo -e "$sbatch_id_BED" >> sbatch_ids.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         BED file        = DOES NOT EXIST
#             qsub_ids.txt    = DOES NOT EXIST
# THEN:       Run Bedtools
# -----------------------------------------------------------------------------
if [ ! -s "$REF_BED" \
       -a ! sbatch_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): BED file construction with BEDTools\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # BEDtools:
  cmd="$BEDTOOLS makewindows -g $REF_INDEX_FILE \
      -w $window > $REF_BED"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_id_BED=`sbatch --job-name=BED_window \
                        --dependency=afterok:$sbatch_id_Picard \
                        --account=$ACCOUNT_SLURM \
                        --partition=$PARTITION_SLURM \
                        --mail-user=$ADDRESS \
                        --mail-type=NONE \
                        --nodes=1 \
                        --ntasks-per-node=1 \
                        --mem-per-cpu=10000 \
                        --time=1:00:00 \
                        --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ sbatch_ids.txt ]
  echo -e "$sbatch_id_BED" >> sbatch_ids.txt
fi


###############################################################################
# SEQUENCE ALIGNMENT and VARIANT CALLING for [ $sequences ]:
# MAIN alignment and variant calling SCRIPT contained with SPANDx
# - STEP 01:    Variants single
# - STEP 02:    Variants
# - SCRIPT:     Align_SNP_indel.sh
###############################################################################

# STEP 01:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# VARIANTS_SINGLE
# "sbatch_ids.txt" was generated in INDEXING
# Submission ID:  [ sbatch_array_id ]
variants_single_slurm ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         sbatch_ids.txt            =   EXIST
#             $sequences.snps.PASS.vcf  =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT Align_SNP_indel.sh
# -----------------------------------------------------------------------------
if [ -s sbatch_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ sbatch_ids.txt ]
  sbatch_cat_ids=`cat sbatch_ids.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/$sequences.snps.PASS.vcf ];
  then
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # SBATCH submission message - PRINTS on screen at the time of submission
    echo -e "Submit JOB (sbatch): SEQUENCE ALIGNMENT and VARIANT CALLING for \
            $sequences\n"
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # VARIABLES
    var="seq=$sequences,\
        ref=$ref,\
        org=$org,\
        strain=$strain,\
        variant_genome=$variant_genome,\
        annotate=$annotate,\
        tech=$tech,\
        pairing=$pairing,\
        seq_path=$seq_directory,\
        SCRIPTPATH=$SCRIPTPATH"
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # Resource manager submission parameters
    sbatch_array_id=`sbatch --job-name=aln_sequences \
                            --dependency=afterok:$sbatch_id_BED \
                            --account=$ACCOUNT_SLURM \
                            --partition=$PARTITION_SLURM \
                            --mail-user=$ADDRESS \
                            --mail-type=NONE \
                            --nodes=1 \
                            --ntasks-per-node=$PROCESSORS_SLURM \
                            --mem-per-cpu=$MEMORY_SLURM \
                            --time=$TIME_SLURM \
                            --export="$var" "$SCRIPTPATH"/Align_SNP_indel.sh`
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # PRINT the JOB ID into [ sbatch_array_ids.txt ]
    echo -e "$sbatch_array_id" >> sbatch_array_ids.txt
	fi
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         sbatch_ids.txt            =   DOES NOT EXIST
#             $sequences.snps.PASS.vcf  =   DOES NOT EXIST
# THEN:       Run SCRIPT Align_SNP_indel.sh
# -----------------------------------------------------------------------------
if [ ! -s sbatch_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/$sequences.snps.PASS.vcf ];
  then
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # SBATCH submission message - PRINTS on screen at the time of submission
    echo -e "Submit JOB (sbatch): SEQUENCE ALIGNMENT and VARIANT CALLING for \
            ${sequences[$i]}\n"
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # VARIABLES
    var="seq=$sequences,\
        ref=$ref,\
        org=$org,\
        strain=$strain,\
        variant_genome=$variant_genome,\
        annotate=$annotate,\
        tech=$tech,\
        pairing=$pairing,\
        seq_path=$seq_directory,\
        SCRIPTPATH=$SCRIPTPATH"
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # Resource manager submission parameters
    sbatch_array_id=`sbatch --job-name=aln_$sequences \
                            --dependency=afterok:$sbatch_id_BED \
                            --account=$ACCOUNT_SLURM \
                            --partition=$PARTITION_SLURM \
                            --mail-user=$ADDRESS \
                            --mail-type=NONE \
                            --nodes=1 \
                            --ntasks-per-node=$PROCESSORS_SLURM \
                            --mem-per-cpu=$MEMORY_SLURM \
                            --time=$TIME_SLURM \
                            --export="$var" "$SCRIPTPATH"/Align_SNP_indel.sh`
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # PRINT the JOB ID into [ sbatch_array_ids.txt ]
    echo -e "$sbatch_array_id" >> sbatch_array_ids.txt
	fi
fi
}

# STEP 02:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# VARIANTS
# "sbatch_ids.txt" was generated in INDEXING
# Submission ID:  [ sbatch_array_id ]
variants_slurm ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         sbatch_ids.txt            =   EXIST
#             $sequences.snps.PASS.vcf  =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT Align_SNP_indel.sh
# -----------------------------------------------------------------------------
if [ -s sbatch_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ sbatch_ids.txt ]
  sbatch_cat_ids=`cat sbatch_ids.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/${sequences[$i]}.snps.PASS.vcf ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # SBATCH submission message - PRINTS on screen at the time of submission
      echo -e "Submit JOB (sbatch): SEQUENCE ALIGNMENT and VARIANT CALLING for \
              ${sequences[$i]}\n"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # VARIABLES
      var="seq=${sequences[$i]},\
          ref=$ref,\
          org=$org,\
          strain=$strain,\
          variant_genome=$variant_genome,\
          annotate=$annotate,\
          tech=$tech,\
          pairing=$pairing,\
          seq_path=$seq_directory,\
          SCRIPTPATH=$SCRIPTPATH"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_array_id=`sbatch --job-name=aln_${sequences[$i]} \
                              --dependency=afterok:$sbatch_id_BED \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export="$var" "$SCRIPTPATH"/Align_SNP_indel.sh`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ sbatch_array_ids.txt ]
      echo -e "$sbatch_array_id" >> sbatch_array_ids.txt
	  fi
  done
fi
# ----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         sbatch_ids.txt            =   DOES NOT EXIST
#             $sequences.snps.PASS.vcf  =   DOES NOT EXIST
# THEN:       Run SCRIPT Align_SNP_indel.sh
# -----------------------------------------------------------------------------
if [ ! -s sbatch_ids.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/${sequences[$i]}.snps.PASS.vcf ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # SBATCH submission message - PRINTS on screen at the time of submission
      echo -e "Submit JOB (qsub): SEQUENCE ALIGNMENT and VARIANT CALLING for \
              ${sequences[$i]}\n"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # VARIABLES
      var="seq=${sequences[$i]},\
          ref=$ref,\
          org=$org,\
          strain=$strain,\
          variant_genome=$variant_genome,\
          annotate=$annotate,\
          tech=$tech,\
          pairing=$pairing,\
          seq_path=$seq_directory,\
          SCRIPTPATH=$SCRIPTPATH"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_array_id=`sbatch --job-name=aln_${sequences[$i]} \
                              --dependency=afterok:$sbatch_id_BED \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export="$var" "$SCRIPTPATH"/Align_SNP_indel.sh`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ sbatch_array_ids.txt ]
      echo -e "$sbatch_array_id" >> sbatch_array_ids.txt
	  fi
  done
fi
}


###############################################################################
# SNP MATRIX GENERATION
# Main comparative genomics section of SPANDx
# Relies on the OUTPUTS from the VARIANT function above
# Final product is a clean.vcf file for SNP calls across genomes
# IF indels merge is set to yes creates clean.vcf files for indels across genomes
# - STEP 01:  Matrix
# - STEP 02:  Matrix ERROR CHECKING
# - STEP 03:  Creation of SNP array
# - SCRIPT:   Master_vcf.sh
# - SCRIPT:   SNP_matrix.sh
###############################################################################

# STEP 01:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# MATRIX
# "sbatch_array_ids.txt" is generated in VARIANT
# Submission ID:  [ sbatch_matrix_id ]
matrix_slurm ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         sbatch_array_ids.txt      =   EXIST
#             master.vcf                =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT Master_vcf.sh
# -----------------------------------------------------------------------------
if [ -s sbatch_array_ids.txt \
     -a \
     ! -s Phylo/out/master.vcf ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ sbatch_array_ids.txt ]
  sbatch_cat_ids=`cat sbatch_array_ids.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): CREATION OF MASTER VCF file\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_matrix_id=`sbatch --job-name=Master_vcf \
                           --dependency=afterok:$sbatch_array_id \
                           --account=$ACCOUNT_SLURM \
                           --partition=$PARTITION_SLURM \
                           --mail-user=$ADDRESS \
                           --mail-type=NONE \
                           --nodes=1 \
                           --ntasks-per-node=$PROCESSORS_SLURM \
                           --mem-per-cpu=$MEMORY_SLURM \
                           --time=$TIME_SLURM \
                           --export="$var" "$SCRIPTPATH"/Master_vcf.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ mastervcf_id.txt ]
  echo -e "$sbatch_matrix_id" >> mastervcf_id.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         sbatch_array_ids.txt      =   DOES NOT EXIST
#             master.vcf                =   DOES NOT EXIST
# THEN:       Run SCRIPT Master_vcf.sh
# -----------------------------------------------------------------------------
if [ ! -s sbatch_array_ids.txt \
     -a \
     ! -s Phylo/out/master.vcf ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): CREATION OF MASTER VCF file\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_matrix_id=`sbatch --job-name=Master_vcf \
                           --dependency=afterok:$sbatch_array_id \
                           --account=$ACCOUNT_SLURM \
                           --partition=$PARTITION_SLURM \
                           --mail-user=$ADDRESS \
                           --mail-type=NONE \
                           --nodes=1 \
                           --ntasks-per-node=$PROCESSORS_SLURM \
                           --mem-per-cpu=$MEMORY_SLURM \
                           --time=$TIME_SLURM \
                           --export="$var" "$SCRIPTPATH"/Master_vcf.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ mastervcf_id.txt ]
  echo -e "$sbatch_matrix_id" >> mastervcf_id.txt
fi

# STEP 02:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# Matrix ERROR CHECKING SNP ACROSS ALL GENOMES IF INDELS ARE PRESENT
# "mastervcf_id.txt" was generated in MATRIX
# Submission ID:  [ sbatch_clean_id ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         mastervcf_id.txt            =   EXIST
#             ${sequences[$i]}.clean.vcf  =   DOES NOT EXIST
#             INDEL MERGE                 =   YES
# THEN:       Extract information in COLUMN 2
#             Run GATK
# -----------------------------------------------------------------------------
if [ -s mastervcf_id.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ mastervcf_id.txt ]
  sbatch_cat_ids=`cat mastervcf_id.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
	echo -e "Submit JOB (sbatch): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	out=("${sequences_tmp[@]/_1_sequence.fastq.gz/.clean.vcf}")
	bam=("${sequences_tmp[@]/_1_sequence.fastq.gz/.bam}")
	bam_array=("${bam[@]/#/$PBS_O_WORKDIR/Phylo/bams/}")
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${sequences[$i]}.clean.vcf ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_clean_id=`sbatch --job-name=clean_vcf \
                              --dependency=afterok:$sbatch_array_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "$sbatch_clean_id" >> clean_vcf_id.txt
		fi
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		if [ ! -s $PBS_O_WORKDIR/Phylo/indels/out/${sequences[$i]}.clean.vcf \
           -a "$indel_merge" == yes ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_clean_id=`sbatch --job-name=clean_vcf \
                              --dependency=afterok:$sbatch_array_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "$sbatch_clean_id" >> clean_vcf_id.txt
		fi
  done
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         mastervcf_id.txt            =   DOES NOT EXIST
#             ${sequences[$i]}.clean.vcf  =   DOES NOT EXIST
#             INDEL MERGE                 =   YES
# THEN:       Run GATK
# -----------------------------------------------------------------------------
if [ ! -s mastervcf_id.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch):ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
	out=("${sequences_tmp[@]/_1_sequence.fastq.gz/.clean.vcf}")
	bam=("${sequences_tmp[@]/_1_sequence.fastq.gz/.bam}")
	bam_array=("${bam[@]/#/$PBS_O_WORKDIR/Phylo/bams/}")
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${sequences[$i]}.clean.vcf ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_clean_id=`sbatch --job-name=clean_vcf \
                              --dependency=afterok:$sbatch_array_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "$sbatch_clean_id" >> clean_vcf_id.txt
		fi
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
		if [ ! -s $PBS_O_WORKDIR/Phylo/indels/out/${sequences[$i]}.clean.vcf \
           -a "$indel_merge" == yes ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_clean_id=`sbatch --job-name=clean_vcf \
                              --dependency=afterok:$sbatch_array_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "$sbatch_clean_id" >> clean_vcf_id.txt
		fi
  done
fi

# STEP 03:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# CREATION OF SNP ARRAY
# "clean_vcf_id.txt" was generated in ERROR CHECKING
# Submission ID:  [ sbatch_matrix_final ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         clean_vcf_id.txt          =   EXIST
#             Ortho_SNP_matrix.nex      =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT SNP_matrix.sh
# -----------------------------------------------------------------------------
if [ -s clean_vcf_id.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ clean_vcf_id.txt ]
  sbatch_cat_ids=`cat clean_vcf_id.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): CREATION OF SNP ARRAY\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      variant_genome=$variant_genome,\
      annotate=$annotate,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge,\
      tri_tetra_allelic=$tri_tetra_allelic"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_matrix_final=`sbatch --job-name=Matrix_vcf \
                           --dependency=afterok:$sbatch_clean_id \
                           --account=$ACCOUNT_SLURM \
                           --partition=$PARTITION_SLURM \
                           --mail-user=$ADDRESS \
                           --mail-type=NONE \
                           --nodes=1 \
                           --ntasks-per-node=$PROCESSORS_SLURM \
                           --mem-per-cpu=$MEMORY_SLURM \
                           --time=$TIME_SLURM \
                           --export="$var" "$SCRIPTPATH"/SNP_matrix.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ matrix_id.txt ]
  echo -e "$sbatch_matrix_final" >> matrix_id.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         clean_vcf_id.txt          =   DOES NOT EXIST
#             Ortho_SNP_matrix.nex      =   DOES NOT EXIST
# THEN:       Run SCRIPT SNP_matrix.sh
# -----------------------------------------------------------------------------
if [ ! -s clean_vcf_id.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): CREATION OF SNP ARRAY\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      variant_genome=$variant_genome,\
      annotate=$annotate,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge,\
      tri_tetra_allelic=$tri_tetra_allelic"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_matrix_final=`sbatch --job-name=Matrix_vcf \
                           --dependency=afterok:$sbatch_clean_id \
                           --account=$ACCOUNT_SLURM \
                           --partition=$PARTITION_SLURM \
                           --mail-user=$ADDRESS \
                           --mail-type=NONE \
                           --nodes=1 \
                           --ntasks-per-node=$PROCESSORS_SLURM \
                           --mem-per-cpu=$MEMORY_SLURM \
                           --time=$TIME_SLURM \
                           --export="$var" "$SCRIPTPATH"/SNP_matrix.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ matrix_id.txt ]
  echo -e "$sbatch_matrix_final" >> matrix_id.txt
fi
}


###############################################################################
# FINAL SNP MATRIX GENERATION
# Generate a SNP matrix from the Phylo DIR assuming all SNP and BAM files have
# been linked into these DIR.
# Relies on the OUTPUTS from the VARIANT function above
# Final product is a clean.vcf file for SNP calls across genomes
# IF indels merge is set to yes creates clean.vcf for indels across genomes
# - STEP 01:  Final Matrix
# - STEP 02:  Matrix final ERROR CHECKING
# - STEP 03:  Creation of Final SNP array
# - SCRIPT:   Master_vcf_final.sh
# - SCRIPT:   SNP_matrix.sh
###############################################################################

# STEP 01:
# MATRIX FINAL
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# "sbatch_ids.txt" is generated in INDEXING
# Submission ID:  [ sbatch_matrix_id ]
matrix_final_slurm ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         sbatch_ids.txt            =   EXIST
#             master.vcf                =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT Master_vcf_final.sh
# -----------------------------------------------------------------------------
if [ -s sbatch_ids.txt \
     -a \
     ! -s Phylo/out/master.vcf ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ sbatch_ids.txt ]
  sbatch_cat_ids=`cat sbatch_ids.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
	echo -e "Submit JOB (sbatch): CREATION OF MASTER VCF file\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_matrix_id=`sbatch --job-name=Master_vcf \
                           --dependency=afterok:$sbatch_array_id \
                           --account=$ACCOUNT_SLURM \
                           --partition=$PARTITION_SLURM \
                           --mail-user=$ADDRESS \
                           --mail-type=NONE \
                           --nodes=1 \
                           --ntasks-per-node=$PROCESSORS_SLURM \
                           --mem-per-cpu=$MEMORY_SLURM \
                           --time=$TIME_SLURM \
                           --export="$var" "$SCRIPTPATH"/Master_vcf_final.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ mastervcf_id.txt ]
	echo -e "$sbatch_matrix_id" >> mastervcf_id.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         sbatch_ids.txt            =   DOES NOT EXIST
#             master.vcf                =   DOES NOT EXIST
# THEN:       Run SCRIPT Master_vcf_final.sh
# -----------------------------------------------------------------------------
if [ ! -s sbatch_ids.txt \
     -a \
     ! -s Phylo/out/master.vcf ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): CREATION OF MASTER VCF file\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_matrix_id=`sbatch --job-name=Master_vcf \
                           --dependency=afterok:$sbatch_array_id \
                           --account=$ACCOUNT_SLURM \
                           --partition=$PARTITION_SLURM \
                           --mail-user=$ADDRESS \
                           --mail-type=NONE \
                           --nodes=1 \
                           --ntasks-per-node=$PROCESSORS_SLURM \
                           --mem-per-cpu=$MEMORY_SLURM \
                           --time=$TIME_SLURM \
                           --export="$var" "$SCRIPTPATH"/Master_vcf_final.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ mastervcf_id.txt ]
  echo -e "$sbatch_matrix_id" >> mastervcf_id.txt
fi

# STEP 02:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# Matrix final ERROR CHECKING SNP ACROSS ALL GENOMES IF INDELS ARE PRESENT
# "mastervcf_id.txt" was generated in MATRIX
# Submission ID:  [ sbatch_clean_id ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         mastervcf_id.txt            =   EXIST
#             Phylo/out/${out[$i]         =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run GATK
# -----------------------------------------------------------------------------
if [ -s mastervcf_id.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ mastervcf_id.txt ]
  sbatch_cat_ids=`cat mastervcf_id.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
	echo -e "Submit JOB (sbatch): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  clean_array=(`find $PBS_O_WORKDIR/Phylo/snps/*.vcf -printf "%f "`)
  out=("${clean_array[@]/.vcf/.clean.vcf}")
  bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${out[$i]} ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_clean_id=`sbatch --job-name=clean_vcf \
                              --dependency=afterok:$sbatch_array_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "$sbatch_clean_id" >> clean_vcf_id.txt
		fi
  done
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         mastervcf_id.txt            =   DOES NOT EXIST
#             Phylo/out/${out[$i]         =   DOES NOT EXIST
# THEN:       Run GATK
# -----------------------------------------------------------------------------
if [ ! -s mastervcf_id.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  clean_array=(`find $PBS_O_WORKDIR/Phylo/snps/*.vcf -printf "%f "`)
  out=("${clean_array[@]/.vcf/.clean.vcf}")
  bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${out[$i]} ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_clean_id=`sbatch --job-name=clean_vcf \
                              --dependency=afterok:$sbatch_array_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "$sbatch_clean_id" >> clean_vcf_id.txt
		fi
  done
fi
# -----------------------------------------------------------------------------
# 3rd Conditional:
# IF:         mastervcf_id.txt            =   EXIST
#             Phylo/out/${out[$i]         =   DOES NOT EXIST
#             INDEL MERGE                 =   YES
# THEN:       Extract information in COLUMN 2
#             Run GATK
# -----------------------------------------------------------------------------
if [ -s mastervcf_id.txt \
     -a "$indel_merge" == yes ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ mastervcf_id.txt ]
  sbatch_cat_ids=`cat mastervcf_id.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
	echo -e "Submit JOB (sbatch): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  clean_array=(`find $PBS_O_WORKDIR/Phylo/indels/*.vcf -printf "%f "`)
  out=("${clean_array[@]/.vcf/.clean.vcf}")
  bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_clean_id=`sbatch --job-name=clean_vcf \
                              --dependency=afterok:$sbatch_array_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into [ clean_vcf_id.txt ]
      echo -e "$sbatch_clean_id" >> clean_vcf_id.txt
		fi
  done
fi
# -----------------------------------------------------------------------------
# 4th Conditional:
# IF:         mastervcf_id.txt            =   DOES NOT EXIST
#             Phylo/out/${out[$i]         =   DOES NOT EXIST
#             INDEL MERGE                 =   YES
# THEN:       Run GATK
# -----------------------------------------------------------------------------
if [ ! -s mastervcf_id.txt \
       -a "$indel_merge" == yes ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  clean_array=(`find $PBS_O_WORKDIR/Phylo/indels/*.vcf -printf "%f "`)
  out=("${clean_array[@]/.vcf/.clean.vcf}")
  bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
  n=${#bam_array[@]}
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${out[$i]} ];
    then
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR \
          $GATK -T UnifiedGenotyper \
                -rf BadCigar \
                -R $PBS_O_WORKDIR/${ref}.fasta \
                -I ${bam_array[$i]} \
                -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                -gt_mode GENOTYPE_GIVEN_ALLELES \
                -out_mode EMIT_ALL_SITES \
                -stand_call_conf 0.0 \
                -glm BOTH \
                -G none"
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # Resource manager submission parameters
      sbatch_clean_id=`sbatch --job-name=clean_vcf \
                              --dependency=afterok:$sbatch_array_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export=command="$cmd" "$SCRIPTPATH"/Header.pbs`
      # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
      # PRINT the JOB ID into  [ clean_vcf_id.txt ]
      echo -e "$sbatch_clean_id" >> clean_vcf_id.txt
		fi
  done
fi

# STEP 03:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# CREATION OF FINAL SNP ARRAY
# "clean_vcf_id.txt" was generated in ERROR CHECKING
# Submission ID:  [ sbatch_matrix_final ]
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         clean_vcf_id.txt          =   EXIST
#             Ortho_SNP_matrix.nex      =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT SNP_matrix.sh
# -----------------------------------------------------------------------------
if [ -s clean_vcf_id.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ clean_vcf_id.txt ]
  sbatch_cat_ids=`cat clean_vcf_id.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): CREATION OF SNP ARRAY\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      variant_genome=$variant_genome,\
      annotate=$annotate,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge,\
      tri_tetra_allelic=$tri_tetra_allelic"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_matrix_final=`sbatch --job-name=Matrix_vcf \
                              --dependency=afterok:$sbatch_clean_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export="$var" "$SCRIPTPATH"/SNP_matrix.sh`
   # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
   # PRINT the JOB ID into [ matrix_id.txt ]
  echo -e "$sbatch_matrix_final" >> matrix_id.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         clean_vcf_id.txt          =   DOES NOT EXIST
#             Ortho_SNP_matrix.nex      =   DOES NOT EXIST
# THEN:       Run SCRIPT SNP_matrix.sh
# -----------------------------------------------------------------------------
if [ ! -s clean_vcf_id.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): CREATION OF SNP ARRAY\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,\
      seq_path=$seq_directory,\
      variant_genome=$variant_genome,\
      annotate=$annotate,\
      SCRIPTPATH=$SCRIPTPATH,\
      indel_merge=$indel_merge,\
      tri_tetra_allelic=$tri_tetra_allelic"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_matrix_final=`sbatch --job-name=Matrix_vcf \
                              --dependency=afterok:$sbatch_clean_id \
                              --account=$ACCOUNT_SLURM \
                              --partition=$PARTITION_SLURM \
                              --mail-user=$ADDRESS \
                              --mail-type=NONE \
                              --nodes=1 \
                              --ntasks-per-node=$PROCESSORS_SLURM \
                              --mem-per-cpu=$MEMORY_SLURM \
                              --time=$TIME_SLURM \
                              --export="$var" "$SCRIPTPATH"/SNP_matrix.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ matrix_id.txt ]
  echo -e "$sbatch_matrix_final" >> matrix_id.txt
fi
}


###############################################################################
# MERGE BED:
# Takes OUTPUT of each BED coverage assessment of the sequence alignment
# AND merges them in a comparative matrix
# Run when $strain=all
# NOT run When a single strain is analysed i.e. $strain doesn't equal all
# - STEP 01:  BED Coverage MERGE
# - SCRIPT:   BEDCov_merge.sh
###############################################################################

# STEP 01:
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# BED Coverage MERGE
# "sbatch_array_ids.txt" is generated in VARIANT
# Submission ID:  [ sbatch_BED_id ]
merge_BED_slurm ()
{
# -----------------------------------------------------------------------------
# 1st Conditional:
# IF:         sbatch_array_ids.txt      =   EXIST
#             Bedcov_merge.txt          =   DOES NOT EXIST
# THEN:       Extract information in COLUMN 2
#             Run SCRIPT BedCov_merge.sh
# -----------------------------------------------------------------------------
if [ -s sbatch_array_ids.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Bedcov_merge.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Extract INFO from [ sbatch_array_ids.txt ]
  sbatch_cat_ids=`cat sbatch_array_ids.txt | \
                  cut -f4 -d ' ' | \
                  sed -e 's/$/,/' | \
                  tr -d '\n' | \
                  sed -e 's/,$//'`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): BEDcov merge\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="seq_path=$seq_directory"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
  sbatch_BED_id=`sbatch --job-name=BEDcov_merge \
                        --dependency=afterok:$sbatch_array_id \
                        --account=$ACCOUNT_SLURM \
                        --partition=$PARTITION_SLURM \
                        --mail-user=$ADDRESS \
                        --mail-type=NONE \
                        --nodes=1 \
                        --ntasks-per-node=$PROCESSORS_SLURM \
                        --mem-per-cpu=$MEMORY_SLURM \
                        --time=$TIME_SLURM \
                        --export="$var" "$SCRIPTPATH"/BedCov_merge.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ sbatch_BED_id.txt ]
  echo -e "$sbatch_BED_id" >> sbatch_BED_id.txt
fi
# -----------------------------------------------------------------------------
# 2nd Conditional:
# IF:         sbatch_array_ids.txt      =   DOES NOT EXIST
#             Bedcov_merge.txt          =   DOES NOT EXIST
# THEN:       Run SCRIPT BedCov_merge.sh
# -----------------------------------------------------------------------------
if [ ! -s sbatch_array_ids.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Bedcov_merge.txt ];
then
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # SBATCH submission message - PRINTS on screen at the time of submission
  echo -e "Submit JOB (sbatch): BEDcov merge\n"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # VARIABLES
  var="ref=$ref,seq_path=$seq_directory"
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # Resource manager submission parameters
	sbatch_BED_id=`sbatch --job-name=BEDcov_merge \
                        --dependency=afterok:$sbatch_array_id \
                        --account=$ACCOUNT_SLURM \
                        --partition=$PARTITION_SLURM \
                        --mail-user=$ADDRESS \
                        --mail-type=NONE \
                        --nodes=1 \
                        --ntasks-per-node=$PROCESSORS_SLURM \
                        --mem-per-cpu=$MEMORY_SLURM \
                        --time=$TIME_SLURM \
                        --export="$var" "$SCRIPTPATH"/BedCov_merge.sh`
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # PRINT the JOB ID into [ sbatch_BED_id.txt ]
  echo -e "$sbatch_BED_id" >> sbatch_BED_id.txt
fi
}


###############################################################################
# VARIABLE TESTS
# Determine which functions need to be run for the SPANDx pipeline
###############################################################################

  # More than ONE STRAIN is present
  if [ "$strain" == all ];
  then
    variants_slurm
	  merge_BED_slurm
  fi
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
  # ONE STRAIN is present
  if [ "$strain" != all -a "$strain" != none ];
  then
    variants_single_slurm
  fi
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  --
  # RUN matrix if more then ONE STRAIN is present
  if [ "$matrix" == yes -a "$strain" != none ];
  then
    matrix_slurm
  fi

  if [ "$matrix" == yes -a "$strain" == none ];
  then
    matrix_final_slurm
  fi
  # -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
fi



###############################################################################
###############################################################################
###############################################################################
#
#                           RESOURCE MANAGER:
#                                NONE
#
###############################################################################
###############################################################################
###############################################################################


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Determine SCHEDULER
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

if [ "$SCHEDULER" == NONE ];
then
  # ---------------------------------------------------------------------------
  if [ ! $PBS_O_WORKDIR ];
  then
    PBS_O_WORKDIR="$PWD"
  fi

cd $PBS_O_WORKDIR
# -----------------------------------------------------------------------------


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# INDEXING:
# - (STEP 01, STEP 02): Indexing of the reference with SAMTools and BWA
# - (STEP 03) Creation of the GATK and picard reference dictionaries
# - (STEP 04) Creation of the BED file for the bedcoverage module
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# STEP 01:
# BWA REFERENCE INDEXING ---> [ sbatch_id_bwa ]
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# IF:
# - BWA INDEX FILE ($ref_index) = DOES NOT EXIST
# THEN:
# - Run BWA
if [ ! -s "$ref_index" ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Run JOB: BWA REFERENCE INDEXING\n"
  # ---------------------------------------------------------------------------
  # BWA:
  # -p STR	Prefix of the output database [same as db filename]
  # -a STR	Algorithm for constructing BWT index
  #         IS linear-time algorithm for constructing suffix array.
  cmd="$BWA index -a is \
                  -p ${seq_directory}/${ref} \
                  ${seq_directory}/${ref}.fasta"
  # ---------------------------------------------------------------------------
  # Run command
	echo $cmd
	$cmd 2>&1
	sleep 1
  # ---------------------------------------------------------------------------
fi

# 02
# SAMtools REFERENCE INDEXING ---> [ sbatch_id_SAMtools ]
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# IF:
# - SAMtools INDEX FILE ($REF_INDEX_FILE) = DOES NOT EXIST
# THEN:
# - Run SAMtools
if [ ! -s "$REF_INDEX_FILE" ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Run JOB: SAMtools REFERENCE indexing\n"
  # ---------------------------------------------------------------------------
  # SAMtools:
  cmd="$SAMTOOLS faidx ${seq_directory}/${ref}.fasta"
  # ---------------------------------------------------------------------------
  # Run command
  echo $cmd
	$cmd 2>&1
	sleep 1
  # ---------------------------------------------------------------------------
fi

# 03
# DICTIONARY CREATION  ---> [ sbatch_id_Picard ]
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
# IF:
# - INDEX DICTIONARY ($REF_DICT) = DOES NOT EXIST
# THEN:
# - Run Picard
if [ ! -s "$REF_DICT" ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Run JOB: ${ref}.dict creation\n"
  # ---------------------------------------------------------------------------
  # Picard:
  cmd="$JAVA $SET_VAR $CREATEDICT \
      R=${seq_directory}/${ref}.fasta \
      O=$REF_DICT"
  # ---------------------------------------------------------------------------
  # Run command
	echo $cmd
	$cmd 2>&1
	sleep 1
  # ---------------------------------------------------------------------------
fi

# 04
# MAKE BED file
# BEDtools makes adjacent or sliding windows across a genome or BED file
# Window size by DEFAULT is set to 1000 bp
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
if [ ! -s "$REF_BED" ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Run JOB: BED file construction with BEDTools\n"
  # ---------------------------------------------------------------------------
  # BEDtools:
	cmd="$BEDTOOLS makewindows -g $REF_INDEX_FILE \
  -w $window "
  # ---------------------------------------------------------------------------
  # Run command
  echo $cmd
	$cmd > $REF_BED
	sleep 1
  # ---------------------------------------------------------------------------
fi


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# SEQUENCE ALIGNMENT and VARIANT CALLING for [ $sequences ]:
# MAIN alignment and variant calling SCRIPT contained with SPANDx

# SCRIPT: Align_SNP_indel.sh
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# STEP 01:
# VARIANTS_SINGLE
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
variants_single ()
{
# IF:
# - $sequences.snps.PASS.vcf = DOES NOT EXIST
# THEN:
# - Run SCRIPT = Align_SNP_indel.sh
  if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/$sequences.snps.PASS.vcf ];
  then
    # -------------------------------------------------------------------------
    # PRINTS on screen
    echo -e "\nRun JOB: SEQUENCE ALIGNMENT and VARIANT CALLING for ${sequences[$i]}\n"
    # -------------------------------------------------------------------------
    # VARIABLES
	  SCRIPTPATH=$SCRIPTPATH
		echo $SCRIPTPATH
    export seq=$sequences \
           ref=$ref \
           org=$org \
           strain=$strain \
           variant_genome=$variant_genome \
           annotate=$annotate \
           tech=$tech \
           pairing=$pairing \
           seq_path=$seq_directory \
           SCRIPTPATH=$SCRIPTPATH
    # -------------------------------------------------------------------------
    # Run command
	  "$SCRIPTPATH"/Align_SNP_indel.sh
    # -------------------------------------------------------------------------
  fi
}

# STEP 02:
# VARIANTS
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
variants ()
{
  # ---------------------------------------------------------------------------
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -------------------------------------------------------------------------
    # IF:
    # - ${sequences[$i]}.snps.PASS.vcf = DOES NOT EXIST
    # THEN:
    # - Run SCRIPT = Align_SNP_indel.sh for $sequences
    if [ ! -s ${PBS_O_WORKDIR}/Outputs/SNPs_indels_PASS/${sequences[$i]}.snps.PASS.vcf ];
    then
      # -------------------------------------------------------------------------
      # PRINTS on screen
      echo -e "Run JOB: SEQUENCE ALIGNMENT and VARIANT CALLING fo ${sequences[$i]}\n"
      # -------------------------------------------------------------------------
      # VARIABLES
	    export seq=${sequences[$i]} \
             ref=$ref \
             org=$org \
             strain=$strain \
             variant_genome=$variant_genome \
             annotate=$annotate \
             tech=$tech \
             pairing=$pairing \
             seq_path=$seq_directory \
             SCRIPTPATH=$SCRIPTPATH
     # -------------------------------------------------------------------------
     # Run command
		 "$SCRIPTPATH"/Align_SNP_indel.sh
     # -------------------------------------------------------------------------
    fi
  done
}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# SNP MATRIX GENERATION
# Main comparative genomics section of SPANDx
# Relies on the OUTPUTS from the VARIANT function above
# Final product is a clean.vcf file for SNP calls across genomes
# IF indels merge is set to yes creates clean.vcf files for indels across genomes

# SCRIPT: Master_vcf.sh
# SCRIPT: SNP_matrix.sh
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# STEP 01:
# MATRIX
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
matrix ()
{
# IF:
# - master.vcf = DOES NOT EXIST
# THEN:
# - Run SCRIPT = Master_vcf.sh
if [ ! -s Phylo/out/master.vcf ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Run JOB: CREATION OF MASTER VCF file\n"
  # ---------------------------------------------------------------------------
  # VARIABLES
  export ref=$ref \
         seq_path=$seq_directory \
         SCRIPTPATH=$SCRIPTPATH \
         indel_merge=$indel_merge
  # ---------------------------------------------------------------------------
  # Run command
  "$SCRIPTPATH"/Master_vcf.sh
  # ---------------------------------------------------------------------------
fi

# ERROR CHECKING SNP ACROSS ALL GENOMES IF INDELS ARE PRESENT
# -----------------------------------------------------------------------------
# PRINTS on screen
echo -e "Run JOB: ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
# -----------------------------------------------------------------------------
out=("${sequences_tmp[@]/_1_sequence.fastq.gz/.clean.vcf}")
bam=("${sequences_tmp[@]/_1_sequence.fastq.gz/.bam}")
bam_array=("${bam[@]/#/$PBS_O_WORKDIR/Phylo/bams/}")
n=${#bam_array[@]}
# -----------------------------------------------------------------------------
# For looop (ALL samples)
for (( i=0; i<n; i++ ));
do
  # ---------------------------------------------------------------------------
  # IF:
  # - ${sequences[$i]}.clean.vcf = DOES NOT EXIST
  # THEN:
  # - Run GATK
  if [ ! -s $PBS_O_WORKDIR/Phylo/out/${sequences[$i]}.clean.vcf ];
  then
    # -------------------------------------------------------------------------
    # GATK
    # -T:   Name of tool to run
    # -rf:  Read filter (Reads not passing filters will not be used)
    # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
    cmd="$JAVA $SET_VAR $GATK -T UnifiedGenotyper \
                              -rf BadCigar \
                              -R $PBS_O_WORKDIR/${ref}.fasta \
                              -I ${bam_array[$i]} \
                              -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                              -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                              -gt_mode GENOTYPE_GIVEN_ALLELES \
                              -out_mode EMIT_ALL_SITES \
                              -stand_call_conf 0.0 \
                              -glm BOTH \
                              -G none"
    # -------------------------------------------------------------------------
    # Run command
  	echo $cmd
  	$cmd 2>&1
    # -------------------------------------------------------------------------
  fi
  # ---------------------------------------------------------------------------
  # IF:
  # - ${sequences[$i]}.clean.vcf = DOES NOT EXIST
  # - INDEL MERGE = YES
  # THEN:
  # - Run GATK
  if [ ! -s $PBS_O_WORKDIR/Phylo/indels/out/${sequences[$i]}.clean.vcf \
         -a "$indel_merge" == yes ];
  then
    # -------------------------------------------------------------------------
    # GATK
    # -T:   Name of tool to run
    # -rf:  Read filter (Reads not passing filters will not be used)
    # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
    cmd="$JAVA $SET_VAR $GATK -T UnifiedGenotyper \
                              -rf BadCigar \
                              -R $PBS_O_WORKDIR/${ref}.fasta \
                              -I ${bam_array[$i]} \
                              -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                              -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                              -gt_mode GENOTYPE_GIVEN_ALLELES \
                              -out_mode EMIT_ALL_SITES \
                              -stand_call_conf 0.0 \
                              -glm BOTH \
                              -G none"
    # -------------------------------------------------------------------------
    # Run command
    echo $cmd
    $cmd
    # -------------------------------------------------------------------------
  fi
done

# CREATION OF SNP ARRAY
# -----------------------------------------------------------------------------
# IF:
# - Ortho_SNP_matrix.nex = DOES NOT EXIST
# THEN:
# - Run SCRIPT = SNP_matrix.sh
if [ ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Run JOB: CREATION OF SNP ARRAY\n"
  # ---------------------------------------------------------------------------
  # VARIABLES
  export ref=$ref \
         seq_path=$seq_directory \
         variant_genome=$variant_genome \
         annotate=$annotate \
         SCRIPTPATH=$SCRIPTPATH \
         indel_merge=$indel_merge \
         tri_tetra_allelic=$tri_tetra_allelic
  # ---------------------------------------------------------------------------
  # Run command
	"$SCRIPTPATH"/SNP_matrix.sh
  # ---------------------------------------------------------------------------
fi
}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# FINAL MATRIX GENERATION
# Generate a SNP matrix from the Phylo DIR assuming all SNP and BAM files have
# been linked into these DIR.
### qsub for indel_merge is currently untested
# Relies on the OUTPUTS from the VARIANT function above
# Final product is a clean.vcf file for SNP calls across genomes
# IF indels merge is set to yes creates clean.vcf files for indels across genomes

# SCRIPT: Master_vcf.sh
# SCRIPT: SNP_matrix.sh
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# STEP 01:
# MATRIX FINAL
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
matrix_final ()
{
# IF:
# - qsub_ids.txt = DOES NOT EXIST
# - master.vcf = DOES NOT EXIST
# THEN:
# - Run SCRIPT = Master_vcf_final.sh
if [ ! -s qsub_ids.txt \
     -a \
     ! -s Phylo/out/master.vcf ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Run JOB: CREATION OF MASTER VCF file\n"
  # ---------------------------------------------------------------------------
  # VARIABLES
  export ref=$ref \
         seq_path=$seq_directory \
         SCRIPTPATH=$SCRIPTPATH \
         indel_merge=$indel_merge
  # ---------------------------------------------------------------------------
  # Run command
  "$SCRIPTPATH"/Master_vcf_final.sh
  # ---------------------------------------------------------------------------
fi

# ERROR CHECKING SNP ACROSS ALL GENOMES
# -----------------------------------------------------------------------------
# IF:
# - mastervcf_id.txt = EXIST
# THEN:
# - Run GATK
  if [ ! -s mastervcf_id.txt ];
  then
    # -------------------------------------------------------------------------
    # PRINTS on screen
    echo -e "Run JOB: ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
    # -------------------------------------------------------------------------
    clean_array=(`find $PBS_O_WORKDIR/Phylo/snps/*.vcf -printf "%f "`)
    out=("${clean_array[@]/.vcf/.clean.vcf}")
    bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
    n=${#bam_array[@]}
    # -------------------------------------------------------------------------
    # For looop (ALL samples)
    for (( i=0; i<n; i++ ));
    do
      # -----------------------------------------------------------------------
      # IF:
      # - ${out[$i]} = DOES NOT EXIST
      # THEN:
      # - Run GATK
      if [ ! -s $PBS_O_WORKDIR/Phylo/out/${out[$i]} ];
      then
        # ---------------------------------------------------------------------
        # GATK
        # -T:   Name of tool to run
        # -rf:  Read filter (Reads not passing filters will not be used)
        # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
        cmd="$JAVA $SET_VAR $GATK -T UnifiedGenotyper \
                                  -rf BadCigar \
                                  -R $PBS_O_WORKDIR/${ref}.fasta \
                                  -I ${bam_array[$i]} \
                                  -o $PBS_O_WORKDIR/Phylo/out/${out[$i]} \
                                  -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/out/master.vcf \
                                  -gt_mode GENOTYPE_GIVEN_ALLELES \
                                  -out_mode EMIT_ALL_SITES \
                                  -stand_call_conf 0.0 \
                                  -glm BOTH \
                                  -G none"
        # ---------------------------------------------------------------------
        # Run command
			  echo $cmd
        $cmd 2>&1
        # ---------------------------------------------------------------------
		  fi
    done
  fi

# ERROR CHECKING SNP ACROSS ALL GENOMES IF INDELS ARE PRESENT
# -----------------------------------------------------------------------------
# IF:
# - mastervcf_id.txt = EXIST
# - INDEL MERGE = YES
# THEN:
# - Run GATK
if [ ! -s mastervcf_id.txt \
       -a "$indel_merge" == yes ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Run JOB: ERROR CHECKING SNP CALLS ACROSS GENOMES\n"
  # ---------------------------------------------------------------------------
  clean_array=(`find $PBS_O_WORKDIR/Phylo/indels/*.vcf -printf "%f "`)
  out=("${clean_array[@]/.vcf/.clean.vcf}")
  bam_array=(`find $PBS_O_WORKDIR/Phylo/bams/*.bam`)
  n=${#bam_array[@]}
  # ---------------------------------------------------------------------------
  # For looop (ALL samples)
  for (( i=0; i<n; i++ ));
  do
    # -------------------------------------------------------------------------
    # IF:
    # - ${out[$i]} = DOES NOT EXIST
    # THEN:
    # - Run GATK
    if [ ! -s $PBS_O_WORKDIR/Phylo/out/${out[$i]} ];
    then
      # -----------------------------------------------------------------------
      # GATK
      # -T:   Name of tool to run
      # -rf:  Read filter (Reads not passing filters will not be used)
      # I:    Input file (Data mapped to a REFERENCE, in BAM or CRAM format)
      cmd="$JAVA $SET_VAR $GATK -T UnifiedGenotyper \
                                -rf BadCigar \
                                -R $PBS_O_WORKDIR/${ref}.fasta \
                                -I ${bam_array[$i]} \
                                -o $PBS_O_WORKDIR/Phylo/indels/out/${out[$i]} \
                                -alleles:masterAlleles $PBS_O_WORKDIR/Phylo/indels/out/master_indels.vcf \
                                -gt_mode GENOTYPE_GIVEN_ALLELES \
                                -out_mode EMIT_ALL_SITES \
                                -stand_call_conf 0.0 \
                                -glm BOTH \
                                -G none"
      # -----------------------------------------------------------------------
      # Run command
		  echo $cmd
		  $cmd
      # -----------------------------------------------------------------------
    fi
  done
fi

# CREATION OF FINAL SNP ARRAY
# -----------------------------------------------------------------------------
# IF:
# - clean_vcf_id.txt = DOES NOT EXIST
# - Ortho_SNP_matrix.nex = DOES NOT EXIST
# THEN:
# - Run SCRIPT = SNP_matrix.sh
if [ ! -s clean_vcf_id.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Ortho_SNP_matrix.nex ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Run JOB: CREATION OF SNP ARRAY\n"
  # ---------------------------------------------------------------------------
  # VARIABLES
  export ref=$ref \
         seq_path=$seq_directory \
         variant_genome=$variant_genome \
         annotate=$annotate \
         SCRIPTPATH=$SCRIPTPATH \
         indel_merge=$indel_merge \
         tri_tetra_allelic=$tri_tetra_allelic
  # ---------------------------------------------------------------------------
  # Run command
  "$SCRIPTPATH"/SNP_matrix.sh
  # ---------------------------------------------------------------------------
fi
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# MERGE BED:
# Takes OUTPUT of each BED coverage assessment of the sequence alignment
# AND merges them in a comparative matrix
# Run when $strain=all
# NOT run When a single strain is analysed i.e. $strain doesn't equal all

# SCRIPT: BEDCov_merge.sh
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# STEP 01:
# BED Coverage MERGE
# = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
merge_BED ()
{
# IF:
# - qsub_array_ids.txt = DOES NOT EXIST
# - Bedcov_merge.txt = DOES NOT EXIST
# THEN:
# - Run SCRIPT = BedCov_merge.sh
if [ ! -s qsub_array_ids.txt \
     -a \
     ! -s $PBS_O_WORKDIR/Outputs/Comparative/Bedcov_merge.txt ];
then
  # ---------------------------------------------------------------------------
  # PRINTS on screen
  echo -e "Running job for BEDcov merge\n"
  # ---------------------------------------------------------------------------
  # VARIABLES
  export ref=$ref seq_path=$seq_directory
  # ---------------------------------------------------------------------------
  # Run command
  "$SCRIPTPATH"/BedCov_merge.sh
  # ---------------------------------------------------------------------------
fi
}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# VARIABLE TESTS
# Determine which functions need to be run for the SPANDx pipeline
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

  # ---------------------------------------------------------------------------
  # More than ONE STRAIN is present
  if [ "$strain" == all ];
  then
    variants
	  merge_BED
  fi
  # ---------------------------------------------------------------------------
  # ONE STRAIN is present
  if [ "$strain" != all -a "$strain" != none ];
  then
    variants_single
  fi
  # ---------------------------------------------------------------------------
  # RUN matrix if more then ONE STRAIN is present
  if [ "$matrix" == yes -a "$strain" != none ];
  then
    matrix
  fi

  if [ "$matrix" == yes -a "$strain" == none ];
  then
    matrix_final
  fi
  # ---------------------------------------------------------------------------
fi

exit 0
